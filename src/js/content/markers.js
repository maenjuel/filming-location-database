import * as C from '../core/constants.js';
import { config } from '../core/config.js';
import { initMap } from '../components/map.js';
import { filmingLocations, geographicLocations } from './locations.js';
import { showLocationContent } from './content.js';

// Start with a set of empty arrays we populate later
var locations = [];

function buildMarkers() {
	// If there are already locations in the array, we remove the locations
	// From the map and empty the array to start anew
	if (locations.length > 0) {
		C.MAP.removeLayer(markers);
		locations = [];
	}

	filmingLocations.forEach(filmLoc => {
		// Define data points
		var marker = {};
		var geoLocId = filmLoc.geoLoc.id;
		var name = filmLoc.name;
		var nativeName;
		var filmLocSource = filmLoc.source;
		var description = filmLoc.description;
		var website;
		var wikimedia;
		var wikipedia;
		var address;
		var lat;
		var lon;
		var openstreetmap;
		var media;
		var seasonEpisode = filmLoc.seasonEpisode;
		var sourceArray = [];

		geographicLocations.forEach(geoLoc => {
			if(geoLocId == geoLoc.id) {
				nativeName = geoLoc.name.native;
				address = geoLoc.address;
				lat = geoLoc.location.lat
				lon = geoLoc.location.lon;
				media = filmLoc.media;
				openstreetmap = geoLoc.openstreetmap;

				// Add description
				if(filmLoc.description != null) {
					description = filmLoc.description;
				} else if (filmLoc.description == null && geoLoc.description != null) {
					description = geoLoc.description;
				} else {
					description = null;
				}

				// Add website
				if(filmLoc.website != null) {
					website = filmLoc.website;
				} else if (filmLoc.website == null && geoLoc.website != null) {
					website = geoLoc.website;
				} else {
					website = null;
				}

				// Add Wikimedia Commons
				if(filmLoc.wikimedia != null) {
					wikimedia = filmLoc.wikimedia;
				} else if (filmLoc.wikimedia == null && geoLoc.wikimedia != null) {
					wikimedia = geoLoc.wikimedia;
				} else {
					wikimedia = null;
				}

				// Add Wikipedia article
				if(filmLoc.wikipedia != null) {
					wikipedia = filmLoc.wikipedia;
				} else if (filmLoc.wikipedia == null && geoLoc.wikipedia != null) {
					wikipedia = geoLoc.wikipedia;
				} else {
					wikipedia = null;
				}

				// Merge sources
				var geoLocSource = geoLoc.source;
				var filmLocSource = filmLoc.source;
				var mergedSources = [];

				if(geoLocSource.length > 0 && filmLocSource.length > 0) {
					filmLocSource.forEach(src => {
						mergedSources.push(src);
					});

					geoLocSource.forEach(src => {
						mergedSources.push(src);
					});

					sourceArray = [...new Set(mergedSources)]; // Reduce source duplicates
				} else {
					filmLocSource.forEach(src => {
						mergedSources.push(src);
						sourceArray = mergedSources;
					});
				}
			}
		});

		// Get season and episode
		var season;
		var episode;

		if(seasonEpisode) {
			var season = seasonEpisode.first;
			var episode = seasonEpisode.second;
		} else {
			var season = null;
			var episode = null;
		}

		// Build marker
		marker = {
			'type': 'Feature',
			'properties': {
				'name': {
					'en':	name,
					'native': nativeName
				},
				'description': description,
				'website': website,
				'wikimedia': wikimedia,
				'wikipedia': wikipedia,
				'media': media,
				'seasonEpisode': {
					'season': season,
					'episode': episode
				},
				'source': sourceArray.sort().join(', '),
				'address': address,
				'openstreetmap': openstreetmap
			},
			'geometry': {
				'type': 'Point',
				'coordinates': [lon, lat]
			}
		};

		locations.push(marker);
	});
}

function toggleMarkers(marker) {
	C.MAP.eachLayer(layer => {
		if(layer._childCount) {
			layer.getAllChildMarkers().forEach(child => {
				child.setIcon(C.MARKER.DEFAULT);
			});
		}

		if(!layer.hasOwnProperty('_group') && layer.hasOwnProperty('_latlng') && layer != marker) {
			layer.setIcon(C.MARKER.DEFAULT);
		}
	});

	if(marker != undefined) {
		marker.setIcon(C.MARKER.ACTIVE);
	}
}

function displayMarkers() {
	function onEachFeature(feature, layer) {
		layer.on('click', function (loc) {
			showLocationContent(loc);
			toggleMarkers(this);
			this.bounce(1);
		});
	}

	// Show all markers on the map
	var jsonLayer = L.geoJSON(null, {
		onEachFeature: onEachFeature,
		pointToLayer: function(feature, latlng) {
			return L.marker(latlng, {
				icon: C.MARKER.DEFAULT
			})
		}
	});

	jsonLayer.addData(locations);

	var markers = L.markerClusterGroup({
		showCoverageOnHover: false,
		maxClusterRadius: 10,
		removeOutsideVisibleBounds: false // Needed to toggle active markers
	});

	markers.addLayer(jsonLayer);
	C.MAP.addLayer(markers);
	C.MAP.fitBounds(markers.getBounds().pad(0.5));
}

function getMarkerByName(name) {
	var marker;

	C.MAP.eachLayer(layer => {
		if(layer._childCount) {
			layer.getAllChildMarkers().forEach(child => {
				if(child.feature.properties.name.en == name) {
					marker = child;
				}
			});
		} else if(!layer.hasOwnProperty('_group') && layer.hasOwnProperty('_latlng')) {
			if(layer.feature.properties.name.en == name) {
				marker = layer;
			}
		}
	});

	return marker;
}

export { locations, buildMarkers, displayMarkers, getMarkerByName, toggleMarkers };
