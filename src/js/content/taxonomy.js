import { config } from '../core/config.js';
import * as C from '../core/constants.js';
import { displayMessage } from '../core/global.js';
import { displayError } from './content.js';

function getMediaList(type, offset = 0, sort = 'name') {
	if(type == 'movie') {
		type = 'm';
	} else {
		type = 'tv';
	}

	var url = config.api.fldbApiUrl + '/taxonomy_term/media/?filter[field_type]=' + type + '&page[offset]=' + offset + '&page[limit]=' + config.entriesPerPage + '&filter[status][value]=1&sort=' + sort;

	return fetch(url)
	.then (res => res.json())
	.then (json => {
		return json;
	})
	.catch(() => {
		displayMessage({
			type: 'error',
			message: 'We had problems loading the media list. Please try again later.'
		});
	});
}

function searchMediaEntries(key, sort = 'name') {
	var url = config.api.fldbApiUrl + '/taxonomy_term/media/?filter[name][operator]=CONTAINS&filter[name][value]=' + key + '&page[limit]=' + config.maxSearchResults + '&filter[status][value]=1&sort=' + sort + '&filter[status][value]=1';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

function getMediaUuid(type) {
	var param = '/' + type + '/';
	param = C.PATH.split(param).pop();
	var url = config.api.fldbRouterUrl + '?path=/' + type + '/' + param + '?filter[status][value]=1';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json.entity.uuid;
		})
		.catch(() => {
		displayMessage({
			type: 'error',
			message: 'We had problems loading the media list. Please try again later.'
		});
	});
}

function getNewestEntries(n=1) {
	var url = config.api.fldbApiUrl + '/taxonomy_term/media/?page[limit]=' + n + '&filter[status][value]=1&sort=-revision_created';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

function getRandomMedia() {
	// First we need to get the number of media entries available. We do that by fetching
	// The ID of the newest media taxonomy tag, and then subtract the number of source
	// taxonomy tags (as the ID increases not per vocabulary, but for taxonomies globally)
	function getMediaEntriesNumber() {
		var mediaUrl = config.api.fldbApiUrl + '/taxonomy_term/media/?page[limit]=1&filter[status][value]=1&sort=-revision_created';
		var sourceUrl = config.api.fldbApiUrl + '/taxonomy_term/source/?filter[status][value]=1&sort=-revision_created';
		var urlArray = [mediaUrl, sourceUrl];
		var urlArray = urlArray.map(url => fetch(url).then(res => res.json()));

		return Promise.all(urlArray)
			.then(res => {
				var id = res[0].data[0].attributes.drupal_internal__revision_id;
				var sourceLength = res[1].data.length;

				var mediaEntries = id - sourceLength;
				return mediaEntries;
			})
			.catch(err => console.log(err));
		}

	// Now that we have those numbers, we first get a random number between 1 and the returned
	// value, and simply return a JSON request with that randomly generated offset
	return getMediaEntriesNumber()
		.then(n => {
			var offset = Math.floor((Math.random() * n) + 1);
			var url = config.api.fldbApiUrl + '/taxonomy_term/media/?page[limit]=1&page[offset]=' + (offset - 1) + '&filter[status][value]=1&sort=-revision_created'; // -1 because JSON starts from 0 and not 1

			return fetch(url)
				.then(res => res.json())
				.then(json => {
					return json;
				})
				.catch(() => {
					displayMessage({
						type: 'error',
						message: 'We had problems loading the random entry. Please try again later.'
					});
				});
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the random entry. Please try again later.'
			});
		});
}

function getOfdbUuid(uuid) {
	var mediaUrl = config.api.fldbApiUrl + '/taxonomy_term/media/' + uuid + '?filter[status][value]=1';
	return fetch(mediaUrl)
		.then(res => res.json())
		.then(res => {
			return [uuid, res.data.attributes.field_ofdb];
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

function getMedia(type) {
	return getMediaUuid(type)
		.then(res => getOfdbUuid(res))
		.then(res => {
			return res;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

export { getMediaList, getNewestEntries, getRandomMedia, searchMediaEntries, getMedia };
