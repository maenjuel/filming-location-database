import { config } from '../core/config.js';

function getWikimediaImage(url) {
	var fileName = url.split('wiki/').pop();
	var url = config.api.wikimediaApiUrl + '?action=query&titles=' + fileName + '&prop=imageinfo&iiprop=extmetadata|url&iiurlwidth=800&origin=*&format=json';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return mapWikimediaImageData(json);
		});
}

function mapWikimediaImageData(image) {
	// First we need to get the pageID before we can get the data
	var id = Object.keys(image.query.pages);

	// Now we get the image data
	var imageinfo = image.query.pages[id].imageinfo[0];
	var title = imageinfo.extmetadata.ObjectName.value;
	var url = imageinfo.thumburl;
	var artist = imageinfo.extmetadata.Artist;
	var licenseName = imageinfo.extmetadata.LicenseShortName;
	var licenseUrl = imageinfo.extmetadata.LicenseUrl;

	if(artist != null) {
		artist = artist.value
	}

	if(licenseName != null) {
		licenseName = licenseName.value
	}

	if(licenseUrl != null) {
		licenseUrl = licenseUrl.value
	}

	return {
		'title': title,
		'url': url,
		'artist': artist,
		'licenseName': licenseName,
		'licenseUrl': licenseUrl
	}
}

export { getWikimediaImage };
