import * as C from '../core/constants.js';
import { isMobile, displayTitle } from '../core/global.js';
import { submitLocation } from '../components/forms.js';
import { displayError, showRandomMedia, displayLocations, switchToMedia } from './content.js';
import { buildMediaList } from './mediaLists.js';
import { getRandomMedia } from './taxonomy.js';

function displayPage() {
	if(C.PATH == '/') {
		// We only need the title for the home page
		displayTitle('Welcome, friend!');
		showRandomMedia();

		// Add event listener for "back" link
		var backLink = document.getElementById('back');
		backLink.addEventListener('click', switchToMedia);
	} else {
		// If we're not on the home page, get the content accordingly
		fetch(C.CONTENT)
			.then(res => res.json())
			.then(json => {
				json = json.page;

				json.forEach(page => {
					// Add the content depending on the path
					if(page.path == C.PATH) {
						C.ELEMENT.PAGE.TITLE.innerHTML = typogr.typogrify(page.title);
						C.ELEMENT.PAGE.CONTENT.innerHTML = typogr.typogrify(page.content);

						// Show page specific content
						 if(C.PATH == '/movies') {
							C.ELEMENT.PAGE.CONTENT.innerHTML += '<div id="list"><ul id="movies"></ul><div id="pagination"></div></div>';
							buildMediaList('movie');
						} else if(C.PATH == '/tv-shows') {
							C.ELEMENT.PAGE.CONTENT.innerHTML += '<div id="list"><ul id="tv-shows"></ul><div id="pagination"></div></div>';
							buildMediaList('tv-show');
						} else if(C.PATH == '/submit-location') {
							var submitButton = document.getElementById('submit-form');
							submitButton.addEventListener('click', function() {
								submitLocation(event);
							});
						}

						displayTitle(page.title); // Update page title
					}
				});

				// If there is no title or content, we display an error message
				if((C.ELEMENT.PAGE.TITLE.innerHTML == '') && (C.ELEMENT.PAGE.CONTENT.innerHTML == '')) {
					displayError();
				}
			})
			.catch(() => displayError());
		}
}

export { displayPage };
