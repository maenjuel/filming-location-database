import { config } from '../core/config.js';
import * as C from '../core/constants.js';
import { showLoader } from '../components/loader.js';
import { displayError } from './content.js';
import { getMediaList } from './taxonomy.js';

function displayList(type, res, offset = 0) {
	var data = res.data;
	var links = res.links;

	var listContainer = document.getElementById('list');
	var list = listContainer.getElementsByTagName('ul')[0];

	list.innerHTML = ''; // Clear list (necessary for pagination)

	data.forEach(media => {
		var id = media.id;
		var path = media.attributes.path.alias;
		var title = media.attributes.name;

		list.innerHTML += '<li><a href="' + path + '" title="' + title + '">' + typogr.typogrify(title) + '</a></li>';
	});

	// Add pagination if there is more than one page
	if(links.prev != undefined || links.next != undefined) {
		var buttons;

		if(links.prev == undefined && links.next != undefined) {
			buttons = 'next';
		} else if (links.prev != undefined && links.next == undefined) {
			buttons = 'prev';
		} else {
			buttons = 'both';
		}

		getPagination(type, buttons, offset);
	}
}

function getPagination(type, buttons = 'next', offset = 0) {
	var paginationContainer = document.getElementById('pagination');

	if(buttons == 'next') {
		paginationContainer.innerHTML = '<span>Pagination:</span><a class="button secondary" id="next" onclick="buildMediaList(\'' + type + '\', ' + (offset + config.entriesPerPage) + ')" href="#">&rsaquo;</a>';
	} else if(buttons == 'prev') {
		paginationContainer.innerHTML = '<span>Pagination:</span><a class="button secondary" id="prev" onclick="buildMediaList(\'' + type+ '\', ' + (offset - config.entriesPerPage) + ')" href="#">&lsaquo;</a>';
	} else {
		paginationContainer.innerHTML = '<span>Pagination:</span><a class="button secondary" id="prev" onclick="buildMediaList(\'' + type+ '\', ' + (offset - config.entriesPerPage) + ')" href="#">&lsaquo;</a> <a class="button secondary" id="next" onclick="buildMediaList(\'' + type+ '\', ' + (offset + config.entriesPerPage) + ')"  href="#">&rsaquo;</a>';
	}
}

function buildMediaList(type, offset) {
	// Display loader
	var listContainer = document.getElementById('list');
	var list = listContainer.getElementsByTagName('ul')[0].id;
	showLoader(list); // Display loader while getting entries

	// Build list
	getMediaList(type, offset)
	.then(res => displayList(type, res, offset))
	.catch(() => {
		var listContainer = document.getElementById('list');
		var list = listContainer.getElementsByTagName('ul')[0];

		list.innerHTML = ''; // Clear list (necessary for pagination)
		list.innerHTML += typogr.typogrify('We had problems getting the media entries. Please try again.');
	});
}

export { buildMediaList };
