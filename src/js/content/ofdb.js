import * as C from '../core/constants.js';
import { config } from '../core/config.js';
import { displayMessage } from '../core/global.js';
import { clearFields, displayFilmInfo } from './content.js';

function getFilmInfo(type, uuid) {
	var param;

	if (type == 'm') {
		param = 'movie';
	} else {
		param = 'tv_show';
	}

	var url = config.api.ofdbApiUrl + '/node/' + param + '/' + uuid + '?filter[status][value]=1';

	return fetch(url)
		.then(res => res.json())
		.then(res => {
			return res;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media information. Please try again later.'
			});
		});
}

export { getFilmInfo };
