import { config } from '../core/config.js';
import * as C from '../core/constants.js';
import { displayMessage } from '../core/global.js';
import { showLoader } from '../components/loader.js';
import { locations } from './markers.js';
import { getMedia } from './taxonomy.js';
import { displayFilmInfo, displayLocationList } from './content.js';
import { getFilmInfo } from './ofdb.js';
import { buildMarkers, displayMarkers } from './markers.js';

// Start with a set of empty arrays we populate later
var filmingLocations = [];
var geographicLocations = [];

async function getFilmingLocationsData(url) {
	let allData = [];
	let allIncluded = [];
	let nextPageUrl = url;
	let finalResponse = [];

	try {
		while (nextPageUrl) {
			const response = await fetch(nextPageUrl);
			const data = await response.json();

			// Add the current page's data to allData
			allData = allData.concat(data.data);

			// Add current page's included data, if present
			if (data.included) {
				allIncluded = allIncluded.concat(data.included);
			}

			// Save final response structure; only once needed
			if (!finalResponse.jsonapi) {
				finalResponse = {
					jsonapi: data.jsonapi || {},
					links: data.links || {},
				};
			}

			// Check if there is a next page
			const links = data.links;
				if (links && links.next) {
					nextPageUrl = links.next.href;
				} else {
					nextPageUrl = null;
				}
			}

		// Combine all data and included data
		finalResponse.data = allData;
		if (allIncluded.length > 0) {
			finalResponse.included = allIncluded;
		}

		// Return all data for further use
		return finalResponse;
	} catch (err) {
		console.error(err);
  }
}

function buildFilmingLocations(res) {
	// Start with an empty object and define data points
	var loc = {};
	var data = res.data;
	var included = res.included;

	// build location object and add it to an object, and then to an array
	data.forEach(loc => {
		var id = loc.id;
		var name = loc.attributes.title;
		var description = loc.attributes.body;
		var website = loc.attributes.field_website;
		var wikimedia = loc.attributes.field_wikimedia_commons;
		var wikipedia = loc.attributes.field_wikipedia;
		var geoLoc = loc.relationships.field_geographic_location.data.id;
		var media = included[0].attributes.name;
		var seasonEpisode = loc.attributes.field_season_episode;
		var source = loc.relationships.field_source.data;

		if(description != null) {
			description = loc.attributes.body.processed;
		}

		if(website != null) {
			website = loc.attributes.field_website.uri;
		}

		if(wikimedia != null) {
			wikimedia = wikimedia.uri;
		}

		if(wikipedia != null) {
			wikipedia = wikipedia.uri;
		}

		// Add sources to an array
		var sourceArray = [];

		source.forEach(src => {
			included.forEach(inc => {
				if((src.id == inc.id) && (!sourceArray.includes(inc.attributes.name))) {
					sourceArray.push(inc.attributes.name);
				}
			})
		});

		loc = {
			'id': id,
			'name': name,
			'description': description,
			'website': website,
			'wikimedia': wikimedia,
			'wikipedia': wikipedia,
			'geoLoc': {
				'id': geoLoc
			},
			'media': media,
			'seasonEpisode': seasonEpisode,
			'source': sourceArray
		}

		filmingLocations.push(loc);
	});
}

function getGeographicLocationsData() {
	var urlArray = []; // An empty array we'll populate with URLs

	filmingLocations.forEach(filmLoc => {
		// Get geoLocId and build the url with it
		var geoLocId = filmLoc.geoLoc.id;
		var geoLocUrl = config.api.fldbApiUrl + '/node/geographic_location/' + geoLocId + '?include=field_source' + '&filter[status][value]=1';
		urlArray.push(geoLocUrl);
	});

	var urlArray = urlArray.map(url => fetch(url).then(res => res.json()));

	return Promise.all(urlArray)
		.then(res => {
			return res;
		})
		.catch(err => console.log(err));
}

function buildGeographicLocationsData(res) {
	var loc = {}; // An empty object we'll populate now

	res.forEach(loc => {
		var data = loc.data;
		var included = loc.included;

		var id = data.id;
		var name = data.attributes.title;
		var nativeName = data.attributes.field_native_name;
		var description = data.attributes.body;
		var website = data.attributes.field_website;
		var wikimedia = data.attributes.field_wikimedia_commons;
		var wikipedia = data.attributes.field_wikipedia;
		var address = data.attributes.field_address;
		var lat = data.attributes.field_location.lat;
		var lon = data.attributes.field_location.lon;
		var openstreetmap = data.attributes.field_openstreetmap;
		var source = data.relationships.field_source.data;

		if(description != null) {
			description = description.processed;
		}

		if(website != null) {
			website = website.uri;
		}

		if(wikimedia != null) {
			wikimedia = wikimedia.uri;
		}

		if(wikipedia != null) {
			wikipedia = wikipedia.uri;
		}

		if(openstreetmap != null) {
			openstreetmap = openstreetmap.uri;
		}

		// Build the address
		var combinedAddress;

		if(address != null) {
			var combinedAddress = ''; // If we don't set the string like this and we don't have a address_line1, we'll get "undefined" rendered in the string

			if(address.address_line1 != '') {
				combinedAddress += address.address_line1 + '\n';
			}

			if(address.address_line2 != '') {
				combinedAddress += address.address_line2 + '\n';
			}

			if(address.postal_code != null) {
				combinedAddress += address.postal_code + ' ';
			}

			if(address.locality != null) {
				combinedAddress += address.locality + ' ';
			}

			if(address.administrative_area != null && address.administrative_area != '') {
				combinedAddress += '\n' + address.administrative_area;
			}

			if(address.country_code != null) {
				if(combinedAddress == ' ') {
					combinedAddress += address.country_code;
				} else {
					combinedAddress += ' (' + address.country_code + ')';
				}
			}
		}

		// Add sources to an array
		var sourceArray = [];

		source.forEach(src => {
			included.forEach(inc => {
				if(src.id == inc.id) {
					sourceArray.push(inc.attributes.name);
				}
			})
		});

		loc = {
			'id': id,
			'name': {
				'en': name,
				'native': nativeName
			},
			'description': description,
			'website': website,
			'wikimedia': wikimedia,
			'wikipedia': wikipedia,
			'address': combinedAddress,
			'location': {
				'lat': lat,
				'lon': lon
			},
			'openstreetmap': openstreetmap,
			'source': sourceArray
		}

		geographicLocations.push(loc);
	});
}

function getLocationInfo(uuid) {
	var url = config.api.fldbApiUrl + '/node/filming_location/?filter[field_media.id]=' + uuid + '&filter[status][value]=1&include=field_media,field_source';

	getFilmingLocationsData(url)
		.then(res => buildFilmingLocations(res))
		.then(() => getGeographicLocationsData())
		.then(res => buildGeographicLocationsData(res))
		.then(() => {
			buildMarkers();
			displayLocationList();
		})
		.then(() => {
			if(locations.length > 0) {
				displayMarkers();
			} else {
				displayMessage({
					type: 'error',
					message: 'No locations found for this entry. Sorry for the inconvenience.'
				});
			}
		})
}

function displayLocations(type) {
	// Show loader
	showLoader('location-list', {
		coverAll: true
	});

	// Get all locations on the map
	getMedia(type)
	.then(res => {
		// Get the TV show/movie information
		getFilmInfo(type, res[1])
		.then(res => displayFilmInfo(res))
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media information. Please try again later.'
			});
		});

		// Get the content for the markers
		getLocationInfo(res[0]);
	});
}

export { filmingLocations, geographicLocations, getLocationInfo, displayLocations };
