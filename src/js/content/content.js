import * as C from '../core/constants.js';
import { config } from '../core/config.js';
import { isMobile, isWide, displayTitle, toggleOverlay, close, displayMessage, headerOffset } from '../core/global.js';
import { getLocationInfo, displayLocations } from './locations.js';
import { locations, getMarkerByName, toggleMarkers } from './markers.js';
import { displayPage } from './page.js';
import { getFilmInfo } from './ofdb.js';
import { getRandomMedia } from './taxonomy.js';
import { getWikimediaImage } from './wikimedia.js';

// Determine content type based on path and call appropriate function
function getContent() {
	if (C.PATH.includes('/m/')) {
		displayLocations('m');

		// Add event listener for "back" link
		var backLink = document.getElementById('back');
		backLink.addEventListener('click', switchToMedia);
	} else if (C.PATH.includes('/tv/')) {
		displayLocations('tv');

		// Add event listener for "back" link
		var backLink = document.getElementById('back');
		backLink.addEventListener('click', switchToMedia);
	} else {
		displayPage();
	}

	// Set content offset
	if(isMobile()) {
		C.ELEMENT.CONTENT.style.top = headerOffset() + config.contentMargin +'px';
	} else {
		C.ELEMENT.CONTENT.style.top = headerOffset() + 'px';
		C.ELEMENT.CONTENT.style.marginBottom = headerOffset() + 'px';
	}

	// Add event listener for the user to close the content box (on mobile)
	var closeContentButton = document.getElementById('close-content');
	closeContentButton.addEventListener('click', closeContent);
}

function showContentBox() {
	C.ELEMENT.CONTENT.classList.add('show');
}

function aboutBoxOffset() {
	C.ELEMENT.ASIDE.style.top = headerOffset() + 'px';
}

// Displaying the newest movie or TV show
function showRandomMedia() {
	getRandomMedia()
		.then(json => {
			json = json.data[0];
			var id = json.id;
			var type = json.attributes.field_type;
			var odfbId = json.attributes.field_ofdb;
			var path = json.attributes.path.alias;

			getLocationInfo(id); // Display markers

			// Get film info
			getFilmInfo(type, odfbId)
			.then(res => {
				json = res.data;
				var title = json.attributes.title;
				var originalTitle = json.attributes.field_original_title;
				var plot = json.attributes.body;
				var poster = json.attributes.field_poster;
				var date = json.attributes.field_release_date;

				// Display title
				C.ELEMENT.MEDIA.TITLE.innerHTML = typogr.typogrify(title);

				// Add notification tooltip
				var containerElement = document.getElementById('media');
				var tooltip = document.createElement('span');
				tooltip.classList.add('tooltip', 'right', 'show');
				tooltip.innerHTML = 'We found this random film for you. <i class="icon material-icons md-18 md-close"></i>';

				C.ELEMENT.MEDIA.TITLE.parentNode.insertBefore(tooltip, C.ELEMENT.MEDIA.TITLE.nextSibling);

				var closeButton = tooltip.lastChild;
				closeButton.addEventListener('click', close);

				// Display original title, if available
				if(originalTitle != null) {
					C.ELEMENT.MEDIA.NATIVE.innerHTML += '<i class="icon material-icons md-18 md-language"></i>' + typogr.typogrify(originalTitle);
				}

				// Get poster, if available
				if(poster != null) {
					poster = poster.uri.split('File:').pop();
					C.ELEMENT.MEDIA.CONTENT.innerHTML += '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="Poster" id="poster" />';

					poster = document.getElementById('poster');
				}

				// Get plot, if available
				if(plot == null) {
					if(type == 'm') {
						C.ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this movie.');
					} else if(type == 'tv') {
						C.ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this TV show.');
					}
				} else {
					C.ELEMENT.MEDIA.CONTENT.innerHTML += '<p>' + typogr.typogrify(plot.value) + '</p>';
				}
			})
			.catch(() => {
				displayMessage({
					type: 'error',
					message: 'We had problems loading the media information. Please try again later.'
				});
			});
		})
		.catch(err => console.log(err));
}

// Add event listener for spoilers
function initSpoilers() {
	var spoilers = document.getElementsByClassName('spoiler');

	for(let spoiler of spoilers) {
		var button = spoiler.getElementsByTagName('button')[0];
		button.addEventListener('click', function() {
			revealSpoiler(this);
		});
	}
}

// Show spoilers
function revealSpoiler(el) {
	var spoiler = el.parentElement;
	spoiler.classList.add('show-spoiler');
	el.remove();
}

// Hide content
function closeContent() {
	C.ELEMENT.CONTENT.classList.remove('show');
}

// Display error if there is no content to show
function displayError() {
	displayTitle('How the turntables...'); // Display page title
	C.ELEMENT.PAGE.TITLE.innerHTML = 'How the turntables...';
	C.ELEMENT.PAGE.CONTENT.innerHTML = typogr.typogrify('<p>This doesn\'t sound right. Something is wrong, please try again or go to the <a href="/" title="Home page">home page</a>.</p>');
}

function clearFields() {
	C.ELEMENT.LOCATION.ADDRESS.innerHTML = '';
	C.ELEMENT.LOCATION.CONTENT.innerHTML = '';
	C.ELEMENT.LOCATION.IMAGE.innerHTML = '';
	C.ELEMENT.LOCATION.LINKS.innerHTML = '';
	C.ELEMENT.LOCATION.MEDIA.innerHTML = '';
	C.ELEMENT.LOCATION.SEASONEPISODE.innerHTML = '';
	C.ELEMENT.LOCATION.NATIVE.innerHTML = '';
	C.ELEMENT.LOCATION.SOURCE.innerHTML = '';
	C.ELEMENT.LOCATION.TITLE.innerHTML = '';
}

function clearErrors() {
	var messages = document.getElementsByClassName('messages');
	for (let el of messages) {
		el.innerHTML = '';
		el.classList.add('empty');
	}
}

function switchToLocation() {
	var mediaContainer = document.getElementById('media');
	var locationContainer = document.getElementById('location');

	locationContainer.classList.add('show');
	mediaContainer.classList.remove('show');

	// To display the scrollbar correctly, we must show/hide the proper containers
	setTimeout(() => { mediaContainer.style.width = 0 + 'px'; }, 300); // value is equal to the CSS transition
	locationContainer.style.width = '100%';
}

function switchToMedia() {
	var mediaContainer = document.getElementById('media');
	var locationContainer = document.getElementById('location');

	mediaContainer.classList.add('show');
	locationContainer.classList.remove('show');

	setTimeout(() => { locationContainer.style.width = 0 + 'px'; }, 300); // value is equal to the CSS transition
	mediaContainer.style.width = '100%';

	toggleMarkers();
}

function displayFilmInfo(json) {
	var title = json.data.attributes.title;
	var originalTitle = json.data.attributes.field_original_title;
	var plot = json.data.attributes.body;
	var poster = json.data.attributes.field_poster;
	var date = json.data.attributes.field_release_date;
	var type = json.data.type;

	clearFields(); // Clear fields

	// Remove error messages if it's not no locations found
	var messagesContainer = C.ELEMENT.MEDIA.CONTENT.getElementsByClassName('messages')[0];
	C.ELEMENT.MEDIA.TITLE.innerHTML = typogr.typogrify(title); // Display title

	// Display original title, if available
	if(originalTitle != null) {
		C.ELEMENT.MEDIA.NATIVE.classList.remove('empty');
		C.ELEMENT.MEDIA.NATIVE.innerHTML = '<i class="icon left material-icons md-18 md-language"></i>' + typogr.typogrify(originalTitle);
	} else {
		C.ELEMENT.MEDIA.NATIVE.classList.add('empty');
	}

	// Get poster, if available
	if(poster != null) {
		poster = poster.uri.split('File:').pop();
		C.ELEMENT.MEDIA.CONTENT.innerHTML = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="Poster" id="poster" />';
	}

	// Get plot, if available
	if(plot == null) {
		if(type == 'node--movie') {
			C.ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this movie.');
		} else if(type == 'node--tv_show') {
			C.ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this TV show.');
		}
	} else {
		C.ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify(plot.value);
	}

	displayTitle(title); // Display page title
}

function displayLocationList() {
	var locationContainer = document.getElementById('location-list');
	if(locations.length > 1) {
		locationContainer.innerHTML = '<p>' + locations.length + ' locations found:</p><ul></ul>';
	} else {
		locationContainer.innerHTML = '<p>' + locations.length + ' location found:</p><ul></ul>';
	}

	var listContainer = locationContainer.getElementsByTagName('ul')[0];
	var list = [];

	// First we add all locations to an array, that we, then, sort
	locations.forEach(loc => {
		list.push(loc.properties.name.en);
	});
	list.sort();

	// Now we create a list from those entries
	list.forEach(loc => {
		listContainer.innerHTML += '<li><i class="icon left material-icons-outline md-18 md-place"></i><a alt="' + loc.replace(/"/g, '\'\'') + '">' + typogr.typogrify(loc) + '</a></li>';
	});

	// Finally, we add an event listener to that element
	var listElements = listContainer.children;
	for (let li of listElements) {
		li.childNodes[1].addEventListener('click', function() {
			// First we move the map view to the marker
			var rawName = li.childNodes[1].attributes[0].value.replace(/''/g, '"'); // We need the raw string of the title to get the correct marker
			var marker = getMarkerByName(rawName);

			if(list.length > 1) {
				var markerLatLng = marker.getLatLng();
				C.MAP.setView(markerLatLng, 16);
			} else {
				marker.bounce(1);
			}

			// Then we display the content and show the necessary animations for the marker
			toggleMarkers(marker);
			showLocationContent(marker);

			if(marker._icon == undefined){
				setTimeout(() => {
					marker.__parent.__parent.__parent.spiderfy();
				}, 400);
			}
		});
	}
}

function showLocationContent(loc) {
	clearFields(); // Clear fields
	clearErrors(); // Clear error messages

	// Define data points
	var properties;
	var latlon;

	if(loc.sourceTarget != undefined) {
		properties = loc.sourceTarget.feature.properties;
		latlon = loc.sourceTarget.feature.geometry.coordinates;
	} else {
		properties = loc.feature.properties;
		latlon = loc.feature.geometry.coordinates;
	}

	var description = properties.description;
	if(description == null) {
		description = 'We don\'t have a description for this location.';
	}

	var name = properties.name.en;
	var nativeName = properties.name.native;
	var website = properties.website;
	var wikimedia = properties.wikimedia;
	var wikipedia = properties.wikipedia;
	var media = properties.media;
	var seasonEpisode = properties.seasonEpisode;
	var source = properties.source;
	var address = properties.address;
	var openstreetmap = properties.openstreetmap;

	// Now we get the Wikimedia Picture, if available
	if(wikimedia != undefined) {
		getWikimediaImage(wikimedia)
			.then(img => {
				// Get image title and create alt tag, if available
				var imageAlt;
				var imageTitle;
				if(img.title) {
					imageAlt = 'alt="' + img.title.replace(/(<([^>]+)>)/ig,'') + '" ';
					imageTitle = '<a href="' + wikimedia + '" title="View picture on Wikimedia Commons" target="_blank">' + typogr.typogrify(img.title.replace(/(<([^>]+)>)/ig,'')) + '</a>. ';
				} else {
					imageAlt = '';
					imageTitle = '';
				}

				C.ELEMENT.LOCATION.IMAGE.innerHTML = '<img src="' + img.url + '" ' + imageAlt + '"/>'; // Add image

				// Build attribution text
				var attribution;
				if(img.licenseUrl == undefined && img.artist == undefined) {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span>' + imageTitle + img.licenseName + '</span></div>';
				} else if(img.licenseUrl == undefined && img.artist != undefined) {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span>' + imageTitle + img.licenseName + '/' + img.artist + '</span></div>';
				} else if(img.licenseUrl != undefined && img.artist == undefined) {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span><a href="' + img.licenseUrl + '" title="' + img.licenseName + '">' + imageTitle + img.licenseName + '</a></span></div>';
				} else {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span><a href="' + img.licenseUrl + '" title="' + img.licenseName + '">' + imageTitle + img.licenseName + '</a>/' + img.artist + '</span></div>';
				}

				// Finally we modify links that don't open in a new window so they do
				var dummyDOM = document.createElement( 'html' );
				dummyDOM.innerHTML = attribution;
				var links = dummyDOM.getElementsByTagName('a');

				for(let link of links) {
					link.target = "_blank";
				}

				attribution = dummyDOM.getElementsByTagName('body')[0].innerHTML;
				C.ELEMENT.LOCATION.IMAGE.innerHTML += attribution; // Display attribution
			})
			.catch(() => {
				displayMessage({
					type: 'info',
					message: 'We had problems loading the image from Wikimedia Commons. Please try again later.'
				});
			});
		}

	// Then we add the spoiler button to description
	if(description.includes('<span class="spoiler">')) {
		var description = description.replace('</span>', '<button class="secondary">reveal spoiler</button></span>');
	}

	// Display the content
	C.ELEMENT.LOCATION.TITLE.innerHTML = typogr.typogrify(name);
	C.ELEMENT.LOCATION.CONTENT.innerHTML = typogr.typogrify(description);

	// Display OSM link
	if(openstreetmap != null) {
		C.ELEMENT.LOCATION.LINKS.innerHTML = '<a href="' + openstreetmap + '" alt="Open on OpenStreetMap" title="Open on OpenStreetMap" target="_blank"><i class="icon left material-icons-outline md-18 md-gps_fixed"></i></a>';
	} else {
		C.ELEMENT.LOCATION.LINKS.innerHTML = '<a href="https://www.openstreetmap.org/?mlat=' + latlon[1] + '&mlon=' + latlon[0] + '#map=17/' + latlon[1] + '/' + latlon[0] + '" alt="Open on OpenStreetMap" title="Open on OpenStreetMap" target="_blank"><i class="icon left material-icons-outline md-18 md-gps_fixed"></i></a>';
	}

	// Display optional fields
	if(website != null || wikipedia != null || address != null) {
		if(website != null) {
			C.ELEMENT.LOCATION.LINKS.innerHTML += '<a href="' + website + '" alt="Go to official website" title="Go to official website" target="_blank"><i class="icon left material-icons-outline md-18 md-open_in_browser"></i></a>';
		}

		if(wikipedia != null) {
			C.ELEMENT.LOCATION.LINKS.innerHTML += '<a href="' + wikipedia + '" alt="Read related Wikipedia article" title="Read related Wikipedia article" target="_blank"><i class="icon left material-icons-outline md-18 md-school"></i></a>';
		}

		if(address != null) {
			C.ELEMENT.LOCATION.ADDRESS.innerHTML += '<i class="icon left material-icons-outline md-pin_drop"></i><span>' + typogr.typogrify(address.replace(/\n/g, '<br />')) + '</span>';
			C.ELEMENT.LOCATION.ADDRESS.classList.remove('empty');
		} else {
			C.ELEMENT.LOCATION.ADDRESS.classList.add('empty');
		}
	}

	C.ELEMENT.LOCATION.LINKS.classList.remove('empty'); // Remove hidden class vom links container, as it always has content

	// Display metadata
	C.ELEMENT.LOCATION.MEDIA.innerHTML = '<i class="icon left material-icons-outline md-movie"></i>' + '<a onclick="switchToMedia()" title="' + media + '">' + typogr.typogrify(media) + '</a>';

	C.ELEMENT.LOCATION.SOURCE.innerHTML = '<i class="icon left material-icons-outline md-format_quote"></i>' + '<span>' + typogr.typogrify(source) + '</span>';

	if(C.PATH.includes('/tv/')) {
		var season;
		var episode;

		if(seasonEpisode.season && seasonEpisode.season.includes('-')) {
			var season = seasonEpisode.season.split('-');
			var season = 'S' + season[0].padStart(2,0) + '-' + season[1].padStart(2,0);
		} else if (seasonEpisode.season && !seasonEpisode.season.includes('-')) {
			var season = 'S' + seasonEpisode.season.padStart(2,0);
		} else {
			var season = null;
		}

		if(seasonEpisode.episode && seasonEpisode.episode.includes('-')) {
			var episode = seasonEpisode.episode.split('-');
			var episode = 'E' + episode[0].padStart(2,0) + '-' + episode[1].padStart(2,0);
		} else if (seasonEpisode.episode && !seasonEpisode.episode.includes('-')) {
			var episode = 'E' + seasonEpisode.episode.padStart(2,0);
		} else {
			var episode = null;
		}

		if(season && episode) {
			C.ELEMENT.LOCATION.SEASONEPISODE.innerHTML = '<i class="icon left material-icons-outline md-format_list_numbered"></i>' + '<span>' + typogr.typogrify(season + episode) + '</span>';
		} else if(season && !episode) {
			C.ELEMENT.LOCATION.SEASONEPISODE.innerHTML = '<i class="icon left material-icons-outline md-format_list_numbered"></i>' + '<span>' + typogr.typogrify(season) + '</span>';
		}
	}

	// Display native name, if available
	if(nativeName != null) {
		C.ELEMENT.LOCATION.NATIVE.innerHTML = '<i class="icon left material-icons-outline md-18 md-language"></i><span>' + typogr.typogrify(nativeName) + '</span>';
		C.ELEMENT.LOCATION.NATIVE.classList.remove('empty');
	} else {
		C.ELEMENT.LOCATION.NATIVE.classList.add('empty');
	}

	initSpoilers();

	if(isMobile) {
		showContentBox();
	}

	switchToLocation(); // Slide effect between media and location
}

export { clearFields, getContent, closeContent, displayError, displayFilmInfo, displayLocationList, aboutBoxOffset, showRandomMedia, showLocationContent, displayLocations, switchToMedia };
