import { showLoader } from './loader.js';
import { getNewestEntries, searchMediaEntries } from '../content/taxonomy.js';

// Container elements
var searchContainer = document.getElementById('search-container');
var searchBox = document.getElementById('search-box');
var suggestionsContainer = document.getElementById('search-suggestions');
var suggestionsList = document.getElementById('suggestions');

function buildSuggestions(key) {
	searchMediaEntries(key)
		.then(res => {
			if(res.data && res.data.length == 0) {
				var searchbox = document.getElementById('search-box');
				var searchValue = searchbox.value;
				suggestionsList.innerHTML = '<div class="messages"><i class="icon left material-icons-outline md-18 md-sentiment_dissatisfied"></i>Oops, we found no title matching ' + typogr.typogrify('"' + searchValue + '"') + '</span>.<br /><a href="/submit-location" alt="Submit location" title="Submit location"><i class="icon left material-icons md-18 md-add_location_alt"></i>Add new location</span></a></div>';
			} else {
				displaySuggestions(res);
			}
		})
		.catch(() => {
			suggestionsList.innerHTML = 'We had troubles getting the search results. Please reload the page and try again.'; // Clear the suggestions list before populating it
		});
}

function displaySuggestions(res) {
	suggestionsList.innerHTML = ''; // Clear the suggestions list before populating it
	var data = res.data;

	data.forEach(entry => {
		var icon;
		var path = entry.attributes.path.alias;
		var name = entry.attributes.name;
		var type = entry.attributes.field_type;

		if(type == 'm') {
			icon = '<i class="icon left material-icons-outline md-18 md-theaters"></i>';
		} else {
			icon = '<i class="icon left material-icons-outline md-18 md-live_tv"></i>';
		}

		suggestionsList.innerHTML += '<li><a href="' + path + '" title="' + name + '">' + icon + typogr.typogrify(name) + '</a></li>';
	});
}

function search() {
	var newestEntries = document.getElementById('newest');
	if(searchBox.value.length > 0 && searchBox.value.length < 3) {
		newestEntries.classList.remove('show');
		suggestionsList.innerHTML = 'Please type 3 or more characters...';
	} else if(searchBox.value.length >= 3) {
		newestEntries.classList.remove('show');
		showLoader('suggestions');
		buildSuggestions(searchBox.value);
	} else {
		newestEntries.classList.add('show');
		suggestionsList.innerHTML = '';
	}
}

function displayNewestEntries() {
	var entriesContainer = document.getElementById('newest-entries');

	getNewestEntries(3)
		.then(json => {
			json = json.data;
			json.forEach(media => {
				var name = media.attributes.name;
				var type = media.attributes.field_type;
				var path = media.attributes.path.alias;
				var created = new Date(media.attributes.revision_created);
				let date = created.getUTCDate() + '.' + (created.getMonth() + 1) + '.' + created.getUTCFullYear();

				var icon;
				if(type == 'm') {
					icon = '<i class="icon left material-icons-outline md-18 md-theaters"></i>';
				} else {
					icon = '<i class="icon left material-icons-outline md-18 md-live_tv"></i>';
				}

				entriesContainer.innerHTML += '<li><a href="' + path + '" title="' + name + '">' + icon +  typogr.typogrify(name) + '</a> (' + date + ')</li>';
			});
		});
}

function initSearch() {
	searchBox.addEventListener('keyup', search);
	displayNewestEntries();
}

function toggleSearchSuggestionsBoxByClick() {
	document.body.addEventListener('click', function (event) {
		// Hide search suggestions if clicked anywhere outside of searchSuggestions
		if(!searchContainer.contains(event.target) && suggestionsContainer.classList.contains('show')) {
			suggestionsContainer.classList.remove('show');
		}

		// Show search suggestions when search field is clicked and suggestions are there
		if(searchBox.contains(event.target) && !suggestionsContainer.classList.contains('show')) {
			suggestionsContainer.classList.add('show');
		}
	});
}

export { toggleSearchSuggestionsBoxByClick, initSearch };
