import { displayMessage } from '../core/global.js';

function clearFields(id, msg) {
	var form = document.getElementById(id);
	var errorContainers = form.getElementsByClassName('error');
	// Remove error messages
	for (var item of errorContainers) {
		item.innerHTML = '';
	}

	// Display success message
	displayMessage({
		type: 'success',
		message: msg
	});
}

function sendMail(file, options, msg, id) {
	var form = document.getElementById(id);

	fetch(file, options)
		.then((res) => {
			clearFields(id, msg);
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'Error sending e-mail. Please try again.'
			});
		});
}

function submitLocation(event) {
	event.preventDefault(); // Prevent from submit

	// Get form data
	var formID = 'submit-location';
	var form = document.getElementById(formID);
	var formData = new FormData(form);

	// Stuff needed to fetch()
	var file = '/assets/submitLocation.php';
	var options = {
		method: 'POST',
		body: formData
	};

	// Messages
	var honeypotMessage = 'Whoa, hold on, spammer!';
	var successMessage = 'Location submitted. Thank you for improving the filming location database!<br /><br /><em>Note: The location will only be online after being reviewed by a moderator.</em>';

	// We add a variable 'halt' set to false. If we encounter a validation error
	// We set the variable to true and won't send the e-mail with that value
	var halt = false;

	// form fields
	var locationName = document.getElementById('location-name').value;
	var nativeName = document.getElementById('native-name').value;
	var description = document.getElementById('description').value;
	var media = document.getElementById('media').value;
	var lat = document.getElementById('lat').value;
	var lon = document.getElementById('lon').value;
	var openstreetmap = document.getElementById('openstreetmap').value;
	var flickrImage = document.getElementById('flickr-image').value;
	var commonsImage = document.getElementById('commons-image').value;
	var wikipedia = document.getElementById('wikipedia').value;
	var website = document.getElementById('website').value;
	var source = document.getElementById('source').value;
	var email = document.getElementById('email').value;

	// Honeypot field
	var honeypotField = document.getElementById('url').value;

	// First we remove old error messages
	var errorContainers = form.getElementsByClassName('error');
	for (var item of errorContainers) {
		item.innerHTML = '';
	}

	// Form validation
	if(!locationName) {
		var locationNameContainer = document.getElementById('location-name-container');
		locationNameContainer.lastChild.innerHTML = 'Please add location name';

		halt = true;
	}

	if(!description) {
		var descriptionContainer = document.getElementById('description-container');
		descriptionContainer.lastChild.innerHTML = 'Please add description';

		halt = true;
	}

	if(!media) {
		var mediaContainer = document.getElementById('media-container');
		mediaContainer.lastChild.innerHTML = 'Please add movie or TV series name';

		halt = true;
	}

	if(!lat || !lon) {
		var latlonContainer = document.getElementById('latlon-container');
		latlonContainer.lastChild.innerHTML = 'Please add lat/lon';

		halt = true;
	}

	if(openstreetmap && (!openstreetmap.includes('://') || !openstreetmap.includes('.'))) {
		var openstreetmapContainer = document.getElementById('openstreetmap-container');
		openstreetmapContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(flickrImage && (!flickrImage.includes('://') || !flickrImage.includes('.'))) {
		var flickrImageContainer = document.getElementById('flickr-image-container');
		flickrImageContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(commonsImage && (!commonsImage.includes('://') || !commonsImage.includes('.'))) {
		var commonsImageContainer = document.getElementById('commons-image-container');
		commonsImageContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(wikipedia && (!wikipedia.includes('://') || !wikipedia.includes('.'))) {
		var wikipediaContainer = document.getElementById('wikipedia-container');
		wikipediaContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(website && (!website.includes('://') || !website.includes('.'))) {
		var websiteContainer = document.getElementById('website-container');
		websiteContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(!source) {
		var sourceContainer = document.getElementById('source-container');
		sourceContainer.lastChild.innerHTML = 'Please add source';

		halt = true;
	}

	if(email && (!email.includes('@') || !email.includes('.'))) {
		var emailContainer = document.getElementById('email-container');
		emailContainer.lastChild.innerHTML = 'Please provide a valid e-mail address';

		halt = true;
	}

	if(honeypotField) {
		var websiteContainer = document.getElementById('website-container');
		websiteContainer.lastChild.innerHTML = honeypotMessage;

		halt = true;
	}

	// Remove messages if there is an error in the form validation
	if(!locationName || !description || !media || !lat || !lon || (openstreetmap && (!openstreetmap.includes('://') || !openstreetmap.includes('.'))) || (flickrImage && (!flickrImage.includes('://') || !flickrImage.includes('.'))) || (commonsImage && (!commonsImage.includes('://') || !commonsImage.includes('.'))) || (wikipedia && (!wikipedia.includes('://') || !wikipedia.includes('.'))) || (website && (!website.includes('://') || !website.includes('.'))) || !source || (email && (!email.includes('@') || !email.includes('.')))) {
		var messagesContainer = document.getElementsByClassName('messages')[0];
		var messagesContainerClasses = messagesContainer.classList;

		messagesContainer.innerHTML = '';
		messagesContainerClasses.forEach(cl => {
			if(cl != 'messages' && cl != 'empty') {
				messagesContainerClasses.remove(cl);
			}
		});

		messagesContainerClasses.add('empty');
	}

	if(halt == false) {
		sendMail(file, options, successMessage, formID);
	}
}

export { submitLocation };
