import { config } from '../core/config.js';

function showLoader(id, options = {
	coverAll: false
}) {
	var el = document.getElementById(id);
	var coverAll = options.coverAll;

	if(el == null) {
		console.log('Couldn\'t find an element with the id of ' + id);
	} else if(typeof(coverAll) !== 'boolean'){
		console.log(id + ': coverAll option must be boolean');
	} else if(options.coverAll == false) {
		el.innerHTML = '<div class="loader"><img src="' + config.loader + '" /></div>';
	} else {
		el.innerHTML = '<div class="loader cover"><img src="' + config.loader + '" /></div>';
	}
}

export { showLoader };
