import * as C from '../core/constants.js';
import { isMobile, headerOffset } from '../core/global.js';

function initMap() {
	if(isMobile()) {
		var zoom = 2;
		var position = 'bottomright';
	} else {
		var zoom = 3;
		var position = 'topleft';
	}

	// Add tile layer to map
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright" title="Map copyright and License" target="_blank">OpenStreetMap contributors</a>'
	}).addTo(C.MAP);

	// Set control position and minimal zoom
	C.MAP.zoomControl.setPosition(position);
	C.MAP.options.minZoom = zoom;

	// Make the Leaflet link open in a new window
	C.MAP.attributionControl.setPrefix('<a href="https://leafletjs.com" title="A JS library for interactive maps" target="_blank">Leaflet</a>');

	// Add offset to control
	var leafletZoom = document.getElementsByClassName('leaflet-control leaflet-control-zoom');
	if(!isMobile()) {
		leafletZoom[0].style.top = headerOffset() + 8 + 'px';
	}
	// If it's on mobile, we load the offset after the menu is populated directy
	// In the ./navigation.js file itself
}

export { initMap };
