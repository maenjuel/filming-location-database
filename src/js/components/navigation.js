import * as C from '../core/constants.js';
import { isMobile, toggleOverlay } from '../core/global.js';
import { aboutBoxOffset } from '../content/content.js';

var headerMenu = document.getElementById('header-menu');
var footerMenu = document.getElementById('footer-menu');

function addHamburgerEventListener() {
	var menuButton = document.getElementById('hamburger');
	menuButton.addEventListener('click', showSiteInfo);
}

function buildMenus() {
	// Populate menus with the links
	fetch(C.CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;
			var footerMenuItems = [];

			json.forEach(page => {
				var path = page.path;
				var name = page.menu.name;
				var position = page.menu.position;
				var rel = page.menu.rel;
				var linkClass;
				var icon = page.menu.icon;

				if(path ==  C.PATH) {
					linkClass = 'class="active"';
				} else {
					linkClass = '';
				}

				if(icon) {
					icon = '<i class="icon material-icons md-18 ' + icon + '"></i>';
				} else {
					icon = '';
				}

				if(position == 'header') {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					headerMenu.innerHTML += '<li><a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + icon + name + '</a></li>';
				} else {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					// Because the footer menu already contains items from index.php we save them
					// In an array, and insert them after the forEach loop to have better control
					// Over the menu order
					if(position != undefined) {
						var listItem = document.createElement('li');
						listItem.innerHTML = '<a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + icon + name + '</a></li>';
						footerMenuItems.push(listItem);
					}
				}
			});

			// And now we add footer menu items to menu
			footerMenuItems.reverse().forEach(item => {
				footerMenu.insertBefore(item, footerMenu.childNodes[0]);
			});

			// Add hamburger icon at the end of the header menu
			headerMenu.innerHTML += '<li id="hamburger"><i class="icon material-icons md-36 md-menu"></i></li>';

			// Add event listener for hamburger icon
			addHamburgerEventListener();
		})
		.catch(err => {
			console.log('Error loading menus: ' + err);
		});
}

function showSiteInfo() {
	// Fetch and display content
	var titleElement = document.getElementById('about-title');
	var contentElement = document.getElementById('about-content');

	fetch(C.CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;

			json.forEach(page => {
				// Add the content depending on the path
				if(page.path == '/about') {
					titleElement.innerHTML = typogr.typogrify(page.title);
					contentElement.innerHTML = typogr.typogrify(page.content);
				}
			});
		})
		.catch(() => displayError());

	C.ELEMENT.ASIDE.classList.toggle('show'); // Toggle show class
	aboutBoxOffset(); // Set about box offset
	toggleHamburgerIcon(); // Toggle hamburger icon
	toggleOverlay();
}

function toggleHamburgerIcon() {
	// Toggle menu icon
	var menuButton = document.getElementById('hamburger');

	if(C.ELEMENT.ASIDE.classList.contains('show')) {
		menuButton.innerHTML = '<i class="icon material-icons md-36 md-close"></i>';
	} else {
		menuButton.innerHTML = '<i class="icon material-icons md-36 md-menu"></i>';
	}
}

export { buildMenus };
