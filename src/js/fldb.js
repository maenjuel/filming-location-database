import * as C from './core/constants.js';
import { initMap } from './components/map.js';
import { initSearch, toggleSearchSuggestionsBoxByClick } from './components/search.js';
import { buildMenus } from './components/navigation.js';
import { getContent } from './content/content.js';
import { OverlayScrollbars } from '../../dist/vendor/KingSora/OverlayScrollbars/overlayscrollbars.esm.js';

// init app
(function init() {
	initMap();
	initSearch();
	buildMenus();
	toggleSearchSuggestionsBoxByClick();
	getContent();
	OverlayScrollbars(document.querySelector('#' + C.ELEMENT.CONTENT.id), {
		overflow: {
			x: 'hidden'
		}
	});
})();
