import * as C from '../core/constants.js';
import { aboutBoxOffset } from '../content/content.js';

// Define breakpoints
var windowWidth = window.innerWidth;

function isMobile() {
	if(windowWidth <= 768) {
		return true;
	} else {
		return false;
	}
}

function isWide() {
	if(windowWidth >= 1440) {
		return true;
	} else {
		return false;
	}
}

// Display page title in <title> tag
function displayTitle(pageTitle) {
	if(pageTitle == undefined) {
		document.title = 'filming location database';
	} else {
		document.title = pageTitle + ' [filming location database]';
	}
}

function toggleOverlay() {
	if(C.ELEMENT.OVERLAY.classList.contains('show')) {
		C.ELEMENT.OVERLAY.classList.remove('show');
	} else {
		C.ELEMENT.OVERLAY.classList.add('show');
	}
}

function close(el) {
	el.target.parentNode.classList.remove('show');
}

// Display (error) messages
function displayMessage(options) {
	var type = options.type;
	var message = options.message;
	var el = C.ELEMENT.CONTENT.getElementsByClassName('messages')[0];
	var icon;

	if (options.type == 'error') {
		icon = '<i class="icon left material-icons-outline md-18 md-error"></i>';
	} else if (options.type == 'success') {
		icon = '<i class="icon left material-icons-outline md-18 md-thumb_up"></i>';
	} else {
		icon = '<i class="icon left material-icons-outline md-18 md-info"></i>'; // Fallback icon
		type = 'info';
	}

	if(options.message != undefined) {
		message = options.message;
	} else {
		message = 'We encountered an error, please try again.';
	}

	el.classList.add(type); // Add class with the name of the message type
	el.classList.remove('empty'); // remove 'empty' class
	el.innerHTML = icon + '<span>' + typogr.typogrify(message) + '</span>';

	// Sometimes there is a loader active. It needs to be removed as well
	var loader = document.getElementsByClassName('loader')[0];
	if(loader != undefined) {
		loader.remove();
	}
}

// Get header offset
function headerOffset() {
	var headerElement = document.getElementById('page-header');
	return headerElement.offsetHeight;
}

export { isMobile, isWide, displayTitle, toggleOverlay, close, displayMessage, headerOffset };
