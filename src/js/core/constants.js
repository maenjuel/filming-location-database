import { config } from './config.js';

// Some elements are only available on specific content types
// We need to check whether these elements appear on the DOM
// Before we add them as a constant.
var loc = document.getElementById('location');
var media = document.getElementById('media');
var page =document.getElementById('page');
var elementLoc = {};
var elementMedia = {};
var elementPage = {};

if(loc) {
	elementLoc = {
		'ADDRESS': document.getElementById('address'),
		'CONTENT': loc.getElementsByClassName('content')[0],
		'IMAGE': document.getElementById('image'),
		'LINKS': document.getElementById('links'),
		'MEDIA': document.getElementById('media-title'),
		'SEASONEPISODE': document.getElementById('season-episode'),
		'NATIVE': loc.getElementsByClassName('native')[0],
		'SOURCE': document.getElementById('source'),
		'TITLE': loc.getElementsByClassName('title')[0]
	};
}

if(media) {
	elementMedia = {
		'CONTENT': media.getElementsByClassName('content')[0],
		'NATIVE': media.getElementsByClassName('native')[0],
		'TITLE': media.getElementsByClassName('title')[0]
	};
}

if(page) {
	elementPage = {
		'CONTENT': page.getElementsByClassName('content')[0],
		'TITLE': page.getElementsByClassName('title')[0]
	};
}

// Define constants for global use
export const CONTENT = '/assets/content.json';
export const ELEMENT = {
	'ASIDE': document.getElementById('about'),
	'CONTENT': document.getElementById('content'),
	'LOCATION': elementLoc,
	'MEDIA': elementMedia,
	'OVERLAY': document.getElementById('overlay'),
	'PAGE': elementPage
};
export const MAP = L.map('map').setView([0, 0], 3);
export const MARKER = {
	DEFAULT: L.icon({
		iconUrl: config.marker.default.iconUrl,
		iconSize: config.marker.default.iconSize,
		iconAnchor: config.marker.default.iconAnchor
	}),
	ACTIVE: L.icon({
		iconUrl: config.marker.active.iconUrl,
		iconSize: config.marker.active.iconSize,
		iconAnchor: config.marker.active.iconAnchor
	})
};
export const PATH = window.location.pathname;
