export const config = {
	'api': {
		'fldbApiUrl': 'https://admin.fldb.cc/api',
		'fldbRouterUrl': 'https://admin.fldb.cc/router/translate-path',
		'ofdbApiUrl': 'https://ofdb.cc/api',
		'wikimediaApiUrl': 'https://commons.wikimedia.org/w/api.php'
	},
	'contentMargin': 20,
	'entriesPerPage': 25,
	'loader': '/assets/images/loading.gif',
	'map': 'L.map(\'map\', { minZoom: zoom, icon: config.marker }).setView([0, 0], 3);',
	'marker': {
		'default': {
			iconUrl: '/assets/images/marker_default.svg',
			iconSize: [34, 45],
			iconAnchor: [17, 45]
		},
		'active': {
			iconUrl: '/assets/images/marker_active.svg',
			iconSize: [34, 45],
			iconAnchor: [17, 45]
		}
	},
	'maxSearchResults': 10
}
// Matomo is configured in the index.php file directly
