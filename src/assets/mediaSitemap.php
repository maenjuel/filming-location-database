<?php
// Define an empty array
$films = array();

// Function to write to sitemap (will be called below)
function writeSitemap($films) {
	$sitemapFile = '/path/to/sitemap-media.xml'; // Our media XML sitemap
	$baseUrl = 'https://fldb.cc'; // The URL we want to append the paths to in the sitemap
	$xml = new DOMDocument();
	$xml->formatOutput = true;
	$xml->preserveWhiteSpace = false;
	$xml->load($sitemapFile);
	$urlset = $xml->documentElement;

	// First we make sure the sitemap is empty
	while ($urlset->hasChildNodes()) {
		$urlset->removeChild($urlset->firstChild);
	}

	// Now we add the new content
	foreach ($films as $film) {
		// Define data points
		$path = $film->path;
		$created = $film->created;

		// Create URL wrapper element
		$url = $xml->createElement('url');
		$urlset->appendChild($url);

		// Populate the URL wrapper with the child elements and the appropriate content
  	$loc = $xml->createElement('loc');
		$url->appendChild($loc);
		$path = $xml->createTextNode($baseUrl . $path);
		$loc->appendChild($path);

		$lastmod = $xml->createElement('lastmod');
		$url->appendChild($lastmod);
		$created = $xml->createTextNode($created);
		$lastmod->appendChild($created);

		$changefreq = $xml->createElement('changefreq');
		$url->appendChild($changefreq);
		$changeFrequency = $xml->createTextNode('monthly');
		$changefreq->appendChild($changeFrequency);

		$priority = $xml->createElement('priority');
		$url->appendChild($priority);
		$priorityValue = $xml->createTextNode('0.5');
		$priority->appendChild($priorityValue);
	}

	$xml->save($sitemapFile);
}

// Now we fetch all media entries from the backend and populate the array with them
for ($i = 0; ; $i+=50) {
	// fetch data with cURL
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://admin.fldb.cc/api/taxonomy_term/media/?filter[status][value]=1&sort=revision_created&page[offset]='.$i.'');
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$res = curl_exec($ch);
	curl_close($ch);

	$json = json_decode($res);
	$data = $json->data;

	foreach ($data as $film) {
		$alias = $film->attributes->path->alias;
		$created = strtotime($film->attributes->revision_created);

		array_push($films, (object)[
			'path' => $alias,
			'created' => date('Y-m-d', $created),
		]);
	}

	if ($i > count($json->data)) {
		writeSitemap($films);
		break;
	}
}
?>
