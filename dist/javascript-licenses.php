<!doctype html>
<html class="no-js" lang="en-GB">

<head>
	<meta charset="utf-8">
	<title>JavaScript licenses [filming location database]</title>
	<meta name="description" content="The filming location database is an open database for movie and TV series filming locations. It is a project maintained by its community.">
	<meta name="keywords" content="fldb, filming location database, location, filming location, movie location, TV series location, TV show location">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#fff">

	<link rel="shortcut icon" type='image/x-icon' href="/favicon.ico" />

	<link rel="stylesheet" href="/vendor/google/@material-icons/css/all.css" />
	<link rel="stylesheet" href="/vendor/bedlaj/@openfonts/lato_latin-ext/index.css" />
	<link rel="stylesheet" href="/css/fldb.min.css" />
</head>

<body class="javascript-licenses">
	<!--[if IE]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

	<main id="page-content">
		<h1 class="title">JavaScript licenses</h1>
		<div class="content">
			<p>This website only uses <a href="https://www.gnu.org/philosophy/free-sw.html" target="_blank" title="What is Free Software?">free/libre</a> JavaScript and is compatible with <a href="https://www.gnu.org/software/librejs/" target="_blank" title="GNU LibreJS">GNU LibreJS</a>.</p>

			<div class="responsible-table">
				<table id="jslicense-labels1">
					<thead>
						<tr>
							<td>Script</td>
							<td>License</td>
							<td>Source</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a href="/vendor/ekalinin/typogr.js/typogr.min.js" target="_blank">typogr.min.js</a></td>
							<td><a href="http://www.jclark.com/xml/copying.txt" target="_blank">Expat</a></td>
							<td><a href="https://github.com/ekalinin/typogr.js/blob/0.6.8/LICENSE" target="_blank">typogr.js 0.6.8</a></td>
						</tr>
						<tr>
							<td><a href="/vendor/hosuaby/leaflet.smooth_marker_bouncing/leaflet.smoothmarkerbouncing.js" target="_blank">leaflet.smoothmarkerbouncing.js</a></td>
							<td><a href="https://spdx.org/licenses/BSD-2-Clause.html" target="_blank">FreeBSD</a></td>
							<td><a href="https://github.com/hosuaby/Leaflet.SmoothMarkerBouncing/blob/v1.3.0/LICENSE" target="_blank">Leaflet.SmoothMarkerBouncin 1.3.0</a></td>
						</tr>
						<tr>
							<td><a href="/vendor/KingSora/OverlayScrollbars/js/OverlayScrollbars.min.js" target="_blank">OverlayScrollbars.min.js</a></td>
							<td><a href="http://www.jclark.com/xml/copying.txt" target="_blank">Expat</a></td>
							<td><a href="https://github.com/KingSora/OverlayScrollbars/blob/v1.12.0/LICENSE" target="_blank">OverlayScrollbars 1.12.0</a></td>
						</tr>
						<tr>
							<td><a href="/vendor/Leaflet/Leaflet/leaflet.js" target="_blank">leaflet.js</a></td>
							<td><a href="https://spdx.org/licenses/BSD-2-Clause.html" target="_blank">FreeBSD</a></td>
							<td><a href="https://github.com/Leaflet/Leaflet/blob/v1.6.0/LICENSE" target="_blank">Leaflet 1.6.0</a></td>
						</tr>
						<tr>
							<td><a href="/vendor/Leaflet/Leaflet.markercluster/leaflet.markercluster.js" target="_blank">leaflet.markercluster.js</a></td>
							<td><a href="http://www.jclark.com/xml/copying.txt" target="_blank">Expat</a></td>
							<td><a href="https://github.com/Leaflet/Leaflet.markercluster/blob/v1.4.1/MIT-LICENCE.txt" target="_blank">Leaflet 1.4.1</a></td>
						</tr>
						<tr>
							<td>
								<a href="/js/fldb.js" target="_blank">fldb.js</a>
							</td>
							<td><a href="http://www.gnu.org/licenses/gpl-3.0.html" target="_blank">GPLv3 or later</a></td>
							<td><a href="https://gitlab.com/maenjuel/filming-location-database/-/blob/master/LICENSE" target="_blank">filming location database</a></td>
						</tr>
					</tbody>
				</table>
			</div> <!-- /.responsive-table -->

			<a href="/" id="back">
				<i class="icon left material-icons md-navigate_before"></i> <span>back</span>
			</a>
		</div> <!-- /.content -->
	</main> <!-- /#page-content -->

	<!-- Matomo -->
	<script type="text/javascript">
		// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
		var _paq = window._paq || [];
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
		  var u="//matomo.fldb.cc/";
		  _paq.push(['setTrackerUrl', u+'matomo.php']);
		  _paq.push(['setSiteId', '1']);
		  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		})();
		// @license-end
	</script>
	<noscript><p><img src="//matomo.fldb.cc/matomo.php?idsite=1&amp;rec=1" style="border:0;" alt="" /></p></noscript>
	<!-- End Matomo Code -->
</body>

</html>
