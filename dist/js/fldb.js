'use strict';

const config = {
	'api': {
		'fldbApiUrl': 'https://admin.fldb.cc/api',
		'fldbRouterUrl': 'https://admin.fldb.cc/router/translate-path',
		'ofdbApiUrl': 'https://ofdb.cc/api',
		'wikimediaApiUrl': 'https://commons.wikimedia.org/w/api.php'
	},
	'contentMargin': 20,
	'entriesPerPage': 25,
	'loader': '/assets/images/loading.gif',
	'map': 'L.map(\'map\', { minZoom: zoom, icon: config.marker }).setView([0, 0], 3);',
	'marker': {
		'default': {
			iconUrl: '/assets/images/marker_default.svg',
			iconSize: [34, 45],
			iconAnchor: [17, 45]
		},
		'active': {
			iconUrl: '/assets/images/marker_active.svg',
			iconSize: [34, 45],
			iconAnchor: [17, 45]
		}
	},
	'maxSearchResults': 10
};
// Matomo is configured in the index.php file directly

// Some elements are only available on specific content types
// We need to check whether these elements appear on the DOM
// Before we add them as a constant.
var loc = document.getElementById('location');
var media = document.getElementById('media');
var page =document.getElementById('page');
var elementLoc = {};
var elementMedia = {};
var elementPage = {};

if(loc) {
	elementLoc = {
		'ADDRESS': document.getElementById('address'),
		'CONTENT': loc.getElementsByClassName('content')[0],
		'IMAGE': document.getElementById('image'),
		'LINKS': document.getElementById('links'),
		'MEDIA': document.getElementById('media-title'),
		'SEASONEPISODE': document.getElementById('season-episode'),
		'NATIVE': loc.getElementsByClassName('native')[0],
		'SOURCE': document.getElementById('source'),
		'TITLE': loc.getElementsByClassName('title')[0]
	};
}

if(media) {
	elementMedia = {
		'CONTENT': media.getElementsByClassName('content')[0],
		'NATIVE': media.getElementsByClassName('native')[0],
		'TITLE': media.getElementsByClassName('title')[0]
	};
}

if(page) {
	elementPage = {
		'CONTENT': page.getElementsByClassName('content')[0],
		'TITLE': page.getElementsByClassName('title')[0]
	};
}

// Define constants for global use
const CONTENT = '/assets/content.json';
const ELEMENT = {
	'ASIDE': document.getElementById('about'),
	'CONTENT': document.getElementById('content'),
	'LOCATION': elementLoc,
	'MEDIA': elementMedia,
	'OVERLAY': document.getElementById('overlay'),
	'PAGE': elementPage
};
const MAP = L.map('map').setView([0, 0], 3);
const MARKER = {
	DEFAULT: L.icon({
		iconUrl: config.marker.default.iconUrl,
		iconSize: config.marker.default.iconSize,
		iconAnchor: config.marker.default.iconAnchor
	}),
	ACTIVE: L.icon({
		iconUrl: config.marker.active.iconUrl,
		iconSize: config.marker.active.iconSize,
		iconAnchor: config.marker.active.iconAnchor
	})
};
const PATH = window.location.pathname;

function showLoader(id, options = {
	coverAll: false
}) {
	var el = document.getElementById(id);
	var coverAll = options.coverAll;

	if(el == null) {
		console.log('Couldn\'t find an element with the id of ' + id);
	} else if(typeof(coverAll) !== 'boolean'){
		console.log(id + ': coverAll option must be boolean');
	} else if(options.coverAll == false) {
		el.innerHTML = '<div class="loader"><img src="' + config.loader + '" /></div>';
	} else {
		el.innerHTML = '<div class="loader cover"><img src="' + config.loader + '" /></div>';
	}
}

// Start with a set of empty arrays we populate later
var locations = [];

function buildMarkers() {
	// If there are already locations in the array, we remove the locations
	// From the map and empty the array to start anew
	if (locations.length > 0) {
		MAP.removeLayer(markers);
		locations = [];
	}

	filmingLocations.forEach(filmLoc => {
		// Define data points
		var marker = {};
		var geoLocId = filmLoc.geoLoc.id;
		var name = filmLoc.name;
		var nativeName;
		filmLoc.source;
		var description = filmLoc.description;
		var website;
		var wikimedia;
		var wikipedia;
		var address;
		var lat;
		var lon;
		var openstreetmap;
		var media;
		var seasonEpisode = filmLoc.seasonEpisode;
		var sourceArray = [];

		geographicLocations.forEach(geoLoc => {
			if(geoLocId == geoLoc.id) {
				nativeName = geoLoc.name.native;
				address = geoLoc.address;
				lat = geoLoc.location.lat;
				lon = geoLoc.location.lon;
				media = filmLoc.media;
				openstreetmap = geoLoc.openstreetmap;

				// Add description
				if(filmLoc.description != null) {
					description = filmLoc.description;
				} else if (filmLoc.description == null && geoLoc.description != null) {
					description = geoLoc.description;
				} else {
					description = null;
				}

				// Add website
				if(filmLoc.website != null) {
					website = filmLoc.website;
				} else if (filmLoc.website == null && geoLoc.website != null) {
					website = geoLoc.website;
				} else {
					website = null;
				}

				// Add Wikimedia Commons
				if(filmLoc.wikimedia != null) {
					wikimedia = filmLoc.wikimedia;
				} else if (filmLoc.wikimedia == null && geoLoc.wikimedia != null) {
					wikimedia = geoLoc.wikimedia;
				} else {
					wikimedia = null;
				}

				// Add Wikipedia article
				if(filmLoc.wikipedia != null) {
					wikipedia = filmLoc.wikipedia;
				} else if (filmLoc.wikipedia == null && geoLoc.wikipedia != null) {
					wikipedia = geoLoc.wikipedia;
				} else {
					wikipedia = null;
				}

				// Merge sources
				var geoLocSource = geoLoc.source;
				var filmLocSource = filmLoc.source;
				var mergedSources = [];

				if(geoLocSource.length > 0 && filmLocSource.length > 0) {
					filmLocSource.forEach(src => {
						mergedSources.push(src);
					});

					geoLocSource.forEach(src => {
						mergedSources.push(src);
					});

					sourceArray = [...new Set(mergedSources)]; // Reduce source duplicates
				} else {
					filmLocSource.forEach(src => {
						mergedSources.push(src);
						sourceArray = mergedSources;
					});
				}
			}
		});

		// Get season and episode
		var season;
		var episode;

		if(seasonEpisode) {
			var season = seasonEpisode.first;
			var episode = seasonEpisode.second;
		} else {
			var season = null;
			var episode = null;
		}

		// Build marker
		marker = {
			'type': 'Feature',
			'properties': {
				'name': {
					'en':	name,
					'native': nativeName
				},
				'description': description,
				'website': website,
				'wikimedia': wikimedia,
				'wikipedia': wikipedia,
				'media': media,
				'seasonEpisode': {
					'season': season,
					'episode': episode
				},
				'source': sourceArray.sort().join(', '),
				'address': address,
				'openstreetmap': openstreetmap
			},
			'geometry': {
				'type': 'Point',
				'coordinates': [lon, lat]
			}
		};

		locations.push(marker);
	});
}

function toggleMarkers(marker) {
	MAP.eachLayer(layer => {
		if(layer._childCount) {
			layer.getAllChildMarkers().forEach(child => {
				child.setIcon(MARKER.DEFAULT);
			});
		}

		if(!layer.hasOwnProperty('_group') && layer.hasOwnProperty('_latlng') && layer != marker) {
			layer.setIcon(MARKER.DEFAULT);
		}
	});

	if(marker != undefined) {
		marker.setIcon(MARKER.ACTIVE);
	}
}

function displayMarkers() {
	function onEachFeature(feature, layer) {
		layer.on('click', function (loc) {
			showLocationContent(loc);
			toggleMarkers(this);
			this.bounce(1);
		});
	}

	// Show all markers on the map
	var jsonLayer = L.geoJSON(null, {
		onEachFeature: onEachFeature,
		pointToLayer: function(feature, latlng) {
			return L.marker(latlng, {
				icon: MARKER.DEFAULT
			})
		}
	});

	jsonLayer.addData(locations);

	var markers = L.markerClusterGroup({
		showCoverageOnHover: false,
		maxClusterRadius: 10,
		removeOutsideVisibleBounds: false // Needed to toggle active markers
	});

	markers.addLayer(jsonLayer);
	MAP.addLayer(markers);
	MAP.fitBounds(markers.getBounds().pad(0.5));
}

function getMarkerByName(name) {
	var marker;

	MAP.eachLayer(layer => {
		if(layer._childCount) {
			layer.getAllChildMarkers().forEach(child => {
				if(child.feature.properties.name.en == name) {
					marker = child;
				}
			});
		} else if(!layer.hasOwnProperty('_group') && layer.hasOwnProperty('_latlng')) {
			if(layer.feature.properties.name.en == name) {
				marker = layer;
			}
		}
	});

	return marker;
}

function getMediaList(type, offset = 0, sort = 'name') {
	if(type == 'movie') {
		type = 'm';
	} else {
		type = 'tv';
	}

	var url = config.api.fldbApiUrl + '/taxonomy_term/media/?filter[field_type]=' + type + '&page[offset]=' + offset + '&page[limit]=' + config.entriesPerPage + '&filter[status][value]=1&sort=' + sort;

	return fetch(url)
	.then (res => res.json())
	.then (json => {
		return json;
	})
	.catch(() => {
		displayMessage({
			type: 'error',
			message: 'We had problems loading the media list. Please try again later.'
		});
	});
}

function searchMediaEntries(key, sort = 'name') {
	var url = config.api.fldbApiUrl + '/taxonomy_term/media/?filter[name][operator]=CONTAINS&filter[name][value]=' + key + '&page[limit]=' + config.maxSearchResults + '&filter[status][value]=1&sort=' + sort + '&filter[status][value]=1';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

function getMediaUuid(type) {
	var param = '/' + type + '/';
	param = PATH.split(param).pop();
	var url = config.api.fldbRouterUrl + '?path=/' + type + '/' + param + '?filter[status][value]=1';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json.entity.uuid;
		})
		.catch(() => {
		displayMessage({
			type: 'error',
			message: 'We had problems loading the media list. Please try again later.'
		});
	});
}

function getNewestEntries(n=1) {
	var url = config.api.fldbApiUrl + '/taxonomy_term/media/?page[limit]=' + n + '&filter[status][value]=1&sort=-revision_created';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

function getRandomMedia() {
	// First we need to get the number of media entries available. We do that by fetching
	// The ID of the newest media taxonomy tag, and then subtract the number of source
	// taxonomy tags (as the ID increases not per vocabulary, but for taxonomies globally)
	function getMediaEntriesNumber() {
		var mediaUrl = config.api.fldbApiUrl + '/taxonomy_term/media/?page[limit]=1&filter[status][value]=1&sort=-revision_created';
		var sourceUrl = config.api.fldbApiUrl + '/taxonomy_term/source/?filter[status][value]=1&sort=-revision_created';
		var urlArray = [mediaUrl, sourceUrl];
		var urlArray = urlArray.map(url => fetch(url).then(res => res.json()));

		return Promise.all(urlArray)
			.then(res => {
				var id = res[0].data[0].attributes.drupal_internal__revision_id;
				var sourceLength = res[1].data.length;

				var mediaEntries = id - sourceLength;
				return mediaEntries;
			})
			.catch(err => console.log(err));
		}

	// Now that we have those numbers, we first get a random number between 1 and the returned
	// value, and simply return a JSON request with that randomly generated offset
	return getMediaEntriesNumber()
		.then(n => {
			var offset = Math.floor((Math.random() * n) + 1);
			var url = config.api.fldbApiUrl + '/taxonomy_term/media/?page[limit]=1&page[offset]=' + (offset - 1) + '&filter[status][value]=1&sort=-revision_created'; // -1 because JSON starts from 0 and not 1

			return fetch(url)
				.then(res => res.json())
				.then(json => {
					return json;
				})
				.catch(() => {
					displayMessage({
						type: 'error',
						message: 'We had problems loading the random entry. Please try again later.'
					});
				});
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the random entry. Please try again later.'
			});
		});
}

function getOfdbUuid(uuid) {
	var mediaUrl = config.api.fldbApiUrl + '/taxonomy_term/media/' + uuid + '?filter[status][value]=1';
	return fetch(mediaUrl)
		.then(res => res.json())
		.then(res => {
			return [uuid, res.data.attributes.field_ofdb];
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

function getMedia(type) {
	return getMediaUuid(type)
		.then(res => getOfdbUuid(res))
		.then(res => {
			return res;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media list. Please try again later.'
			});
		});
}

function getFilmInfo(type, uuid) {
	var param;

	if (type == 'm') {
		param = 'movie';
	} else {
		param = 'tv_show';
	}

	var url = config.api.ofdbApiUrl + '/node/' + param + '/' + uuid + '?filter[status][value]=1';

	return fetch(url)
		.then(res => res.json())
		.then(res => {
			return res;
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media information. Please try again later.'
			});
		});
}

// Start with a set of empty arrays we populate later
var filmingLocations = [];
var geographicLocations = [];

async function getFilmingLocationsData(url) {
	let allData = [];
	let allIncluded = [];
	let nextPageUrl = url;
	let finalResponse = [];

	try {
		while (nextPageUrl) {
			const response = await fetch(nextPageUrl);
			const data = await response.json();

			// Add the current page's data to allData
			allData = allData.concat(data.data);

			// Add current page's included data, if present
			if (data.included) {
				allIncluded = allIncluded.concat(data.included);
			}

			// Save final response structure; only once needed
			if (!finalResponse.jsonapi) {
				finalResponse = {
					jsonapi: data.jsonapi || {},
					links: data.links || {},
				};
			}

			// Check if there is a next page
			const links = data.links;
				if (links && links.next) {
					nextPageUrl = links.next.href;
				} else {
					nextPageUrl = null;
				}
			}

		// Combine all data and included data
		finalResponse.data = allData;
		if (allIncluded.length > 0) {
			finalResponse.included = allIncluded;
		}

		// Return all data for further use
		return finalResponse;
	} catch (err) {
		console.error(err);
  }
}

function buildFilmingLocations(res) {
	var data = res.data;
	var included = res.included;

	// build location object and add it to an object, and then to an array
	data.forEach(loc => {
		var id = loc.id;
		var name = loc.attributes.title;
		var description = loc.attributes.body;
		var website = loc.attributes.field_website;
		var wikimedia = loc.attributes.field_wikimedia_commons;
		var wikipedia = loc.attributes.field_wikipedia;
		var geoLoc = loc.relationships.field_geographic_location.data.id;
		var media = included[0].attributes.name;
		var seasonEpisode = loc.attributes.field_season_episode;
		var source = loc.relationships.field_source.data;

		if(description != null) {
			description = loc.attributes.body.processed;
		}

		if(website != null) {
			website = loc.attributes.field_website.uri;
		}

		if(wikimedia != null) {
			wikimedia = wikimedia.uri;
		}

		if(wikipedia != null) {
			wikipedia = wikipedia.uri;
		}

		// Add sources to an array
		var sourceArray = [];

		source.forEach(src => {
			included.forEach(inc => {
				if((src.id == inc.id) && (!sourceArray.includes(inc.attributes.name))) {
					sourceArray.push(inc.attributes.name);
				}
			});
		});

		loc = {
			'id': id,
			'name': name,
			'description': description,
			'website': website,
			'wikimedia': wikimedia,
			'wikipedia': wikipedia,
			'geoLoc': {
				'id': geoLoc
			},
			'media': media,
			'seasonEpisode': seasonEpisode,
			'source': sourceArray
		};

		filmingLocations.push(loc);
	});
}

function getGeographicLocationsData() {
	var urlArray = []; // An empty array we'll populate with URLs

	filmingLocations.forEach(filmLoc => {
		// Get geoLocId and build the url with it
		var geoLocId = filmLoc.geoLoc.id;
		var geoLocUrl = config.api.fldbApiUrl + '/node/geographic_location/' + geoLocId + '?include=field_source' + '&filter[status][value]=1';
		urlArray.push(geoLocUrl);
	});

	var urlArray = urlArray.map(url => fetch(url).then(res => res.json()));

	return Promise.all(urlArray)
		.then(res => {
			return res;
		})
		.catch(err => console.log(err));
}

function buildGeographicLocationsData(res) {

	res.forEach(loc => {
		var data = loc.data;
		var included = loc.included;

		var id = data.id;
		var name = data.attributes.title;
		var nativeName = data.attributes.field_native_name;
		var description = data.attributes.body;
		var website = data.attributes.field_website;
		var wikimedia = data.attributes.field_wikimedia_commons;
		var wikipedia = data.attributes.field_wikipedia;
		var address = data.attributes.field_address;
		var lat = data.attributes.field_location.lat;
		var lon = data.attributes.field_location.lon;
		var openstreetmap = data.attributes.field_openstreetmap;
		var source = data.relationships.field_source.data;

		if(description != null) {
			description = description.processed;
		}

		if(website != null) {
			website = website.uri;
		}

		if(wikimedia != null) {
			wikimedia = wikimedia.uri;
		}

		if(wikipedia != null) {
			wikipedia = wikipedia.uri;
		}

		if(openstreetmap != null) {
			openstreetmap = openstreetmap.uri;
		}

		// Build the address
		var combinedAddress;

		if(address != null) {
			var combinedAddress = ''; // If we don't set the string like this and we don't have a address_line1, we'll get "undefined" rendered in the string

			if(address.address_line1 != '') {
				combinedAddress += address.address_line1 + '\n';
			}

			if(address.address_line2 != '') {
				combinedAddress += address.address_line2 + '\n';
			}

			if(address.postal_code != null) {
				combinedAddress += address.postal_code + ' ';
			}

			if(address.locality != null) {
				combinedAddress += address.locality + ' ';
			}

			if(address.administrative_area != null && address.administrative_area != '') {
				combinedAddress += '\n' + address.administrative_area;
			}

			if(address.country_code != null) {
				if(combinedAddress == ' ') {
					combinedAddress += address.country_code;
				} else {
					combinedAddress += ' (' + address.country_code + ')';
				}
			}
		}

		// Add sources to an array
		var sourceArray = [];

		source.forEach(src => {
			included.forEach(inc => {
				if(src.id == inc.id) {
					sourceArray.push(inc.attributes.name);
				}
			});
		});

		loc = {
			'id': id,
			'name': {
				'en': name,
				'native': nativeName
			},
			'description': description,
			'website': website,
			'wikimedia': wikimedia,
			'wikipedia': wikipedia,
			'address': combinedAddress,
			'location': {
				'lat': lat,
				'lon': lon
			},
			'openstreetmap': openstreetmap,
			'source': sourceArray
		};

		geographicLocations.push(loc);
	});
}

function getLocationInfo(uuid) {
	var url = config.api.fldbApiUrl + '/node/filming_location/?filter[field_media.id]=' + uuid + '&filter[status][value]=1&include=field_media,field_source';

	getFilmingLocationsData(url)
		.then(res => buildFilmingLocations(res))
		.then(() => getGeographicLocationsData())
		.then(res => buildGeographicLocationsData(res))
		.then(() => {
			buildMarkers();
			displayLocationList();
		})
		.then(() => {
			if(locations.length > 0) {
				displayMarkers();
			} else {
				displayMessage({
					type: 'error',
					message: 'No locations found for this entry. Sorry for the inconvenience.'
				});
			}
		});
}

function displayLocations(type) {
	// Show loader
	showLoader('location-list', {
		coverAll: true
	});

	// Get all locations on the map
	getMedia(type)
	.then(res => {
		// Get the TV show/movie information
		getFilmInfo(type, res[1])
		.then(res => displayFilmInfo(res))
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems loading the media information. Please try again later.'
			});
		});

		// Get the content for the markers
		getLocationInfo(res[0]);
	});
}

function clearFields$1(id, msg) {
	var form = document.getElementById(id);
	var errorContainers = form.getElementsByClassName('error');
	// Remove error messages
	for (var item of errorContainers) {
		item.innerHTML = '';
	}

	// Display success message
	displayMessage({
		type: 'success',
		message: msg
	});
}

function sendMail(file, options, msg, id) {
	document.getElementById(id);

	fetch(file, options)
		.then((res) => {
			clearFields$1(id, msg);
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'Error sending e-mail. Please try again.'
			});
		});
}

function submitLocation(event) {
	event.preventDefault(); // Prevent from submit

	// Get form data
	var formID = 'submit-location';
	var form = document.getElementById(formID);
	var formData = new FormData(form);

	// Stuff needed to fetch()
	var file = '/assets/submitLocation.php';
	var options = {
		method: 'POST',
		body: formData
	};

	// Messages
	var honeypotMessage = 'Whoa, hold on, spammer!';
	var successMessage = 'Location submitted. Thank you for improving the filming location database!<br /><br /><em>Note: The location will only be online after being reviewed by a moderator.</em>';

	// We add a variable 'halt' set to false. If we encounter a validation error
	// We set the variable to true and won't send the e-mail with that value
	var halt = false;

	// form fields
	var locationName = document.getElementById('location-name').value;
	document.getElementById('native-name').value;
	var description = document.getElementById('description').value;
	var media = document.getElementById('media').value;
	var lat = document.getElementById('lat').value;
	var lon = document.getElementById('lon').value;
	var openstreetmap = document.getElementById('openstreetmap').value;
	var flickrImage = document.getElementById('flickr-image').value;
	var commonsImage = document.getElementById('commons-image').value;
	var wikipedia = document.getElementById('wikipedia').value;
	var website = document.getElementById('website').value;
	var source = document.getElementById('source').value;
	var email = document.getElementById('email').value;

	// Honeypot field
	var honeypotField = document.getElementById('url').value;

	// First we remove old error messages
	var errorContainers = form.getElementsByClassName('error');
	for (var item of errorContainers) {
		item.innerHTML = '';
	}

	// Form validation
	if(!locationName) {
		var locationNameContainer = document.getElementById('location-name-container');
		locationNameContainer.lastChild.innerHTML = 'Please add location name';

		halt = true;
	}

	if(!description) {
		var descriptionContainer = document.getElementById('description-container');
		descriptionContainer.lastChild.innerHTML = 'Please add description';

		halt = true;
	}

	if(!media) {
		var mediaContainer = document.getElementById('media-container');
		mediaContainer.lastChild.innerHTML = 'Please add movie or TV series name';

		halt = true;
	}

	if(!lat || !lon) {
		var latlonContainer = document.getElementById('latlon-container');
		latlonContainer.lastChild.innerHTML = 'Please add lat/lon';

		halt = true;
	}

	if(openstreetmap && (!openstreetmap.includes('://') || !openstreetmap.includes('.'))) {
		var openstreetmapContainer = document.getElementById('openstreetmap-container');
		openstreetmapContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(flickrImage && (!flickrImage.includes('://') || !flickrImage.includes('.'))) {
		var flickrImageContainer = document.getElementById('flickr-image-container');
		flickrImageContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(commonsImage && (!commonsImage.includes('://') || !commonsImage.includes('.'))) {
		var commonsImageContainer = document.getElementById('commons-image-container');
		commonsImageContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(wikipedia && (!wikipedia.includes('://') || !wikipedia.includes('.'))) {
		var wikipediaContainer = document.getElementById('wikipedia-container');
		wikipediaContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(website && (!website.includes('://') || !website.includes('.'))) {
		var websiteContainer = document.getElementById('website-container');
		websiteContainer.lastChild.innerHTML = 'Please provide a valid URL';

		halt = true;
	}

	if(!source) {
		var sourceContainer = document.getElementById('source-container');
		sourceContainer.lastChild.innerHTML = 'Please add source';

		halt = true;
	}

	if(email && (!email.includes('@') || !email.includes('.'))) {
		var emailContainer = document.getElementById('email-container');
		emailContainer.lastChild.innerHTML = 'Please provide a valid e-mail address';

		halt = true;
	}

	if(honeypotField) {
		var websiteContainer = document.getElementById('website-container');
		websiteContainer.lastChild.innerHTML = honeypotMessage;

		halt = true;
	}

	// Remove messages if there is an error in the form validation
	if(!locationName || !description || !media || !lat || !lon || (openstreetmap && (!openstreetmap.includes('://') || !openstreetmap.includes('.'))) || (flickrImage && (!flickrImage.includes('://') || !flickrImage.includes('.'))) || (commonsImage && (!commonsImage.includes('://') || !commonsImage.includes('.'))) || (wikipedia && (!wikipedia.includes('://') || !wikipedia.includes('.'))) || (website && (!website.includes('://') || !website.includes('.'))) || !source || (email && (!email.includes('@') || !email.includes('.')))) {
		var messagesContainer = document.getElementsByClassName('messages')[0];
		var messagesContainerClasses = messagesContainer.classList;

		messagesContainer.innerHTML = '';
		messagesContainerClasses.forEach(cl => {
			if(cl != 'messages' && cl != 'empty') {
				messagesContainerClasses.remove(cl);
			}
		});

		messagesContainerClasses.add('empty');
	}

	if(halt == false) {
		sendMail(file, options, successMessage, formID);
	}
}

function displayList(type, res, offset = 0) {
	var data = res.data;
	var links = res.links;

	var listContainer = document.getElementById('list');
	var list = listContainer.getElementsByTagName('ul')[0];

	list.innerHTML = ''; // Clear list (necessary for pagination)

	data.forEach(media => {
		media.id;
		var path = media.attributes.path.alias;
		var title = media.attributes.name;

		list.innerHTML += '<li><a href="' + path + '" title="' + title + '">' + typogr.typogrify(title) + '</a></li>';
	});

	// Add pagination if there is more than one page
	if(links.prev != undefined || links.next != undefined) {
		var buttons;

		if(links.prev == undefined && links.next != undefined) {
			buttons = 'next';
		} else if (links.prev != undefined && links.next == undefined) {
			buttons = 'prev';
		} else {
			buttons = 'both';
		}

		getPagination(type, buttons, offset);
	}
}

function getPagination(type, buttons = 'next', offset = 0) {
	var paginationContainer = document.getElementById('pagination');

	if(buttons == 'next') {
		paginationContainer.innerHTML = '<span>Pagination:</span><a class="button secondary" id="next" onclick="buildMediaList(\'' + type + '\', ' + (offset + config.entriesPerPage) + ')" href="#">&rsaquo;</a>';
	} else if(buttons == 'prev') {
		paginationContainer.innerHTML = '<span>Pagination:</span><a class="button secondary" id="prev" onclick="buildMediaList(\'' + type+ '\', ' + (offset - config.entriesPerPage) + ')" href="#">&lsaquo;</a>';
	} else {
		paginationContainer.innerHTML = '<span>Pagination:</span><a class="button secondary" id="prev" onclick="buildMediaList(\'' + type+ '\', ' + (offset - config.entriesPerPage) + ')" href="#">&lsaquo;</a> <a class="button secondary" id="next" onclick="buildMediaList(\'' + type+ '\', ' + (offset + config.entriesPerPage) + ')"  href="#">&rsaquo;</a>';
	}
}

function buildMediaList(type, offset) {
	// Display loader
	var listContainer = document.getElementById('list');
	var list = listContainer.getElementsByTagName('ul')[0].id;
	showLoader(list); // Display loader while getting entries

	// Build list
	getMediaList(type, offset)
	.then(res => displayList(type, res, offset))
	.catch(() => {
		var listContainer = document.getElementById('list');
		var list = listContainer.getElementsByTagName('ul')[0];

		list.innerHTML = ''; // Clear list (necessary for pagination)
		list.innerHTML += typogr.typogrify('We had problems getting the media entries. Please try again.');
	});
}

function displayPage() {
	if(PATH == '/') {
		// We only need the title for the home page
		displayTitle('Welcome, friend!');
		showRandomMedia();

		// Add event listener for "back" link
		var backLink = document.getElementById('back');
		backLink.addEventListener('click', switchToMedia);
	} else {
		// If we're not on the home page, get the content accordingly
		fetch(CONTENT)
			.then(res => res.json())
			.then(json => {
				json = json.page;

				json.forEach(page => {
					// Add the content depending on the path
					if(page.path == PATH) {
						ELEMENT.PAGE.TITLE.innerHTML = typogr.typogrify(page.title);
						ELEMENT.PAGE.CONTENT.innerHTML = typogr.typogrify(page.content);

						// Show page specific content
						 if(PATH == '/movies') {
							ELEMENT.PAGE.CONTENT.innerHTML += '<div id="list"><ul id="movies"></ul><div id="pagination"></div></div>';
							buildMediaList('movie');
						} else if(PATH == '/tv-shows') {
							ELEMENT.PAGE.CONTENT.innerHTML += '<div id="list"><ul id="tv-shows"></ul><div id="pagination"></div></div>';
							buildMediaList('tv-show');
						} else if(PATH == '/submit-location') {
							var submitButton = document.getElementById('submit-form');
							submitButton.addEventListener('click', function() {
								submitLocation(event);
							});
						}

						displayTitle(page.title); // Update page title
					}
				});

				// If there is no title or content, we display an error message
				if((ELEMENT.PAGE.TITLE.innerHTML == '') && (ELEMENT.PAGE.CONTENT.innerHTML == '')) {
					displayError$1();
				}
			})
			.catch(() => displayError$1());
		}
}

function getWikimediaImage(url) {
	var fileName = url.split('wiki/').pop();
	var url = config.api.wikimediaApiUrl + '?action=query&titles=' + fileName + '&prop=imageinfo&iiprop=extmetadata|url&iiurlwidth=800&origin=*&format=json';

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return mapWikimediaImageData(json);
		});
}

function mapWikimediaImageData(image) {
	// First we need to get the pageID before we can get the data
	var id = Object.keys(image.query.pages);

	// Now we get the image data
	var imageinfo = image.query.pages[id].imageinfo[0];
	var title = imageinfo.extmetadata.ObjectName.value;
	var url = imageinfo.thumburl;
	var artist = imageinfo.extmetadata.Artist;
	var licenseName = imageinfo.extmetadata.LicenseShortName;
	var licenseUrl = imageinfo.extmetadata.LicenseUrl;

	if(artist != null) {
		artist = artist.value;
	}

	if(licenseName != null) {
		licenseName = licenseName.value;
	}

	if(licenseUrl != null) {
		licenseUrl = licenseUrl.value;
	}

	return {
		'title': title,
		'url': url,
		'artist': artist,
		'licenseName': licenseName,
		'licenseUrl': licenseUrl
	}
}

// Determine content type based on path and call appropriate function
function getContent() {
	if (PATH.includes('/m/')) {
		displayLocations('m');

		// Add event listener for "back" link
		var backLink = document.getElementById('back');
		backLink.addEventListener('click', switchToMedia);
	} else if (PATH.includes('/tv/')) {
		displayLocations('tv');

		// Add event listener for "back" link
		var backLink = document.getElementById('back');
		backLink.addEventListener('click', switchToMedia);
	} else {
		displayPage();
	}

	// Set content offset
	if(isMobile()) {
		ELEMENT.CONTENT.style.top = headerOffset() + config.contentMargin +'px';
	} else {
		ELEMENT.CONTENT.style.top = headerOffset() + 'px';
		ELEMENT.CONTENT.style.marginBottom = headerOffset() + 'px';
	}

	// Add event listener for the user to close the content box (on mobile)
	var closeContentButton = document.getElementById('close-content');
	closeContentButton.addEventListener('click', closeContent);
}

function showContentBox() {
	ELEMENT.CONTENT.classList.add('show');
}

function aboutBoxOffset() {
	ELEMENT.ASIDE.style.top = headerOffset() + 'px';
}

// Displaying the newest movie or TV show
function showRandomMedia() {
	getRandomMedia()
		.then(json => {
			json = json.data[0];
			var id = json.id;
			var type = json.attributes.field_type;
			var odfbId = json.attributes.field_ofdb;
			json.attributes.path.alias;

			getLocationInfo(id); // Display markers

			// Get film info
			getFilmInfo(type, odfbId)
			.then(res => {
				json = res.data;
				var title = json.attributes.title;
				var originalTitle = json.attributes.field_original_title;
				var plot = json.attributes.body;
				var poster = json.attributes.field_poster;
				json.attributes.field_release_date;

				// Display title
				ELEMENT.MEDIA.TITLE.innerHTML = typogr.typogrify(title);

				// Add notification tooltip
				document.getElementById('media');
				var tooltip = document.createElement('span');
				tooltip.classList.add('tooltip', 'right', 'show');
				tooltip.innerHTML = 'We found this random film for you. <i class="icon material-icons md-18 md-close"></i>';

				ELEMENT.MEDIA.TITLE.parentNode.insertBefore(tooltip, ELEMENT.MEDIA.TITLE.nextSibling);

				var closeButton = tooltip.lastChild;
				closeButton.addEventListener('click', close);

				// Display original title, if available
				if(originalTitle != null) {
					ELEMENT.MEDIA.NATIVE.innerHTML += '<i class="icon material-icons md-18 md-language"></i>' + typogr.typogrify(originalTitle);
				}

				// Get poster, if available
				if(poster != null) {
					poster = poster.uri.split('File:').pop();
					ELEMENT.MEDIA.CONTENT.innerHTML += '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="Poster" id="poster" />';

					poster = document.getElementById('poster');
				}

				// Get plot, if available
				if(plot == null) {
					if(type == 'm') {
						ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this movie.');
					} else if(type == 'tv') {
						ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this TV show.');
					}
				} else {
					ELEMENT.MEDIA.CONTENT.innerHTML += '<p>' + typogr.typogrify(plot.value) + '</p>';
				}
			})
			.catch(() => {
				displayMessage({
					type: 'error',
					message: 'We had problems loading the media information. Please try again later.'
				});
			});
		})
		.catch(err => console.log(err));
}

// Add event listener for spoilers
function initSpoilers() {
	var spoilers = document.getElementsByClassName('spoiler');

	for(let spoiler of spoilers) {
		var button = spoiler.getElementsByTagName('button')[0];
		button.addEventListener('click', function() {
			revealSpoiler(this);
		});
	}
}

// Show spoilers
function revealSpoiler(el) {
	var spoiler = el.parentElement;
	spoiler.classList.add('show-spoiler');
	el.remove();
}

// Hide content
function closeContent() {
	ELEMENT.CONTENT.classList.remove('show');
}

// Display error if there is no content to show
function displayError$1() {
	displayTitle('How the turntables...'); // Display page title
	ELEMENT.PAGE.TITLE.innerHTML = 'How the turntables...';
	ELEMENT.PAGE.CONTENT.innerHTML = typogr.typogrify('<p>This doesn\'t sound right. Something is wrong, please try again or go to the <a href="/" title="Home page">home page</a>.</p>');
}

function clearFields() {
	ELEMENT.LOCATION.ADDRESS.innerHTML = '';
	ELEMENT.LOCATION.CONTENT.innerHTML = '';
	ELEMENT.LOCATION.IMAGE.innerHTML = '';
	ELEMENT.LOCATION.LINKS.innerHTML = '';
	ELEMENT.LOCATION.MEDIA.innerHTML = '';
	ELEMENT.LOCATION.SEASONEPISODE.innerHTML = '';
	ELEMENT.LOCATION.NATIVE.innerHTML = '';
	ELEMENT.LOCATION.SOURCE.innerHTML = '';
	ELEMENT.LOCATION.TITLE.innerHTML = '';
}

function clearErrors() {
	var messages = document.getElementsByClassName('messages');
	for (let el of messages) {
		el.innerHTML = '';
		el.classList.add('empty');
	}
}

function switchToLocation() {
	var mediaContainer = document.getElementById('media');
	var locationContainer = document.getElementById('location');

	locationContainer.classList.add('show');
	mediaContainer.classList.remove('show');

	// To display the scrollbar correctly, we must show/hide the proper containers
	setTimeout(() => { mediaContainer.style.width = 0 + 'px'; }, 300); // value is equal to the CSS transition
	locationContainer.style.width = '100%';
}

function switchToMedia() {
	var mediaContainer = document.getElementById('media');
	var locationContainer = document.getElementById('location');

	mediaContainer.classList.add('show');
	locationContainer.classList.remove('show');

	setTimeout(() => { locationContainer.style.width = 0 + 'px'; }, 300); // value is equal to the CSS transition
	mediaContainer.style.width = '100%';

	toggleMarkers();
}

function displayFilmInfo(json) {
	var title = json.data.attributes.title;
	var originalTitle = json.data.attributes.field_original_title;
	var plot = json.data.attributes.body;
	var poster = json.data.attributes.field_poster;
	json.data.attributes.field_release_date;
	var type = json.data.type;

	clearFields(); // Clear fields

	// Remove error messages if it's not no locations found
	ELEMENT.MEDIA.CONTENT.getElementsByClassName('messages')[0];
	ELEMENT.MEDIA.TITLE.innerHTML = typogr.typogrify(title); // Display title

	// Display original title, if available
	if(originalTitle != null) {
		ELEMENT.MEDIA.NATIVE.classList.remove('empty');
		ELEMENT.MEDIA.NATIVE.innerHTML = '<i class="icon left material-icons md-18 md-language"></i>' + typogr.typogrify(originalTitle);
	} else {
		ELEMENT.MEDIA.NATIVE.classList.add('empty');
	}

	// Get poster, if available
	if(poster != null) {
		poster = poster.uri.split('File:').pop();
		ELEMENT.MEDIA.CONTENT.innerHTML = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="Poster" id="poster" />';
	}

	// Get plot, if available
	if(plot == null) {
		if(type == 'node--movie') {
			ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this movie.');
		} else if(type == 'node--tv_show') {
			ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify('We don\'t have a plot for this TV show.');
		}
	} else {
		ELEMENT.MEDIA.CONTENT.innerHTML += typogr.typogrify(plot.value);
	}

	displayTitle(title); // Display page title
}

function displayLocationList() {
	var locationContainer = document.getElementById('location-list');
	if(locations.length > 1) {
		locationContainer.innerHTML = '<p>' + locations.length + ' locations found:</p><ul></ul>';
	} else {
		locationContainer.innerHTML = '<p>' + locations.length + ' location found:</p><ul></ul>';
	}

	var listContainer = locationContainer.getElementsByTagName('ul')[0];
	var list = [];

	// First we add all locations to an array, that we, then, sort
	locations.forEach(loc => {
		list.push(loc.properties.name.en);
	});
	list.sort();

	// Now we create a list from those entries
	list.forEach(loc => {
		listContainer.innerHTML += '<li><i class="icon left material-icons-outline md-18 md-place"></i><a alt="' + loc.replace(/"/g, '\'\'') + '">' + typogr.typogrify(loc) + '</a></li>';
	});

	// Finally, we add an event listener to that element
	var listElements = listContainer.children;
	for (let li of listElements) {
		li.childNodes[1].addEventListener('click', function() {
			// First we move the map view to the marker
			var rawName = li.childNodes[1].attributes[0].value.replace(/''/g, '"'); // We need the raw string of the title to get the correct marker
			var marker = getMarkerByName(rawName);

			if(list.length > 1) {
				var markerLatLng = marker.getLatLng();
				MAP.setView(markerLatLng, 16);
			} else {
				marker.bounce(1);
			}

			// Then we display the content and show the necessary animations for the marker
			toggleMarkers(marker);
			showLocationContent(marker);

			if(marker._icon == undefined){
				setTimeout(() => {
					marker.__parent.__parent.__parent.spiderfy();
				}, 400);
			}
		});
	}
}

function showLocationContent(loc) {
	clearFields(); // Clear fields
	clearErrors(); // Clear error messages

	// Define data points
	var properties;
	var latlon;

	if(loc.sourceTarget != undefined) {
		properties = loc.sourceTarget.feature.properties;
		latlon = loc.sourceTarget.feature.geometry.coordinates;
	} else {
		properties = loc.feature.properties;
		latlon = loc.feature.geometry.coordinates;
	}

	var description = properties.description;
	if(description == null) {
		description = 'We don\'t have a description for this location.';
	}

	var name = properties.name.en;
	var nativeName = properties.name.native;
	var website = properties.website;
	var wikimedia = properties.wikimedia;
	var wikipedia = properties.wikipedia;
	var media = properties.media;
	var seasonEpisode = properties.seasonEpisode;
	var source = properties.source;
	var address = properties.address;
	var openstreetmap = properties.openstreetmap;

	// Now we get the Wikimedia Picture, if available
	if(wikimedia != undefined) {
		getWikimediaImage(wikimedia)
			.then(img => {
				// Get image title and create alt tag, if available
				var imageAlt;
				var imageTitle;
				if(img.title) {
					imageAlt = 'alt="' + img.title.replace(/(<([^>]+)>)/ig,'') + '" ';
					imageTitle = '<a href="' + wikimedia + '" title="View picture on Wikimedia Commons" target="_blank">' + typogr.typogrify(img.title.replace(/(<([^>]+)>)/ig,'')) + '</a>. ';
				} else {
					imageAlt = '';
					imageTitle = '';
				}

				ELEMENT.LOCATION.IMAGE.innerHTML = '<img src="' + img.url + '" ' + imageAlt + '"/>'; // Add image

				// Build attribution text
				var attribution;
				if(img.licenseUrl == undefined && img.artist == undefined) {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span>' + imageTitle + img.licenseName + '</span></div>';
				} else if(img.licenseUrl == undefined && img.artist != undefined) {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span>' + imageTitle + img.licenseName + '/' + img.artist + '</span></div>';
				} else if(img.licenseUrl != undefined && img.artist == undefined) {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span><a href="' + img.licenseUrl + '" title="' + img.licenseName + '">' + imageTitle + img.licenseName + '</a></span></div>';
				} else {
					attribution = '<div id="image-credit"><i class="icon left material-icons-outline md-copyright"></i><span><a href="' + img.licenseUrl + '" title="' + img.licenseName + '">' + imageTitle + img.licenseName + '</a>/' + img.artist + '</span></div>';
				}

				// Finally we modify links that don't open in a new window so they do
				var dummyDOM = document.createElement( 'html' );
				dummyDOM.innerHTML = attribution;
				var links = dummyDOM.getElementsByTagName('a');

				for(let link of links) {
					link.target = "_blank";
				}

				attribution = dummyDOM.getElementsByTagName('body')[0].innerHTML;
				ELEMENT.LOCATION.IMAGE.innerHTML += attribution; // Display attribution
			})
			.catch(() => {
				displayMessage({
					type: 'info',
					message: 'We had problems loading the image from Wikimedia Commons. Please try again later.'
				});
			});
		}

	// Then we add the spoiler button to description
	if(description.includes('<span class="spoiler">')) {
		var description = description.replace('</span>', '<button class="secondary">reveal spoiler</button></span>');
	}

	// Display the content
	ELEMENT.LOCATION.TITLE.innerHTML = typogr.typogrify(name);
	ELEMENT.LOCATION.CONTENT.innerHTML = typogr.typogrify(description);

	// Display OSM link
	if(openstreetmap != null) {
		ELEMENT.LOCATION.LINKS.innerHTML = '<a href="' + openstreetmap + '" alt="Open on OpenStreetMap" title="Open on OpenStreetMap" target="_blank"><i class="icon left material-icons-outline md-18 md-gps_fixed"></i></a>';
	} else {
		ELEMENT.LOCATION.LINKS.innerHTML = '<a href="https://www.openstreetmap.org/?mlat=' + latlon[1] + '&mlon=' + latlon[0] + '#map=17/' + latlon[1] + '/' + latlon[0] + '" alt="Open on OpenStreetMap" title="Open on OpenStreetMap" target="_blank"><i class="icon left material-icons-outline md-18 md-gps_fixed"></i></a>';
	}

	// Display optional fields
	if(website != null || wikipedia != null || address != null) {
		if(website != null) {
			ELEMENT.LOCATION.LINKS.innerHTML += '<a href="' + website + '" alt="Go to official website" title="Go to official website" target="_blank"><i class="icon left material-icons-outline md-18 md-open_in_browser"></i></a>';
		}

		if(wikipedia != null) {
			ELEMENT.LOCATION.LINKS.innerHTML += '<a href="' + wikipedia + '" alt="Read related Wikipedia article" title="Read related Wikipedia article" target="_blank"><i class="icon left material-icons-outline md-18 md-school"></i></a>';
		}

		if(address != null) {
			ELEMENT.LOCATION.ADDRESS.innerHTML += '<i class="icon left material-icons-outline md-pin_drop"></i><span>' + typogr.typogrify(address.replace(/\n/g, '<br />')) + '</span>';
			ELEMENT.LOCATION.ADDRESS.classList.remove('empty');
		} else {
			ELEMENT.LOCATION.ADDRESS.classList.add('empty');
		}
	}

	ELEMENT.LOCATION.LINKS.classList.remove('empty'); // Remove hidden class vom links container, as it always has content

	// Display metadata
	ELEMENT.LOCATION.MEDIA.innerHTML = '<i class="icon left material-icons-outline md-movie"></i>' + '<a onclick="switchToMedia()" title="' + media + '">' + typogr.typogrify(media) + '</a>';

	ELEMENT.LOCATION.SOURCE.innerHTML = '<i class="icon left material-icons-outline md-format_quote"></i>' + '<span>' + typogr.typogrify(source) + '</span>';

	if(PATH.includes('/tv/')) {
		var season;
		var episode;

		if(seasonEpisode.season && seasonEpisode.season.includes('-')) {
			var season = seasonEpisode.season.split('-');
			var season = 'S' + season[0].padStart(2,0) + '-' + season[1].padStart(2,0);
		} else if (seasonEpisode.season && !seasonEpisode.season.includes('-')) {
			var season = 'S' + seasonEpisode.season.padStart(2,0);
		} else {
			var season = null;
		}

		if(seasonEpisode.episode && seasonEpisode.episode.includes('-')) {
			var episode = seasonEpisode.episode.split('-');
			var episode = 'E' + episode[0].padStart(2,0) + '-' + episode[1].padStart(2,0);
		} else if (seasonEpisode.episode && !seasonEpisode.episode.includes('-')) {
			var episode = 'E' + seasonEpisode.episode.padStart(2,0);
		} else {
			var episode = null;
		}

		if(season && episode) {
			ELEMENT.LOCATION.SEASONEPISODE.innerHTML = '<i class="icon left material-icons-outline md-format_list_numbered"></i>' + '<span>' + typogr.typogrify(season + episode) + '</span>';
		} else if(season && !episode) {
			ELEMENT.LOCATION.SEASONEPISODE.innerHTML = '<i class="icon left material-icons-outline md-format_list_numbered"></i>' + '<span>' + typogr.typogrify(season) + '</span>';
		}
	}

	// Display native name, if available
	if(nativeName != null) {
		ELEMENT.LOCATION.NATIVE.innerHTML = '<i class="icon left material-icons-outline md-18 md-language"></i><span>' + typogr.typogrify(nativeName) + '</span>';
		ELEMENT.LOCATION.NATIVE.classList.remove('empty');
	} else {
		ELEMENT.LOCATION.NATIVE.classList.add('empty');
	}

	initSpoilers();

	if(isMobile) {
		showContentBox();
	}

	switchToLocation(); // Slide effect between media and location
}

// Define breakpoints
var windowWidth = window.innerWidth;

function isMobile() {
	if(windowWidth <= 768) {
		return true;
	} else {
		return false;
	}
}

// Display page title in <title> tag
function displayTitle(pageTitle) {
	if(pageTitle == undefined) {
		document.title = 'filming location database';
	} else {
		document.title = pageTitle + ' [filming location database]';
	}
}

function toggleOverlay() {
	if(ELEMENT.OVERLAY.classList.contains('show')) {
		ELEMENT.OVERLAY.classList.remove('show');
	} else {
		ELEMENT.OVERLAY.classList.add('show');
	}
}

function close(el) {
	el.target.parentNode.classList.remove('show');
}

// Display (error) messages
function displayMessage(options) {
	var type = options.type;
	var message = options.message;
	var el = ELEMENT.CONTENT.getElementsByClassName('messages')[0];
	var icon;

	if (options.type == 'error') {
		icon = '<i class="icon left material-icons-outline md-18 md-error"></i>';
	} else if (options.type == 'success') {
		icon = '<i class="icon left material-icons-outline md-18 md-thumb_up"></i>';
	} else {
		icon = '<i class="icon left material-icons-outline md-18 md-info"></i>'; // Fallback icon
		type = 'info';
	}

	if(options.message != undefined) {
		message = options.message;
	} else {
		message = 'We encountered an error, please try again.';
	}

	el.classList.add(type); // Add class with the name of the message type
	el.classList.remove('empty'); // remove 'empty' class
	el.innerHTML = icon + '<span>' + typogr.typogrify(message) + '</span>';

	// Sometimes there is a loader active. It needs to be removed as well
	var loader = document.getElementsByClassName('loader')[0];
	if(loader != undefined) {
		loader.remove();
	}
}

// Get header offset
function headerOffset() {
	var headerElement = document.getElementById('page-header');
	return headerElement.offsetHeight;
}

function initMap() {
	if(isMobile()) {
		var zoom = 2;
		var position = 'bottomright';
	} else {
		var zoom = 3;
		var position = 'topleft';
	}

	// Add tile layer to map
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright" title="Map copyright and License" target="_blank">OpenStreetMap contributors</a>'
	}).addTo(MAP);

	// Set control position and minimal zoom
	MAP.zoomControl.setPosition(position);
	MAP.options.minZoom = zoom;

	// Make the Leaflet link open in a new window
	MAP.attributionControl.setPrefix('<a href="https://leafletjs.com" title="A JS library for interactive maps" target="_blank">Leaflet</a>');

	// Add offset to control
	var leafletZoom = document.getElementsByClassName('leaflet-control leaflet-control-zoom');
	if(!isMobile()) {
		leafletZoom[0].style.top = headerOffset() + 8 + 'px';
	}
	// If it's on mobile, we load the offset after the menu is populated directy
	// In the ./navigation.js file itself
}

// Container elements
var searchContainer = document.getElementById('search-container');
var searchBox = document.getElementById('search-box');
var suggestionsContainer = document.getElementById('search-suggestions');
var suggestionsList = document.getElementById('suggestions');

function buildSuggestions(key) {
	searchMediaEntries(key)
		.then(res => {
			if(res.data && res.data.length == 0) {
				var searchbox = document.getElementById('search-box');
				var searchValue = searchbox.value;
				suggestionsList.innerHTML = '<div class="messages"><i class="icon left material-icons-outline md-18 md-sentiment_dissatisfied"></i>Oops, we found no title matching ' + typogr.typogrify('"' + searchValue + '"') + '</span>.<br /><a href="/submit-location" alt="Submit location" title="Submit location"><i class="icon left material-icons md-18 md-add_location_alt"></i>Add new location</span></a></div>';
			} else {
				displaySuggestions(res);
			}
		})
		.catch(() => {
			suggestionsList.innerHTML = 'We had troubles getting the search results. Please reload the page and try again.'; // Clear the suggestions list before populating it
		});
}

function displaySuggestions(res) {
	suggestionsList.innerHTML = ''; // Clear the suggestions list before populating it
	var data = res.data;

	data.forEach(entry => {
		var icon;
		var path = entry.attributes.path.alias;
		var name = entry.attributes.name;
		var type = entry.attributes.field_type;

		if(type == 'm') {
			icon = '<i class="icon left material-icons-outline md-18 md-theaters"></i>';
		} else {
			icon = '<i class="icon left material-icons-outline md-18 md-live_tv"></i>';
		}

		suggestionsList.innerHTML += '<li><a href="' + path + '" title="' + name + '">' + icon + typogr.typogrify(name) + '</a></li>';
	});
}

function search() {
	var newestEntries = document.getElementById('newest');
	if(searchBox.value.length > 0 && searchBox.value.length < 3) {
		newestEntries.classList.remove('show');
		suggestionsList.innerHTML = 'Please type 3 or more characters...';
	} else if(searchBox.value.length >= 3) {
		newestEntries.classList.remove('show');
		showLoader('suggestions');
		buildSuggestions(searchBox.value);
	} else {
		newestEntries.classList.add('show');
		suggestionsList.innerHTML = '';
	}
}

function displayNewestEntries() {
	var entriesContainer = document.getElementById('newest-entries');

	getNewestEntries(3)
		.then(json => {
			json = json.data;
			json.forEach(media => {
				var name = media.attributes.name;
				var type = media.attributes.field_type;
				var path = media.attributes.path.alias;
				var created = new Date(media.attributes.revision_created);
				let date = created.getUTCDate() + '.' + (created.getMonth() + 1) + '.' + created.getUTCFullYear();

				var icon;
				if(type == 'm') {
					icon = '<i class="icon left material-icons-outline md-18 md-theaters"></i>';
				} else {
					icon = '<i class="icon left material-icons-outline md-18 md-live_tv"></i>';
				}

				entriesContainer.innerHTML += '<li><a href="' + path + '" title="' + name + '">' + icon +  typogr.typogrify(name) + '</a> (' + date + ')</li>';
			});
		});
}

function initSearch() {
	searchBox.addEventListener('keyup', search);
	displayNewestEntries();
}

function toggleSearchSuggestionsBoxByClick() {
	document.body.addEventListener('click', function (event) {
		// Hide search suggestions if clicked anywhere outside of searchSuggestions
		if(!searchContainer.contains(event.target) && suggestionsContainer.classList.contains('show')) {
			suggestionsContainer.classList.remove('show');
		}

		// Show search suggestions when search field is clicked and suggestions are there
		if(searchBox.contains(event.target) && !suggestionsContainer.classList.contains('show')) {
			suggestionsContainer.classList.add('show');
		}
	});
}

var headerMenu = document.getElementById('header-menu');
var footerMenu = document.getElementById('footer-menu');

function addHamburgerEventListener() {
	var menuButton = document.getElementById('hamburger');
	menuButton.addEventListener('click', showSiteInfo);
}

function buildMenus() {
	// Populate menus with the links
	fetch(CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;
			var footerMenuItems = [];

			json.forEach(page => {
				var path = page.path;
				var name = page.menu.name;
				var position = page.menu.position;
				var rel = page.menu.rel;
				var linkClass;
				var icon = page.menu.icon;

				if(path ==  PATH) {
					linkClass = 'class="active"';
				} else {
					linkClass = '';
				}

				if(icon) {
					icon = '<i class="icon material-icons md-18 ' + icon + '"></i>';
				} else {
					icon = '';
				}

				if(position == 'header') {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					headerMenu.innerHTML += '<li><a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + icon + name + '</a></li>';
				} else {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					// Because the footer menu already contains items from index.php we save them
					// In an array, and insert them after the forEach loop to have better control
					// Over the menu order
					if(position != undefined) {
						var listItem = document.createElement('li');
						listItem.innerHTML = '<a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + icon + name + '</a></li>';
						footerMenuItems.push(listItem);
					}
				}
			});

			// And now we add footer menu items to menu
			footerMenuItems.reverse().forEach(item => {
				footerMenu.insertBefore(item, footerMenu.childNodes[0]);
			});

			// Add hamburger icon at the end of the header menu
			headerMenu.innerHTML += '<li id="hamburger"><i class="icon material-icons md-36 md-menu"></i></li>';

			// Add event listener for hamburger icon
			addHamburgerEventListener();
		})
		.catch(err => {
			console.log('Error loading menus: ' + err);
		});
}

function showSiteInfo() {
	// Fetch and display content
	var titleElement = document.getElementById('about-title');
	var contentElement = document.getElementById('about-content');

	fetch(CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;

			json.forEach(page => {
				// Add the content depending on the path
				if(page.path == '/about') {
					titleElement.innerHTML = typogr.typogrify(page.title);
					contentElement.innerHTML = typogr.typogrify(page.content);
				}
			});
		})
		.catch(() => displayError());

	ELEMENT.ASIDE.classList.toggle('show'); // Toggle show class
	aboutBoxOffset(); // Set about box offset
	toggleHamburgerIcon(); // Toggle hamburger icon
	toggleOverlay();
}

function toggleHamburgerIcon() {
	// Toggle menu icon
	var menuButton = document.getElementById('hamburger');

	if(ELEMENT.ASIDE.classList.contains('show')) {
		menuButton.innerHTML = '<i class="icon material-icons md-36 md-close"></i>';
	} else {
		menuButton.innerHTML = '<i class="icon material-icons md-36 md-menu"></i>';
	}
}

/*!
 * OverlayScrollbars
 * Version: 2.10.0
 *
 * Copyright (c) Rene Haas | KingSora.
 * https://github.com/KingSora
 *
 * Released under the MIT license.
 */
const createCache = (t, n) => {
  const {o: o, i: s, u: e} = t;
  let c = o;
  let r;
  const cacheUpdateContextual = (t, n) => {
    const o = c;
    const l = t;
    const i = n || (s ? !s(o, l) : o !== l);
    if (i || e) {
      c = l;
      r = o;
    }
    return [ c, i, r ];
  };
  const cacheUpdateIsolated = t => cacheUpdateContextual(n(c, r), t);
  const getCurrentCache = t => [ c, !!t, r ];
  return [ n ? cacheUpdateIsolated : cacheUpdateContextual, getCurrentCache ];
};

const t = typeof window !== "undefined" && typeof HTMLElement !== "undefined" && !!window.document;

const n = t ? window : {};

const o = Math.max;

const s = Math.min;

const e = Math.round;

const c = Math.abs;

const r = Math.sign;

const l = n.cancelAnimationFrame;

const i = n.requestAnimationFrame;

const a = n.setTimeout;

const u = n.clearTimeout;

const getApi = t => typeof n[t] !== "undefined" ? n[t] : void 0;

const _ = getApi("MutationObserver");

const d = getApi("IntersectionObserver");

const f = getApi("ResizeObserver");

const v = getApi("ScrollTimeline");

const isUndefined = t => t === void 0;

const isNull = t => t === null;

const isNumber = t => typeof t === "number";

const isString = t => typeof t === "string";

const isBoolean = t => typeof t === "boolean";

const isFunction = t => typeof t === "function";

const isArray = t => Array.isArray(t);

const isObject = t => typeof t === "object" && !isArray(t) && !isNull(t);

const isArrayLike = t => {
  const n = !!t && t.length;
  const o = isNumber(n) && n > -1 && n % 1 == 0;
  return isArray(t) || !isFunction(t) && o ? n > 0 && isObject(t) ? n - 1 in t : true : false;
};

const isPlainObject = t => !!t && t.constructor === Object;

const isHTMLElement = t => t instanceof HTMLElement;

const isElement = t => t instanceof Element;

function each(t, n) {
  if (isArrayLike(t)) {
    for (let o = 0; o < t.length; o++) {
      if (n(t[o], o, t) === false) {
        break;
      }
    }
  } else if (t) {
    each(Object.keys(t), (o => n(t[o], o, t)));
  }
  return t;
}

const inArray = (t, n) => t.indexOf(n) >= 0;

const concat = (t, n) => t.concat(n);

const push = (t, n, o) => {
  !isString(n) && isArrayLike(n) ? Array.prototype.push.apply(t, n) : t.push(n);
  return t;
};

const from = t => Array.from(t || []);

const createOrKeepArray = t => {
  if (isArray(t)) {
    return t;
  }
  return !isString(t) && isArrayLike(t) ? from(t) : [ t ];
};

const isEmptyArray = t => !!t && !t.length;

const deduplicateArray = t => from(new Set(t));

const runEachAndClear = (t, n, o) => {
  const runFn = t => t ? t.apply(void 0, n || []) : true;
  each(t, runFn);
  !o && (t.length = 0);
};

const p = "paddingTop";

const h = "paddingRight";

const g = "paddingLeft";

const b = "paddingBottom";

const w = "marginLeft";

const y = "marginRight";

const S = "marginBottom";

const m = "overflowX";

const O = "overflowY";

const $ = "width";

const C = "height";

const x = "visible";

const H = "hidden";

const E = "scroll";

const capitalizeFirstLetter = t => {
  const n = String(t || "");
  return n ? n[0].toUpperCase() + n.slice(1) : "";
};

const equal = (t, n, o, s) => {
  if (t && n) {
    let e = true;
    each(o, (o => {
      const c = t[o];
      const r = n[o];
      if (c !== r) {
        e = false;
      }
    }));
    return e;
  }
  return false;
};

const equalWH = (t, n) => equal(t, n, [ "w", "h" ]);

const equalXY = (t, n) => equal(t, n, [ "x", "y" ]);

const equalTRBL = (t, n) => equal(t, n, [ "t", "r", "b", "l" ]);

const noop = () => {};

const bind = (t, ...n) => t.bind(0, ...n);

const selfClearTimeout = t => {
  let n;
  const o = t ? a : i;
  const s = t ? u : l;
  return [ e => {
    s(n);
    n = o((() => e()), isFunction(t) ? t() : t);
  }, () => s(n) ];
};

const debounce = (t, n) => {
  const {_: o, v: s, p: e, S: c} = n || {};
  let r;
  let _;
  let d;
  let f;
  let v = noop;
  const p = function invokeFunctionToDebounce(n) {
    v();
    u(r);
    f = r = _ = void 0;
    v = noop;
    t.apply(this, n);
  };
  const mergeParms = t => c && _ ? c(_, t) : t;
  const flush = () => {
    if (v !== noop) {
      p(mergeParms(d) || d);
    }
  };
  const h = function debouncedFn() {
    const t = from(arguments);
    const n = isFunction(o) ? o() : o;
    const c = isNumber(n) && n >= 0;
    if (c) {
      const o = isFunction(s) ? s() : s;
      const c = isNumber(o) && o >= 0;
      const h = n > 0 ? a : i;
      const g = n > 0 ? u : l;
      const b = mergeParms(t);
      const w = b || t;
      const y = p.bind(0, w);
      let S;
      v();
      if (e && !f) {
        y();
        f = true;
        S = h((() => f = void 0), n);
      } else {
        S = h(y, n);
        if (c && !r) {
          r = a(flush, o);
        }
      }
      v = () => g(S);
      _ = d = w;
    } else {
      p(t);
    }
  };
  h.m = flush;
  return h;
};

const hasOwnProperty = (t, n) => Object.prototype.hasOwnProperty.call(t, n);

const keys = t => t ? Object.keys(t) : [];

const assignDeep = (t, n, o, s, e, c, r) => {
  const l = [ n, o, s, e, c, r ];
  if ((typeof t !== "object" || isNull(t)) && !isFunction(t)) {
    t = {};
  }
  each(l, (n => {
    each(n, ((o, s) => {
      const e = n[s];
      if (t === e) {
        return true;
      }
      const c = isArray(e);
      if (e && isPlainObject(e)) {
        const n = t[s];
        let o = n;
        if (c && !isArray(n)) {
          o = [];
        } else if (!c && !isPlainObject(n)) {
          o = {};
        }
        t[s] = assignDeep(o, e);
      } else {
        t[s] = c ? e.slice() : e;
      }
    }));
  }));
  return t;
};

const removeUndefinedProperties = (t, n) => each(assignDeep({}, t), ((t, o, s) => {
  if (t === void 0) {
    delete s[o];
  } else if (t && isPlainObject(t)) {
    s[o] = removeUndefinedProperties(t);
  }
}));

const isEmptyObject = t => !keys(t).length;

const capNumber = (t, n, e) => o(t, s(n, e));

const getDomTokensArray = t => deduplicateArray((isArray(t) ? t : (t || "").split(" ")).filter((t => t)));

const getAttr = (t, n) => t && t.getAttribute(n);

const hasAttr = (t, n) => t && t.hasAttribute(n);

const setAttrs = (t, n, o) => {
  each(getDomTokensArray(n), (n => {
    t && t.setAttribute(n, String(o || ""));
  }));
};

const removeAttrs = (t, n) => {
  each(getDomTokensArray(n), (n => t && t.removeAttribute(n)));
};

const domTokenListAttr = (t, n) => {
  const o = getDomTokensArray(getAttr(t, n));
  const s = bind(setAttrs, t, n);
  const domTokenListOperation = (t, n) => {
    const s = new Set(o);
    each(getDomTokensArray(t), (t => {
      s[n](t);
    }));
    return from(s).join(" ");
  };
  return {
    O: t => s(domTokenListOperation(t, "delete")),
    $: t => s(domTokenListOperation(t, "add")),
    C: t => {
      const n = getDomTokensArray(t);
      return n.reduce(((t, n) => t && o.includes(n)), n.length > 0);
    }
  };
};

const removeAttrClass = (t, n, o) => {
  domTokenListAttr(t, n).O(o);
  return bind(addAttrClass, t, n, o);
};

const addAttrClass = (t, n, o) => {
  domTokenListAttr(t, n).$(o);
  return bind(removeAttrClass, t, n, o);
};

const addRemoveAttrClass = (t, n, o, s) => (s ? addAttrClass : removeAttrClass)(t, n, o);

const hasAttrClass = (t, n, o) => domTokenListAttr(t, n).C(o);

const createDomTokenListClass = t => domTokenListAttr(t, "class");

const removeClass = (t, n) => {
  createDomTokenListClass(t).O(n);
};

const addClass = (t, n) => {
  createDomTokenListClass(t).$(n);
  return bind(removeClass, t, n);
};

const find = (t, n) => {
  const o = n ? isElement(n) && n : document;
  return o ? from(o.querySelectorAll(t)) : [];
};

const findFirst = (t, n) => {
  const o = n ? isElement(n) && n : document;
  return o && o.querySelector(t);
};

const is = (t, n) => isElement(t) && t.matches(n);

const isBodyElement = t => is(t, "body");

const contents = t => t ? from(t.childNodes) : [];

const parent = t => t && t.parentElement;

const closest = (t, n) => isElement(t) && t.closest(n);

const getFocusedElement = t => (document).activeElement;

const liesBetween = (t, n, o) => {
  const s = closest(t, n);
  const e = t && findFirst(o, s);
  const c = closest(e, n) === s;
  return s && e ? s === t || e === t || c && closest(closest(t, o), n) !== s : false;
};

const removeElements = t => {
  each(createOrKeepArray(t), (t => {
    const n = parent(t);
    t && n && n.removeChild(t);
  }));
};

const appendChildren = (t, n) => bind(removeElements, t && n && each(createOrKeepArray(n), (n => {
  n && t.appendChild(n);
})));

const createDiv = t => {
  const n = document.createElement("div");
  setAttrs(n, "class", t);
  return n;
};

const createDOM = t => {
  const n = createDiv();
  n.innerHTML = t.trim();
  return each(contents(n), (t => removeElements(t)));
};

const getCSSVal = (t, n) => t.getPropertyValue(n) || t[n] || "";

const validFiniteNumber = t => {
  const n = t || 0;
  return isFinite(n) ? n : 0;
};

const parseToZeroOrNumber = t => validFiniteNumber(parseFloat(t || ""));

const roundCssNumber = t => Math.round(t * 1e4) / 1e4;

const numberToCssPx = t => `${roundCssNumber(validFiniteNumber(t))}px`;

function setStyles(t, n) {
  t && n && each(n, ((n, o) => {
    try {
      const s = t.style;
      const e = isNull(n) || isBoolean(n) ? "" : isNumber(n) ? numberToCssPx(n) : n;
      if (o.indexOf("--") === 0) {
        s.setProperty(o, e);
      } else {
        s[o] = e;
      }
    } catch (s) {}
  }));
}

function getStyles(t, o, s) {
  const e = isString(o);
  let c = e ? "" : {};
  if (t) {
    const r = n.getComputedStyle(t, s) || t.style;
    c = e ? getCSSVal(r, o) : from(o).reduce(((t, n) => {
      t[n] = getCSSVal(r, n);
      return t;
    }), c);
  }
  return c;
}

const topRightBottomLeft = (t, n, o) => {
  const s = n ? `${n}-` : "";
  const e = o ? `-${o}` : "";
  const c = `${s}top${e}`;
  const r = `${s}right${e}`;
  const l = `${s}bottom${e}`;
  const i = `${s}left${e}`;
  const a = getStyles(t, [ c, r, l, i ]);
  return {
    t: parseToZeroOrNumber(a[c]),
    r: parseToZeroOrNumber(a[r]),
    b: parseToZeroOrNumber(a[l]),
    l: parseToZeroOrNumber(a[i])
  };
};

const getTrasformTranslateValue = (t, n) => `translate${isObject(t) ? `(${t.x},${t.y})` : `${"Y"}(${t})`}`;

const elementHasDimensions = t => !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length);

const z = {
  w: 0,
  h: 0
};

const getElmWidthHeightProperty = (t, n) => n ? {
  w: n[`${t}Width`],
  h: n[`${t}Height`]
} : z;

const getWindowSize = t => getElmWidthHeightProperty("inner", t || n);

const I = bind(getElmWidthHeightProperty, "offset");

const A = bind(getElmWidthHeightProperty, "client");

const D = bind(getElmWidthHeightProperty, "scroll");

const getFractionalSize = t => {
  const n = parseFloat(getStyles(t, $)) || 0;
  const o = parseFloat(getStyles(t, C)) || 0;
  return {
    w: n - e(n),
    h: o - e(o)
  };
};

const getBoundingClientRect = t => t.getBoundingClientRect();

const hasDimensions = t => !!t && elementHasDimensions(t);

const domRectHasDimensions = t => !!(t && (t[C] || t[$]));

const domRectAppeared = (t, n) => {
  const o = domRectHasDimensions(t);
  const s = domRectHasDimensions(n);
  return !s && o;
};

const removeEventListener = (t, n, o, s) => {
  each(getDomTokensArray(n), (n => {
    t && t.removeEventListener(n, o, s);
  }));
};

const addEventListener = (t, n, o, s) => {
  var e;
  const c = (e = s && s.H) != null ? e : true;
  const r = s && s.I || false;
  const l = s && s.A || false;
  const i = {
    passive: c,
    capture: r
  };
  return bind(runEachAndClear, getDomTokensArray(n).map((n => {
    const s = l ? e => {
      removeEventListener(t, n, s, r);
      o && o(e);
    } : o;
    t && t.addEventListener(n, s, i);
    return bind(removeEventListener, t, n, s, r);
  })));
};

const stopPropagation = t => t.stopPropagation();

const preventDefault = t => t.preventDefault();

const stopAndPrevent = t => stopPropagation(t) || preventDefault(t);

const scrollElementTo = (t, n) => {
  const {x: o, y: s} = isNumber(n) ? {
    x: n,
    y: n
  } : n || {};
  isNumber(o) && (t.scrollLeft = o);
  isNumber(s) && (t.scrollTop = s);
};

const getElementScroll = t => ({
  x: t.scrollLeft,
  y: t.scrollTop
});

const getZeroScrollCoordinates = () => ({
  D: {
    x: 0,
    y: 0
  },
  M: {
    x: 0,
    y: 0
  }
});

const sanitizeScrollCoordinates = (t, n) => {
  const {D: o, M: s} = t;
  const {w: e, h: l} = n;
  const sanitizeAxis = (t, n, o) => {
    let s = r(t) * o;
    let e = r(n) * o;
    if (s === e) {
      const o = c(t);
      const r = c(n);
      e = o > r ? 0 : e;
      s = o < r ? 0 : s;
    }
    s = s === e ? 0 : s;
    return [ s + 0, e + 0 ];
  };
  const [i, a] = sanitizeAxis(o.x, s.x, e);
  const [u, _] = sanitizeAxis(o.y, s.y, l);
  return {
    D: {
      x: i,
      y: u
    },
    M: {
      x: a,
      y: _
    }
  };
};

const isDefaultDirectionScrollCoordinates = ({D: t, M: n}) => {
  const getAxis = (t, n) => t === 0 && t <= n;
  return {
    x: getAxis(t.x, n.x),
    y: getAxis(t.y, n.y)
  };
};

const getScrollCoordinatesPercent = ({D: t, M: n}, o) => {
  const getAxis = (t, n, o) => capNumber(0, 1, (t - o) / (t - n) || 0);
  return {
    x: getAxis(t.x, n.x, o.x),
    y: getAxis(t.y, n.y, o.y)
  };
};

const focusElement = t => {
  if (t && t.focus) {
    t.focus({
      preventScroll: true
    });
  }
};

const manageListener = (t, n) => {
  each(createOrKeepArray(n), t);
};

const createEventListenerHub = t => {
  const n = new Map;
  const removeEvent = (t, o) => {
    if (t) {
      const s = n.get(t);
      manageListener((t => {
        if (s) {
          s[t ? "delete" : "clear"](t);
        }
      }), o);
    } else {
      n.forEach((t => {
        t.clear();
      }));
      n.clear();
    }
  };
  const addEvent = (t, o) => {
    if (isString(t)) {
      const s = n.get(t) || new Set;
      n.set(t, s);
      manageListener((t => {
        isFunction(t) && s.add(t);
      }), o);
      return bind(removeEvent, t, o);
    }
    if (isBoolean(o) && o) {
      removeEvent();
    }
    const s = keys(t);
    const e = [];
    each(s, (n => {
      const o = t[n];
      o && push(e, addEvent(n, o));
    }));
    return bind(runEachAndClear, e);
  };
  const triggerEvent = (t, o) => {
    each(from(n.get(t)), (t => {
      if (o && !isEmptyArray(o)) {
        t.apply(0, o);
      } else {
        t();
      }
    }));
  };
  addEvent(t || {});
  return [ addEvent, removeEvent, triggerEvent ];
};

const opsStringify = t => JSON.stringify(t, ((t, n) => {
  if (isFunction(n)) {
    throw 0;
  }
  return n;
}));

const getPropByPath = (t, n) => t ? `${n}`.split(".").reduce(((t, n) => t && hasOwnProperty(t, n) ? t[n] : void 0), t) : void 0;

const M = {
  paddingAbsolute: false,
  showNativeOverlaidScrollbars: false,
  update: {
    elementEvents: [ [ "img", "load" ] ],
    debounce: [ 0, 33 ],
    attributes: null,
    ignoreMutation: null
  },
  overflow: {
    x: "scroll",
    y: "scroll"
  },
  scrollbars: {
    theme: "os-theme-dark",
    visibility: "auto",
    autoHide: "never",
    autoHideDelay: 1300,
    autoHideSuspend: false,
    dragScroll: true,
    clickScroll: false,
    pointers: [ "mouse", "touch", "pen" ]
  }
};

const getOptionsDiff = (t, n) => {
  const o = {};
  const s = concat(keys(n), keys(t));
  each(s, (s => {
    const e = t[s];
    const c = n[s];
    if (isObject(e) && isObject(c)) {
      assignDeep(o[s] = {}, getOptionsDiff(e, c));
      if (isEmptyObject(o[s])) {
        delete o[s];
      }
    } else if (hasOwnProperty(n, s) && c !== e) {
      let t = true;
      if (isArray(e) || isArray(c)) {
        try {
          if (opsStringify(e) === opsStringify(c)) {
            t = false;
          }
        } catch (r) {}
      }
      if (t) {
        o[s] = c;
      }
    }
  }));
  return o;
};

const createOptionCheck = (t, n, o) => s => [ getPropByPath(t, s), o || getPropByPath(n, s) !== void 0 ];

const T = `data-overlayscrollbars`;

const k = "os-environment";

const R = `${k}-scrollbar-hidden`;

const V = `${T}-initialize`;

const L$1 = "noClipping";

const U = `${T}-body`;

const P = T;

const N = "host";

const q = `${T}-viewport`;

const B = m;

const F = O;

const j = "arrange";

const X = "measuring";

const Y = "scrolling";

const W = "scrollbarHidden";

const J = "noContent";

const G = `${T}-padding`;

const K = `${T}-content`;

const Q = "os-size-observer";

const Z = `${Q}-appear`;

const tt = `${Q}-listener`;

const et = "os-trinsic-observer";

const ct = "os-theme-none";

const rt = "os-scrollbar";

const lt = `${rt}-rtl`;

const it = `${rt}-horizontal`;

const at = `${rt}-vertical`;

const ut = `${rt}-track`;

const _t = `${rt}-handle`;

const dt = `${rt}-visible`;

const ft = `${rt}-cornerless`;

const vt = `${rt}-interaction`;

const pt = `${rt}-unusable`;

const ht = `${rt}-auto-hide`;

const gt = `${ht}-hidden`;

const bt = `${rt}-wheel`;

const wt = `${ut}-interactive`;

const yt = `${_t}-interactive`;

let St;

const getNonce = () => St;

const setNonce = t => {
  St = t;
};

let mt;

const createEnvironment = () => {
  const getNativeScrollbarSize = (t, n, o) => {
    appendChildren(document.body, t);
    appendChildren(document.body, t);
    const s = A(t);
    const e = I(t);
    const c = getFractionalSize(n);
    o && removeElements(t);
    return {
      x: e.h - s.h + c.h,
      y: e.w - s.w + c.w
    };
  };
  const getNativeScrollbarsHiding = t => {
    let n = false;
    const o = addClass(t, R);
    try {
      n = getStyles(t, "scrollbar-width") === "none" || getStyles(t, "display", "::-webkit-scrollbar") === "none";
    } catch (s) {}
    o();
    return n;
  };
  const t = `.${k}{scroll-behavior:auto!important;position:fixed;opacity:0;visibility:hidden;overflow:scroll;height:200px;width:200px;z-index:-1}.${k} div{width:200%;height:200%;margin:10px 0}.${R}{scrollbar-width:none!important}.${R}::-webkit-scrollbar,.${R}::-webkit-scrollbar-corner{appearance:none!important;display:none!important;width:0!important;height:0!important}`;
  const o = createDOM(`<div class="${k}"><div></div><style>${t}</style></div>`);
  const s = o[0];
  const e = s.firstChild;
  const c = s.lastChild;
  const r = getNonce();
  if (r) {
    c.nonce = r;
  }
  const [l, , i] = createEventListenerHub();
  const [a, u] = createCache({
    o: getNativeScrollbarSize(s, e),
    i: equalXY
  }, bind(getNativeScrollbarSize, s, e, true));
  const [_] = u();
  const d = getNativeScrollbarsHiding(s);
  const f = {
    x: _.x === 0,
    y: _.y === 0
  };
  const p = {
    elements: {
      host: null,
      padding: !d,
      viewport: t => d && isBodyElement(t) && t,
      content: false
    },
    scrollbars: {
      slot: true
    },
    cancel: {
      nativeScrollbarsOverlaid: false,
      body: null
    }
  };
  const h = assignDeep({}, M);
  const g = bind(assignDeep, {}, h);
  const b = bind(assignDeep, {}, p);
  const w = {
    T: _,
    k: f,
    R: d,
    V: !!v,
    L: bind(l, "r"),
    U: b,
    P: t => assignDeep(p, t) && b(),
    N: g,
    q: t => assignDeep(h, t) && g(),
    B: assignDeep({}, p),
    F: assignDeep({}, h)
  };
  removeAttrs(s, "style");
  removeElements(s);
  addEventListener(n, "resize", (() => {
    i("r", []);
  }));
  if (isFunction(n.matchMedia) && !d && (!f.x || !f.y)) {
    const addZoomListener = t => {
      const o = n.matchMedia(`(resolution: ${n.devicePixelRatio}dppx)`);
      addEventListener(o, "change", (() => {
        t();
        addZoomListener(t);
      }), {
        A: true
      });
    };
    addZoomListener((() => {
      const [t, n] = a();
      assignDeep(w.T, t);
      i("r", [ n ]);
    }));
  }
  return w;
};

const getEnvironment = () => {
  if (!mt) {
    mt = createEnvironment();
  }
  return mt;
};

const resolveInitialization = (t, n) => isFunction(n) ? n.apply(0, t) : n;

const staticInitializationElement = (t, n, o, s) => {
  const e = isUndefined(s) ? o : s;
  const c = resolveInitialization(t, e);
  return c || n.apply(0, t);
};

const dynamicInitializationElement = (t, n, o, s) => {
  const e = isUndefined(s) ? o : s;
  const c = resolveInitialization(t, e);
  return !!c && (isHTMLElement(c) ? c : n.apply(0, t));
};

const cancelInitialization = (t, n) => {
  const {nativeScrollbarsOverlaid: o, body: s} = n || {};
  const {k: e, R: c, U: r} = getEnvironment();
  const {nativeScrollbarsOverlaid: l, body: i} = r().cancel;
  const a = o != null ? o : l;
  const u = isUndefined(s) ? i : s;
  const _ = (e.x || e.y) && a;
  const d = t && (isNull(u) ? !c : u);
  return !!_ || !!d;
};

const Ot = new WeakMap;

const addInstance = (t, n) => {
  Ot.set(t, n);
};

const removeInstance = t => {
  Ot.delete(t);
};

const getInstance = t => Ot.get(t);

const createEventContentChange = (t, n, o) => {
  let s = false;
  const e = o ? new WeakMap : false;
  const destroy = () => {
    s = true;
  };
  const updateElements = c => {
    if (e && o) {
      const r = o.map((n => {
        const [o, s] = n || [];
        const e = s && o ? (c || find)(o, t) : [];
        return [ e, s ];
      }));
      each(r, (o => each(o[0], (c => {
        const r = o[1];
        const l = e.get(c) || [];
        const i = t.contains(c);
        if (i && r) {
          const t = addEventListener(c, r, (o => {
            if (s) {
              t();
              e.delete(c);
            } else {
              n(o);
            }
          }));
          e.set(c, push(l, t));
        } else {
          runEachAndClear(l);
          e.delete(c);
        }
      }))));
    }
  };
  updateElements();
  return [ destroy, updateElements ];
};

const createDOMObserver = (t, n, o, s) => {
  let e = false;
  const {j: c, X: r, Y: l, W: i, J: a, G: u} = s || {};
  const d = debounce((() => e && o(true)), {
    _: 33,
    v: 99
  });
  const [f, v] = createEventContentChange(t, d, l);
  const p = c || [];
  const h = r || [];
  const g = concat(p, h);
  const observerCallback = (e, c) => {
    if (!isEmptyArray(c)) {
      const r = a || noop;
      const l = u || noop;
      const _ = [];
      const d = [];
      let f = false;
      let p = false;
      each(c, (o => {
        const {attributeName: e, target: c, type: a, oldValue: u, addedNodes: v, removedNodes: g} = o;
        const b = a === "attributes";
        const w = a === "childList";
        const y = t === c;
        const S = b && e;
        const m = S && getAttr(c, e || "");
        const O = isString(m) ? m : null;
        const $ = S && u !== O;
        const C = inArray(h, e) && $;
        if (n && (w || !y)) {
          const n = b && $;
          const a = n && i && is(c, i);
          const d = a ? !r(c, e, u, O) : !b || n;
          const f = d && !l(o, !!a, t, s);
          each(v, (t => push(_, t)));
          each(g, (t => push(_, t)));
          p = p || f;
        }
        if (!n && y && $ && !r(c, e, u, O)) {
          push(d, e);
          f = f || C;
        }
      }));
      v((t => deduplicateArray(_).reduce(((n, o) => {
        push(n, find(t, o));
        return is(o, t) ? push(n, o) : n;
      }), [])));
      if (n) {
        !e && p && o(false);
        return [ false ];
      }
      if (!isEmptyArray(d) || f) {
        const t = [ deduplicateArray(d), f ];
        !e && o.apply(0, t);
        return t;
      }
    }
  };
  const b = new _(bind(observerCallback, false));
  return [ () => {
    b.observe(t, {
      attributes: true,
      attributeOldValue: true,
      attributeFilter: g,
      subtree: n,
      childList: n,
      characterData: n
    });
    e = true;
    return () => {
      if (e) {
        f();
        b.disconnect();
        e = false;
      }
    };
  }, () => {
    if (e) {
      d.m();
      return observerCallback(true, b.takeRecords());
    }
  } ];
};

const $t = {};

const Ct = {};

const addPlugins = t => {
  each(t, (t => each(t, ((n, o) => {
    $t[o] = t[o];
  }))));
};

const registerPluginModuleInstances = (t, n, o) => keys(t).map((s => {
  const {static: e, instance: c} = t[s];
  const [r, l, i] = o || [];
  const a = o ? c : e;
  if (a) {
    const t = o ? a(r, l, n) : a(n);
    return (i || Ct)[s] = t;
  }
}));

const getStaticPluginModuleInstance = t => Ct[t];

const xt = "__osOptionsValidationPlugin";

const Ht = "__osSizeObserverPlugin";

const getShowNativeOverlaidScrollbars = (t, n) => {
  const {k: o} = n;
  const [s, e] = t("showNativeOverlaidScrollbars");
  return [ s && o.x && o.y, e ];
};

const overflowIsVisible = t => t.indexOf(x) === 0;

const createViewportOverflowState = (t, n) => {
  const getAxisOverflowStyle = (t, n, o, s) => {
    const e = t === x ? H : t.replace(`${x}-`, "");
    const c = overflowIsVisible(t);
    const r = overflowIsVisible(o);
    if (!n && !s) {
      return H;
    }
    if (c && r) {
      return x;
    }
    if (c) {
      const t = n ? x : H;
      return n && s ? e : t;
    }
    const l = r && s ? x : H;
    return n ? e : l;
  };
  const o = {
    x: getAxisOverflowStyle(n.x, t.x, n.y, t.y),
    y: getAxisOverflowStyle(n.y, t.y, n.x, t.x)
  };
  return {
    K: o,
    Z: {
      x: o.x === E,
      y: o.y === E
    }
  };
};

const zt = "__osScrollbarsHidingPlugin";

const At = "__osClickScrollPlugin";

const createSizeObserver = (t, n, o) => {
  const {dt: s} = o || {};
  const e = getStaticPluginModuleInstance(Ht);
  const [c] = createCache({
    o: false,
    u: true
  });
  return () => {
    const o = [];
    const r = createDOM(`<div class="${Q}"><div class="${tt}"></div></div>`);
    const l = r[0];
    const i = l.firstChild;
    const onSizeChangedCallbackProxy = t => {
      const o = t instanceof ResizeObserverEntry;
      let s = false;
      let e = false;
      if (o) {
        const [n, , o] = c(t.contentRect);
        const r = domRectHasDimensions(n);
        e = domRectAppeared(n, o);
        s = !e && !r;
      } else {
        e = t === true;
      }
      if (!s) {
        n({
          ft: true,
          dt: e
        });
      }
    };
    if (f) {
      const t = new f((t => onSizeChangedCallbackProxy(t.pop())));
      t.observe(i);
      push(o, (() => {
        t.disconnect();
      }));
    } else if (e) {
      const [t, n] = e(i, onSizeChangedCallbackProxy, s);
      push(o, concat([ addClass(l, Z), addEventListener(l, "animationstart", t) ], n));
    } else {
      return noop;
    }
    return bind(runEachAndClear, push(o, appendChildren(t, l)));
  };
};

const createTrinsicObserver = (t, n) => {
  let o;
  const isHeightIntrinsic = t => t.h === 0 || t.isIntersecting || t.intersectionRatio > 0;
  const s = createDiv(et);
  const [e] = createCache({
    o: false
  });
  const triggerOnTrinsicChangedCallback = (t, o) => {
    if (t) {
      const s = e(isHeightIntrinsic(t));
      const [, c] = s;
      return c && !o && n(s) && [ s ];
    }
  };
  const intersectionObserverCallback = (t, n) => triggerOnTrinsicChangedCallback(n.pop(), t);
  return [ () => {
    const n = [];
    if (d) {
      o = new d(bind(intersectionObserverCallback, false), {
        root: t
      });
      o.observe(s);
      push(n, (() => {
        o.disconnect();
      }));
    } else {
      const onSizeChanged = () => {
        const t = I(s);
        triggerOnTrinsicChangedCallback(t);
      };
      push(n, createSizeObserver(s, onSizeChanged)());
      onSizeChanged();
    }
    return bind(runEachAndClear, push(n, appendChildren(t, s)));
  }, () => o && intersectionObserverCallback(true, o.takeRecords()) ];
};

const createObserversSetup = (t, n, o, s) => {
  let e;
  let c;
  let r;
  let l;
  let i;
  let a;
  const u = `[${P}]`;
  const _ = `[${q}]`;
  const d = [ "id", "class", "style", "open", "wrap", "cols", "rows" ];
  const {vt: v, ht: p, ot: h, gt: g, bt: b, nt: w, wt: y, yt: S, St: m, Ot: O} = t;
  const getDirectionIsRTL = t => getStyles(t, "direction") === "rtl";
  const $ = {
    $t: false,
    ct: getDirectionIsRTL(v)
  };
  const C = getEnvironment();
  const x = getStaticPluginModuleInstance(zt);
  const [H] = createCache({
    i: equalWH,
    o: {
      w: 0,
      h: 0
    }
  }, (() => {
    const s = x && x.tt(t, n, $, C, o).ut;
    const e = y && w;
    const c = !e && hasAttrClass(p, P, L$1);
    const r = !w && S(j);
    const l = r && getElementScroll(g);
    const i = l && O();
    const a = m(X, c);
    const u = r && s && s()[0];
    const _ = D(h);
    const d = getFractionalSize(h);
    u && u();
    scrollElementTo(g, l);
    i && i();
    c && a();
    return {
      w: _.w + d.w,
      h: _.h + d.h
    };
  }));
  const E = debounce(s, {
    _: () => e,
    v: () => c,
    S(t, n) {
      const [o] = t;
      const [s] = n;
      return [ concat(keys(o), keys(s)).reduce(((t, n) => {
        t[n] = o[n] || s[n];
        return t;
      }), {}) ];
    }
  });
  const setDirection = t => {
    const n = getDirectionIsRTL(v);
    assignDeep(t, {
      Ct: a !== n
    });
    assignDeep($, {
      ct: n
    });
    a = n;
  };
  const onTrinsicChanged = (t, n) => {
    const [o, e] = t;
    const c = {
      xt: e
    };
    assignDeep($, {
      $t: o
    });
    !n && s(c);
    return c;
  };
  const onSizeChanged = ({ft: t, dt: n}) => {
    const o = t && !n;
    const e = !o && C.R ? E : s;
    const c = {
      ft: t || n,
      dt: n
    };
    setDirection(c);
    e(c);
  };
  const onContentMutation = (t, n) => {
    const [, o] = H();
    const e = {
      Ht: o
    };
    setDirection(e);
    const c = t ? s : E;
    o && !n && c(e);
    return e;
  };
  const onHostMutation = (t, n, o) => {
    const s = {
      Et: n
    };
    setDirection(s);
    if (n && !o) {
      E(s);
    }
    return s;
  };
  const [z, I] = b ? createTrinsicObserver(p, onTrinsicChanged) : [];
  const A = !w && createSizeObserver(p, onSizeChanged, {
    dt: true
  });
  const [M, T] = createDOMObserver(p, false, onHostMutation, {
    X: d,
    j: d
  });
  const k = w && f && new f((t => {
    const n = t[t.length - 1].contentRect;
    onSizeChanged({
      ft: true,
      dt: domRectAppeared(n, i)
    });
    i = n;
  }));
  const R = debounce((() => {
    const [, t] = H();
    s({
      Ht: t
    });
  }), {
    _: 222,
    p: true
  });
  return [ () => {
    k && k.observe(p);
    const t = A && A();
    const n = z && z();
    const o = M();
    const s = C.L((t => {
      if (t) {
        E({
          zt: t
        });
      } else {
        R();
      }
    }));
    return () => {
      k && k.disconnect();
      t && t();
      n && n();
      l && l();
      o();
      s();
    };
  }, ({It: t, At: n, Dt: o}) => {
    const s = {};
    const [i] = t("update.ignoreMutation");
    const [a, f] = t("update.attributes");
    const [v, p] = t("update.elementEvents");
    const [g, y] = t("update.debounce");
    const S = p || f;
    const m = n || o;
    const ignoreMutationFromOptions = t => isFunction(i) && i(t);
    if (S) {
      r && r();
      l && l();
      const [t, n] = createDOMObserver(b || h, true, onContentMutation, {
        j: concat(d, a || []),
        Y: v,
        W: u,
        G: (t, n) => {
          const {target: o, attributeName: s} = t;
          const e = !n && s && !w ? liesBetween(o, u, _) : false;
          return e || !!closest(o, `.${rt}`) || !!ignoreMutationFromOptions(t);
        }
      });
      l = t();
      r = n;
    }
    if (y) {
      E.m();
      if (isArray(g)) {
        const t = g[0];
        const n = g[1];
        e = isNumber(t) && t;
        c = isNumber(n) && n;
      } else if (isNumber(g)) {
        e = g;
        c = false;
      } else {
        e = false;
        c = false;
      }
    }
    if (m) {
      const t = T();
      const n = I && I();
      const o = r && r();
      t && assignDeep(s, onHostMutation(t[0], t[1], m));
      n && assignDeep(s, onTrinsicChanged(n[0], m));
      o && assignDeep(s, onContentMutation(o[0], m));
    }
    setDirection(s);
    return s;
  }, $ ];
};

const createScrollbarsSetupElements = (t, n, o, s) => {
  const e = "--os-viewport-percent";
  const c = "--os-scroll-percent";
  const r = "--os-scroll-direction";
  const {U: l} = getEnvironment();
  const {scrollbars: i} = l();
  const {slot: a} = i;
  const {vt: u, ht: _, ot: d, Mt: f, gt: p, wt: h, nt: g} = n;
  const {scrollbars: b} = f ? {} : t;
  const {slot: w} = b || {};
  const y = [];
  const S = [];
  const m = [];
  const O = dynamicInitializationElement([ u, _, d ], (() => g && h ? u : _), a, w);
  const initScrollTimeline = t => {
    if (v) {
      const n = new v({
        source: p,
        axis: t
      });
      const _addScrollPercentAnimation = t => {
        const o = t.Tt.animate({
          clear: [ "left" ],
          [c]: [ 0, 1 ]
        }, {
          timeline: n
        });
        return () => o.cancel();
      };
      return {
        kt: _addScrollPercentAnimation
      };
    }
  };
  const $ = {
    x: initScrollTimeline("x"),
    y: initScrollTimeline("y")
  };
  const getViewportPercent = () => {
    const {Rt: t, Vt: n} = o;
    const getAxisValue = (t, n) => capNumber(0, 1, t / (t + n) || 0);
    return {
      x: getAxisValue(n.x, t.x),
      y: getAxisValue(n.y, t.y)
    };
  };
  const scrollbarStructureAddRemoveClass = (t, n, o) => {
    const s = o ? addClass : removeClass;
    each(t, (t => {
      s(t.Tt, n);
    }));
  };
  const scrollbarStyle = (t, n) => {
    each(t, (t => {
      const [o, s] = n(t);
      setStyles(o, s);
    }));
  };
  const scrollbarsAddRemoveClass = (t, n, o) => {
    const s = isBoolean(o);
    const e = s ? o : true;
    const c = s ? !o : true;
    e && scrollbarStructureAddRemoveClass(S, t, n);
    c && scrollbarStructureAddRemoveClass(m, t, n);
  };
  const refreshScrollbarsHandleLength = () => {
    const t = getViewportPercent();
    const createScrollbarStyleFn = t => n => [ n.Tt, {
      [e]: roundCssNumber(t) + ""
    } ];
    scrollbarStyle(S, createScrollbarStyleFn(t.x));
    scrollbarStyle(m, createScrollbarStyleFn(t.y));
  };
  const refreshScrollbarsHandleOffset = () => {
    if (!v) {
      const {Lt: t} = o;
      const n = getScrollCoordinatesPercent(t, getElementScroll(p));
      const createScrollbarStyleFn = t => n => [ n.Tt, {
        [c]: roundCssNumber(t) + ""
      } ];
      scrollbarStyle(S, createScrollbarStyleFn(n.x));
      scrollbarStyle(m, createScrollbarStyleFn(n.y));
    }
  };
  const refreshScrollbarsScrollCoordinates = () => {
    const {Lt: t} = o;
    const n = isDefaultDirectionScrollCoordinates(t);
    const createScrollbarStyleFn = t => n => [ n.Tt, {
      [r]: t ? "0" : "1"
    } ];
    scrollbarStyle(S, createScrollbarStyleFn(n.x));
    scrollbarStyle(m, createScrollbarStyleFn(n.y));
  };
  const refreshScrollbarsScrollbarOffset = () => {
    if (g && !h) {
      const {Rt: t, Lt: n} = o;
      const s = isDefaultDirectionScrollCoordinates(n);
      const e = getScrollCoordinatesPercent(n, getElementScroll(p));
      const styleScrollbarPosition = n => {
        const {Tt: o} = n;
        const c = parent(o) === d && o;
        const getTranslateValue = (t, n, o) => {
          const s = n * t;
          return numberToCssPx(o ? s : -s);
        };
        return [ c, c && {
          transform: getTrasformTranslateValue({
            x: getTranslateValue(e.x, t.x, s.x),
            y: getTranslateValue(e.y, t.y, s.y)
          })
        } ];
      };
      scrollbarStyle(S, styleScrollbarPosition);
      scrollbarStyle(m, styleScrollbarPosition);
    }
  };
  const generateScrollbarDOM = t => {
    const n = t ? "x" : "y";
    const o = t ? it : at;
    const e = createDiv(`${rt} ${o}`);
    const c = createDiv(ut);
    const r = createDiv(_t);
    const l = {
      Tt: e,
      Ut: c,
      Pt: r
    };
    const i = $[n];
    push(t ? S : m, l);
    push(y, [ appendChildren(e, c), appendChildren(c, r), bind(removeElements, e), i && i.kt(l), s(l, scrollbarsAddRemoveClass, t) ]);
    return l;
  };
  const C = bind(generateScrollbarDOM, true);
  const x = bind(generateScrollbarDOM, false);
  const appendElements = () => {
    appendChildren(O, S[0].Tt);
    appendChildren(O, m[0].Tt);
    return bind(runEachAndClear, y);
  };
  C();
  x();
  return [ {
    Nt: refreshScrollbarsHandleLength,
    qt: refreshScrollbarsHandleOffset,
    Bt: refreshScrollbarsScrollCoordinates,
    Ft: refreshScrollbarsScrollbarOffset,
    jt: scrollbarsAddRemoveClass,
    Xt: {
      Yt: S,
      Wt: C,
      Jt: bind(scrollbarStyle, S)
    },
    Gt: {
      Yt: m,
      Wt: x,
      Jt: bind(scrollbarStyle, m)
    }
  }, appendElements ];
};

const createScrollbarsSetupEvents = (t, n, o, s) => (r, l, i) => {
  const {ht: u, ot: _, nt: d, gt: f, Kt: v, Ot: p} = n;
  const {Tt: h, Ut: g, Pt: b} = r;
  const [w, y] = selfClearTimeout(333);
  const [S, m] = selfClearTimeout(444);
  const scrollOffsetElementScrollBy = t => {
    isFunction(f.scrollBy) && f.scrollBy({
      behavior: "smooth",
      left: t.x,
      top: t.y
    });
  };
  const createInteractiveScrollEvents = () => {
    const n = "pointerup pointercancel lostpointercapture";
    const s = `client${i ? "X" : "Y"}`;
    const r = i ? $ : C;
    const l = i ? "left" : "top";
    const a = i ? "w" : "h";
    const u = i ? "x" : "y";
    const createRelativeHandleMove = (t, n) => s => {
      const {Rt: e} = o;
      const c = I(g)[a] - I(b)[a];
      const r = n * s / c;
      const l = r * e[u];
      scrollElementTo(f, {
        [u]: t + l
      });
    };
    const _ = [];
    return addEventListener(g, "pointerdown", (o => {
      const i = closest(o.target, `.${_t}`) === b;
      const d = i ? b : g;
      const h = t.scrollbars;
      const w = h[i ? "dragScroll" : "clickScroll"];
      const {button: y, isPrimary: O, pointerType: $} = o;
      const {pointers: C} = h;
      const x = y === 0 && O && w && (C || []).includes($);
      if (x) {
        runEachAndClear(_);
        m();
        const t = !i && (o.shiftKey || w === "instant");
        const h = bind(getBoundingClientRect, b);
        const y = bind(getBoundingClientRect, g);
        const getHandleOffset = (t, n) => (t || h())[l] - (n || y())[l];
        const O = e(getBoundingClientRect(f)[r]) / I(f)[a] || 1;
        const $ = createRelativeHandleMove(getElementScroll(f)[u], 1 / O);
        const C = o[s];
        const x = h();
        const H = y();
        const E = x[r];
        const z = getHandleOffset(x, H) + E / 2;
        const A = C - H[l];
        const D = i ? 0 : A - z;
        const releasePointerCapture = t => {
          runEachAndClear(k);
          d.releasePointerCapture(t.pointerId);
        };
        const M = i || t;
        const T = p();
        const k = [ addEventListener(v, n, releasePointerCapture), addEventListener(v, "selectstart", (t => preventDefault(t)), {
          H: false
        }), addEventListener(g, n, releasePointerCapture), M && addEventListener(g, "pointermove", (t => $(D + (t[s] - C)))), M && (() => {
          const t = getElementScroll(f);
          T();
          const n = getElementScroll(f);
          const o = {
            x: n.x - t.x,
            y: n.y - t.y
          };
          if (c(o.x) > 3 || c(o.y) > 3) {
            p();
            scrollElementTo(f, t);
            scrollOffsetElementScrollBy(o);
            S(T);
          }
        }) ];
        d.setPointerCapture(o.pointerId);
        if (t) {
          $(D);
        } else if (!i) {
          const t = getStaticPluginModuleInstance(At);
          if (t) {
            const n = t($, D, E, (t => {
              if (t) {
                T();
              } else {
                push(k, T);
              }
            }));
            push(k, n);
            push(_, bind(n, true));
          }
        }
      }
    }));
  };
  let O = true;
  return bind(runEachAndClear, [ addEventListener(b, "pointermove pointerleave", s), addEventListener(h, "pointerenter", (() => {
    l(vt, true);
  })), addEventListener(h, "pointerleave pointercancel", (() => {
    l(vt, false);
  })), !d && addEventListener(h, "mousedown", (() => {
    const t = getFocusedElement();
    if (hasAttr(t, q) || hasAttr(t, P) || t === document.body) {
      a(bind(focusElement, _), 25);
    }
  })), addEventListener(h, "wheel", (t => {
    const {deltaX: n, deltaY: o, deltaMode: s} = t;
    if (O && s === 0 && parent(h) === u) {
      scrollOffsetElementScrollBy({
        x: n,
        y: o
      });
    }
    O = false;
    l(bt, true);
    w((() => {
      O = true;
      l(bt);
    }));
    preventDefault(t);
  }), {
    H: false,
    I: true
  }), addEventListener(h, "pointerdown", bind(addEventListener, v, "click", stopAndPrevent, {
    A: true,
    I: true,
    H: false
  }), {
    I: true
  }), createInteractiveScrollEvents(), y, m ]);
};

const createScrollbarsSetup = (t, n, o, s, e, c) => {
  let r;
  let l;
  let i;
  let a;
  let u;
  let _ = noop;
  let d = 0;
  const isHoverablePointerType = t => t.pointerType === "mouse";
  const [f, v] = selfClearTimeout();
  const [p, h] = selfClearTimeout(100);
  const [g, b] = selfClearTimeout(100);
  const [w, y] = selfClearTimeout((() => d));
  const [S, m] = createScrollbarsSetupElements(t, e, s, createScrollbarsSetupEvents(n, e, s, (t => isHoverablePointerType(t) && manageScrollbarsAutoHideInstantInteraction())));
  const {ht: O, Qt: $, wt: C} = e;
  const {jt: H, Nt: z, qt: I, Bt: A, Ft: D} = S;
  const manageScrollbarsAutoHide = (t, n) => {
    y();
    if (t) {
      H(gt);
    } else {
      const t = bind(H, gt, true);
      if (d > 0 && !n) {
        w(t);
      } else {
        t();
      }
    }
  };
  const manageScrollbarsAutoHideInstantInteraction = () => {
    if (i ? !r : !a) {
      manageScrollbarsAutoHide(true);
      p((() => {
        manageScrollbarsAutoHide(false);
      }));
    }
  };
  const manageAutoHideSuspension = t => {
    H(ht, t, true);
    H(ht, t, false);
  };
  const onHostMouseEnter = t => {
    if (isHoverablePointerType(t)) {
      r = i;
      i && manageScrollbarsAutoHide(true);
    }
  };
  const M = [ y, h, b, v, () => _(), addEventListener(O, "pointerover", onHostMouseEnter, {
    A: true
  }), addEventListener(O, "pointerenter", onHostMouseEnter), addEventListener(O, "pointerleave", (t => {
    if (isHoverablePointerType(t)) {
      r = false;
      i && manageScrollbarsAutoHide(false);
    }
  })), addEventListener(O, "pointermove", (t => {
    isHoverablePointerType(t) && l && manageScrollbarsAutoHideInstantInteraction();
  })), addEventListener($, "scroll", (t => {
    f((() => {
      I();
      manageScrollbarsAutoHideInstantInteraction();
    }));
    c(t);
    D();
  })) ];
  return [ () => bind(runEachAndClear, push(M, m())), ({It: t, Dt: n, Zt: e, tn: c}) => {
    const {nn: r, sn: f, en: v, cn: p} = c || {};
    const {Ct: h, dt: b} = e || {};
    const {ct: w} = o;
    const {k: y} = getEnvironment();
    const {K: S, rn: m} = s;
    const [O, M] = t("showNativeOverlaidScrollbars");
    const [T, k] = t("scrollbars.theme");
    const [R, V] = t("scrollbars.visibility");
    const [L, U] = t("scrollbars.autoHide");
    const [P, N] = t("scrollbars.autoHideSuspend");
    const [q] = t("scrollbars.autoHideDelay");
    const [B, F] = t("scrollbars.dragScroll");
    const [j, X] = t("scrollbars.clickScroll");
    const [Y, W] = t("overflow");
    const J = b && !n;
    const G = m.x || m.y;
    const K = r || f || p || h || n;
    const Q = v || V || W;
    const Z = O && y.x && y.y;
    const setScrollbarVisibility = (t, n, o) => {
      const s = t.includes(E) && (R === x || R === "auto" && n === E);
      H(dt, s, o);
      return s;
    };
    d = q;
    if (J) {
      if (P && G) {
        manageAutoHideSuspension(false);
        _();
        g((() => {
          _ = addEventListener($, "scroll", bind(manageAutoHideSuspension, true), {
            A: true
          });
        }));
      } else {
        manageAutoHideSuspension(true);
      }
    }
    if (M) {
      H(ct, Z);
    }
    if (k) {
      H(u);
      H(T, true);
      u = T;
    }
    if (N && !P) {
      manageAutoHideSuspension(true);
    }
    if (U) {
      l = L === "move";
      i = L === "leave";
      a = L === "never";
      manageScrollbarsAutoHide(a, true);
    }
    if (F) {
      H(yt, B);
    }
    if (X) {
      H(wt, !!j);
    }
    if (Q) {
      const t = setScrollbarVisibility(Y.x, S.x, true);
      const n = setScrollbarVisibility(Y.y, S.y, false);
      const o = t && n;
      H(ft, !o);
    }
    if (K) {
      I();
      z();
      D();
      p && A();
      H(pt, !m.x, true);
      H(pt, !m.y, false);
      H(lt, w && !C);
    }
  }, {}, S ];
};

const createStructureSetupElements = t => {
  const o = getEnvironment();
  const {U: s, R: e} = o;
  const {elements: c} = s();
  const {padding: r, viewport: l, content: i} = c;
  const a = isHTMLElement(t);
  const u = a ? {} : t;
  const {elements: _} = u;
  const {padding: d, viewport: f, content: v} = _ || {};
  const p = a ? t : u.target;
  const h = isBodyElement(p);
  const g = p.ownerDocument;
  const b = g.documentElement;
  const getDocumentWindow = () => g.defaultView || n;
  const w = bind(staticInitializationElement, [ p ]);
  const y = bind(dynamicInitializationElement, [ p ]);
  const S = bind(createDiv, "");
  const $ = bind(w, S, l);
  const C = bind(y, S, i);
  const elementHasOverflow = t => {
    const n = I(t);
    const o = D(t);
    const s = getStyles(t, m);
    const e = getStyles(t, O);
    return o.w - n.w > 0 && !overflowIsVisible(s) || o.h - n.h > 0 && !overflowIsVisible(e);
  };
  const x = $(f);
  const H = x === p;
  const E = H && h;
  const z = !H && C(v);
  const A = !H && x === z;
  const M = E ? b : x;
  const T = E ? M : p;
  const k = !H && y(S, r, d);
  const R = !A && z;
  const L = [ R, M, k, T ].map((t => isHTMLElement(t) && !parent(t) && t));
  const elementIsGenerated = t => t && inArray(L, t);
  const B = !elementIsGenerated(M) && elementHasOverflow(M) ? M : p;
  const F = E ? b : M;
  const j = E ? g : M;
  const X = {
    vt: p,
    ht: T,
    ot: M,
    ln: k,
    bt: R,
    gt: F,
    Qt: j,
    an: h ? b : B,
    Kt: g,
    wt: h,
    Mt: a,
    nt: H,
    un: getDocumentWindow,
    yt: t => hasAttrClass(M, q, t),
    St: (t, n) => addRemoveAttrClass(M, q, t, n),
    Ot: () => addRemoveAttrClass(F, q, Y, true)
  };
  const {vt: J, ht: Q, ln: Z, ot: tt, bt: nt} = X;
  const ot = [ () => {
    removeAttrs(Q, [ P, V ]);
    removeAttrs(J, V);
    if (h) {
      removeAttrs(b, [ V, P ]);
    }
  } ];
  let st = contents([ nt, tt, Z, Q, J ].find((t => t && !elementIsGenerated(t))));
  const et = E ? J : nt || tt;
  const ct = bind(runEachAndClear, ot);
  const appendElements = () => {
    const t = getDocumentWindow();
    const n = getFocusedElement();
    const unwrap = t => {
      appendChildren(parent(t), contents(t));
      removeElements(t);
    };
    const prepareWrapUnwrapFocus = t => addEventListener(t, "focusin focusout focus blur", stopAndPrevent, {
      I: true,
      H: false
    });
    const o = "tabindex";
    const s = getAttr(tt, o);
    const c = prepareWrapUnwrapFocus(n);
    setAttrs(Q, P, H ? "" : N);
    setAttrs(Z, G, "");
    setAttrs(tt, q, "");
    setAttrs(nt, K, "");
    if (!H) {
      setAttrs(tt, o, s || "-1");
      h && setAttrs(b, U, "");
    }
    appendChildren(et, st);
    appendChildren(Q, Z);
    appendChildren(Z || Q, !H && tt);
    appendChildren(tt, nt);
    push(ot, [ c, () => {
      const t = getFocusedElement();
      const n = elementIsGenerated(tt);
      const e = n && t === tt ? J : t;
      const c = prepareWrapUnwrapFocus(e);
      removeAttrs(Z, G);
      removeAttrs(nt, K);
      removeAttrs(tt, q);
      h && removeAttrs(b, U);
      s ? setAttrs(tt, o, s) : removeAttrs(tt, o);
      elementIsGenerated(nt) && unwrap(nt);
      n && unwrap(tt);
      elementIsGenerated(Z) && unwrap(Z);
      focusElement(e);
      c();
    } ]);
    if (e && !H) {
      addAttrClass(tt, q, W);
      push(ot, bind(removeAttrs, tt, q));
    }
    focusElement(!H && h && n === J && t.top === t ? tt : n);
    c();
    st = 0;
    return ct;
  };
  return [ X, appendElements, ct ];
};

const createTrinsicUpdateSegment = ({bt: t}) => ({Zt: n, _n: o, Dt: s}) => {
  const {xt: e} = n || {};
  const {$t: c} = o;
  const r = t && (e || s);
  if (r) {
    setStyles(t, {
      [C]: c && "100%"
    });
  }
};

const createPaddingUpdateSegment = ({ht: t, ln: n, ot: o, nt: s}, e) => {
  const [c, r] = createCache({
    i: equalTRBL,
    o: topRightBottomLeft()
  }, bind(topRightBottomLeft, t, "padding", ""));
  return ({It: t, Zt: l, _n: i, Dt: a}) => {
    let [u, _] = r(a);
    const {R: d} = getEnvironment();
    const {ft: f, Ht: v, Ct: m} = l || {};
    const {ct: O} = i;
    const [C, x] = t("paddingAbsolute");
    const H = a || v;
    if (f || _ || H) {
      [u, _] = c(a);
    }
    const E = !s && (x || m || _);
    if (E) {
      const t = !C || !n && !d;
      const s = u.r + u.l;
      const c = u.t + u.b;
      const r = {
        [y]: t && !O ? -s : 0,
        [S]: t ? -c : 0,
        [w]: t && O ? -s : 0,
        top: t ? -u.t : 0,
        right: t ? O ? -u.r : "auto" : 0,
        left: t ? O ? "auto" : -u.l : 0,
        [$]: t && `calc(100% + ${s}px)`
      };
      const l = {
        [p]: t ? u.t : 0,
        [h]: t ? u.r : 0,
        [b]: t ? u.b : 0,
        [g]: t ? u.l : 0
      };
      setStyles(n || o, r);
      setStyles(o, l);
      assignDeep(e, {
        ln: u,
        dn: !t,
        rt: n ? l : assignDeep({}, r, l)
      });
    }
    return {
      fn: E
    };
  };
};

const createOverflowUpdateSegment = (t, s) => {
  const e = getEnvironment();
  const {ht: c, ln: r, ot: l, nt: a, Qt: u, gt: _, wt: d, St: f, un: v} = t;
  const {R: p} = e;
  const h = d && a;
  const g = bind(o, 0);
  const b = {
    display: () => false,
    direction: t => t !== "ltr",
    flexDirection: t => t.endsWith("-reverse"),
    writingMode: t => t !== "horizontal-tb"
  };
  const w = keys(b);
  const y = {
    i: equalWH,
    o: {
      w: 0,
      h: 0
    }
  };
  const S = {
    i: equalXY,
    o: {}
  };
  const setMeasuringMode = t => {
    f(X, !h && t);
  };
  const getMeasuredScrollCoordinates = t => {
    const n = w.some((n => {
      const o = t[n];
      return o && b[n](o);
    }));
    if (!n) {
      return {
        D: {
          x: 0,
          y: 0
        },
        M: {
          x: 1,
          y: 1
        }
      };
    }
    setMeasuringMode(true);
    const o = getElementScroll(_);
    const s = f(J, true);
    const e = addEventListener(u, E, (t => {
      const n = getElementScroll(_);
      if (t.isTrusted && n.x === o.x && n.y === o.y) {
        stopPropagation(t);
      }
    }), {
      I: true,
      A: true
    });
    scrollElementTo(_, {
      x: 0,
      y: 0
    });
    s();
    const c = getElementScroll(_);
    const r = D(_);
    scrollElementTo(_, {
      x: r.w,
      y: r.h
    });
    const l = getElementScroll(_);
    scrollElementTo(_, {
      x: l.x - c.x < 1 && -r.w,
      y: l.y - c.y < 1 && -r.h
    });
    const a = getElementScroll(_);
    scrollElementTo(_, o);
    i((() => e()));
    return {
      D: c,
      M: a
    };
  };
  const getOverflowAmount = (t, o) => {
    const s = n.devicePixelRatio % 1 !== 0 ? 1 : 0;
    const e = {
      w: g(t.w - o.w),
      h: g(t.h - o.h)
    };
    return {
      w: e.w > s ? e.w : 0,
      h: e.h > s ? e.h : 0
    };
  };
  const [m, O] = createCache(y, bind(getFractionalSize, l));
  const [$, C] = createCache(y, bind(D, l));
  const [z, I] = createCache(y);
  const [M] = createCache(S);
  const [T, k] = createCache(y);
  const [R] = createCache(S);
  const [V] = createCache({
    i: (t, n) => equal(t, n, w),
    o: {}
  }, (() => hasDimensions(l) ? getStyles(l, w) : {}));
  const [U, N] = createCache({
    i: (t, n) => equalXY(t.D, n.D) && equalXY(t.M, n.M),
    o: getZeroScrollCoordinates()
  });
  const q = getStaticPluginModuleInstance(zt);
  const createViewportOverflowStyleClassName = (t, n) => {
    const o = n ? B : F;
    return `${o}${capitalizeFirstLetter(t)}`;
  };
  const setViewportOverflowStyle = t => {
    const createAllOverflowStyleClassNames = t => [ x, H, E ].map((n => createViewportOverflowStyleClassName(n, t)));
    const n = createAllOverflowStyleClassNames(true).concat(createAllOverflowStyleClassNames()).join(" ");
    f(n);
    f(keys(t).map((n => createViewportOverflowStyleClassName(t[n], n === "x"))).join(" "), true);
  };
  return ({It: n, Zt: o, _n: i, Dt: a}, {fn: u}) => {
    const {ft: _, Ht: d, Ct: b, dt: w, zt: y} = o || {};
    const S = q && q.tt(t, s, i, e, n);
    const {it: x, ut: H, _t: E} = S || {};
    const [D, B] = getShowNativeOverlaidScrollbars(n, e);
    const [F, j] = n("overflow");
    const X = overflowIsVisible(F.x);
    const Y = overflowIsVisible(F.y);
    const J = true;
    let K = O(a);
    let Q = C(a);
    let Z = I(a);
    let tt = k(a);
    if (B && p) {
      f(W, !D);
    }
    {
      if (hasAttrClass(c, P, L$1)) {
        setMeasuringMode(true);
      }
      const [t] = H ? H() : [];
      const [n] = K = m(a);
      const [o] = Q = $(a);
      const s = A(l);
      const e = h && getWindowSize(v());
      const r = {
        w: g(o.w + n.w),
        h: g(o.h + n.h)
      };
      const i = {
        w: g((e ? e.w : s.w + g(s.w - o.w)) + n.w),
        h: g((e ? e.h : s.h + g(s.h - o.h)) + n.h)
      };
      t && t();
      tt = T(i);
      Z = z(getOverflowAmount(r, i), a);
    }
    const [nt, ot] = tt;
    const [st, et] = Z;
    const [ct, rt] = Q;
    const [lt, it] = K;
    const [at, ut] = M({
      x: st.w > 0,
      y: st.h > 0
    });
    const _t = X && Y && (at.x || at.y) || X && at.x && !at.y || Y && at.y && !at.x;
    const dt = u || b || y || it || rt || ot || et || j || B || J;
    const ft = createViewportOverflowState(at, F);
    const [vt, pt] = R(ft.K);
    const [ht, gt] = V(a);
    const bt = b || w || gt || ut || a;
    const [wt, yt] = bt ? U(getMeasuredScrollCoordinates(ht), a) : N();
    if (dt) {
      pt && setViewportOverflowStyle(ft.K);
      if (E && x) {
        setStyles(l, E(ft, i, x(ft, ct, lt)));
      }
    }
    setMeasuringMode(false);
    addRemoveAttrClass(c, P, L$1, _t);
    addRemoveAttrClass(r, G, L$1, _t);
    assignDeep(s, {
      K: vt,
      Vt: {
        x: nt.w,
        y: nt.h
      },
      Rt: {
        x: st.w,
        y: st.h
      },
      rn: at,
      Lt: sanitizeScrollCoordinates(wt, st)
    });
    return {
      en: pt,
      nn: ot,
      sn: et,
      cn: yt || et,
      vn: bt
    };
  };
};

const createStructureSetup = t => {
  const [n, o, s] = createStructureSetupElements(t);
  const e = {
    ln: {
      t: 0,
      r: 0,
      b: 0,
      l: 0
    },
    dn: false,
    rt: {
      [y]: 0,
      [S]: 0,
      [w]: 0,
      [p]: 0,
      [h]: 0,
      [b]: 0,
      [g]: 0
    },
    Vt: {
      x: 0,
      y: 0
    },
    Rt: {
      x: 0,
      y: 0
    },
    K: {
      x: H,
      y: H
    },
    rn: {
      x: false,
      y: false
    },
    Lt: getZeroScrollCoordinates()
  };
  const {vt: c, gt: r, nt: l, Ot: i} = n;
  const {R: a, k: u} = getEnvironment();
  const _ = !a && (u.x || u.y);
  const d = [ createTrinsicUpdateSegment(n), createPaddingUpdateSegment(n, e), createOverflowUpdateSegment(n, e) ];
  return [ o, t => {
    const n = {};
    const o = _;
    const s = o && getElementScroll(r);
    const e = s && i();
    each(d, (o => {
      assignDeep(n, o(t, n) || {});
    }));
    scrollElementTo(r, s);
    e && e();
    !l && scrollElementTo(c, 0);
    return n;
  }, e, n, s ];
};

const createSetups = (t, n, o, s, e) => {
  let c = false;
  const r = createOptionCheck(n, {});
  const [l, i, a, u, _] = createStructureSetup(t);
  const [d, f, v] = createObserversSetup(u, a, r, (t => {
    update({}, t);
  }));
  const [p, h, , g] = createScrollbarsSetup(t, n, v, a, u, e);
  const updateHintsAreTruthy = t => keys(t).some((n => !!t[n]));
  const update = (t, e) => {
    if (o()) {
      return false;
    }
    const {pn: r, Dt: l, At: a, hn: u} = t;
    const _ = r || {};
    const d = !!l || !c;
    const p = {
      It: createOptionCheck(n, _, d),
      pn: _,
      Dt: d
    };
    if (u) {
      h(p);
      return false;
    }
    const g = e || f(assignDeep({}, p, {
      At: a
    }));
    const b = i(assignDeep({}, p, {
      _n: v,
      Zt: g
    }));
    h(assignDeep({}, p, {
      Zt: g,
      tn: b
    }));
    const w = updateHintsAreTruthy(g);
    const y = updateHintsAreTruthy(b);
    const S = w || y || !isEmptyObject(_) || d;
    c = true;
    S && s(t, {
      Zt: g,
      tn: b
    });
    return S;
  };
  return [ () => {
    const {an: t, gt: n, Ot: o} = u;
    const s = getElementScroll(t);
    const e = [ d(), l(), p() ];
    const c = o();
    scrollElementTo(n, s);
    c();
    return bind(runEachAndClear, e);
  }, update, () => ({
    gn: v,
    bn: a
  }), {
    wn: u,
    yn: g
  }, _ ];
};

const OverlayScrollbars = (t, n, o) => {
  const {N: s} = getEnvironment();
  const e = isHTMLElement(t);
  const c = e ? t : t.target;
  const r = getInstance(c);
  if (n && !r) {
    let r = false;
    const l = [];
    const i = {};
    const validateOptions = t => {
      const n = removeUndefinedProperties(t);
      const o = getStaticPluginModuleInstance(xt);
      return o ? o(n, true) : n;
    };
    const a = assignDeep({}, s(), validateOptions(n));
    const [u, _, d] = createEventListenerHub();
    const [f, v, p] = createEventListenerHub(o);
    const triggerEvent = (t, n) => {
      p(t, n);
      d(t, n);
    };
    const [h, g, b, w, y] = createSetups(t, a, (() => r), (({pn: t, Dt: n}, {Zt: o, tn: s}) => {
      const {ft: e, Ct: c, xt: r, Ht: l, Et: i, dt: a} = o;
      const {nn: u, sn: _, en: d, cn: f} = s;
      triggerEvent("updated", [ S, {
        updateHints: {
          sizeChanged: !!e,
          directionChanged: !!c,
          heightIntrinsicChanged: !!r,
          overflowEdgeChanged: !!u,
          overflowAmountChanged: !!_,
          overflowStyleChanged: !!d,
          scrollCoordinatesChanged: !!f,
          contentMutation: !!l,
          hostMutation: !!i,
          appear: !!a
        },
        changedOptions: t || {},
        force: !!n
      } ]);
    }), (t => triggerEvent("scroll", [ S, t ])));
    const destroy = t => {
      removeInstance(c);
      runEachAndClear(l);
      r = true;
      triggerEvent("destroyed", [ S, t ]);
      _();
      v();
    };
    const S = {
      options(t, n) {
        if (t) {
          const o = n ? s() : {};
          const e = getOptionsDiff(a, assignDeep(o, validateOptions(t)));
          if (!isEmptyObject(e)) {
            assignDeep(a, e);
            g({
              pn: e
            });
          }
        }
        return assignDeep({}, a);
      },
      on: f,
      off: (t, n) => {
        t && n && v(t, n);
      },
      state() {
        const {gn: t, bn: n} = b();
        const {ct: o} = t;
        const {Vt: s, Rt: e, K: c, rn: l, ln: i, dn: a, Lt: u} = n;
        return assignDeep({}, {
          overflowEdge: s,
          overflowAmount: e,
          overflowStyle: c,
          hasOverflow: l,
          scrollCoordinates: {
            start: u.D,
            end: u.M
          },
          padding: i,
          paddingAbsolute: a,
          directionRTL: o,
          destroyed: r
        });
      },
      elements() {
        const {vt: t, ht: n, ln: o, ot: s, bt: e, gt: c, Qt: r} = w.wn;
        const {Xt: l, Gt: i} = w.yn;
        const translateScrollbarStructure = t => {
          const {Pt: n, Ut: o, Tt: s} = t;
          return {
            scrollbar: s,
            track: o,
            handle: n
          };
        };
        const translateScrollbarsSetupElement = t => {
          const {Yt: n, Wt: o} = t;
          const s = translateScrollbarStructure(n[0]);
          return assignDeep({}, s, {
            clone: () => {
              const t = translateScrollbarStructure(o());
              g({
                hn: true
              });
              return t;
            }
          });
        };
        return assignDeep({}, {
          target: t,
          host: n,
          padding: o || s,
          viewport: s,
          content: e || s,
          scrollOffsetElement: c,
          scrollEventElement: r,
          scrollbarHorizontal: translateScrollbarsSetupElement(l),
          scrollbarVertical: translateScrollbarsSetupElement(i)
        });
      },
      update: t => g({
        Dt: t,
        At: true
      }),
      destroy: bind(destroy, false),
      plugin: t => i[keys(t)[0]]
    };
    push(l, [ y ]);
    addInstance(c, S);
    registerPluginModuleInstances($t, OverlayScrollbars, [ S, u, i ]);
    if (cancelInitialization(w.wn.wt, !e && t.cancel)) {
      destroy(true);
      return S;
    }
    push(l, h());
    triggerEvent("initialized", [ S ]);
    S.update();
    return S;
  }
  return r;
};

OverlayScrollbars.plugin = t => {
  const n = isArray(t);
  const o = n ? t : [ t ];
  const s = o.map((t => registerPluginModuleInstances(t, OverlayScrollbars)[0]));
  addPlugins(o);
  return n ? s : s[0];
};

OverlayScrollbars.valid = t => {
  const n = t && t.elements;
  const o = isFunction(n) && n();
  return isPlainObject(o) && !!getInstance(o.target);
};

OverlayScrollbars.env = () => {
  const {T: t, k: n, R: o, V: s, B: e, F: c, U: r, P: l, N: i, q: a} = getEnvironment();
  return assignDeep({}, {
    scrollbarsSize: t,
    scrollbarsOverlaid: n,
    scrollbarsHiding: o,
    scrollTimeline: s,
    staticDefaultInitialization: e,
    staticDefaultOptions: c,
    getDefaultInitialization: r,
    setDefaultInitialization: l,
    getDefaultOptions: i,
    setDefaultOptions: a
  });
};

OverlayScrollbars.nonce = setNonce;

// init app
(function init() {
	initMap();
	initSearch();
	buildMenus();
	toggleSearchSuggestionsBoxByClick();
	getContent();
	OverlayScrollbars(document.querySelector('#' + ELEMENT.CONTENT.id), {
		overflow: {
			x: 'hidden'
		}
	});
})();
