<!-- begin content -->
<div class="container">
	<article class="show" id="media">
		<h1 class="title"></h1>
		<div class="native empty"></div>
		<div class="content"></div>
		<div class="clear"></div>
		<div id="location-list"></div>
	</article> <!-- /.media -->

	<article id="location">
		<div id="image"></div>

		<div class="details">
			<h1 class="title"></h1>
			<div class="native empty"></div>
			<div class="empty" id="address"></div>
			<div id="metadata">
				<div id="media-title"></div>
				<div id="season-episode"></div>
				<div id="source"></div>
			</div> <!-- /#metadata -->
			<div class="empty" id="links"></div>
			<div class="content"></div>
		</div> <!-- /.details -->

		<a id="back">
			<i class="icon left material-icons md-navigate_before"></i> <span>back</span>
		</a> <!-- /#back -->
	</article> <!-- /.location -->
</div>
<!-- end content -->
