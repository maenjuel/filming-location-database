<?php
	$to = '';
	$subject = 'New location submitted';

	$locName = $_POST['location-name'];
	$nativeName = $_POST['native-name'];
	$description = $_POST['description'];
	$media = $_POST['media'];
	$lat = $_POST['lat'];
	$lon = $_POST['lon'];
	$openstreetmap = $_POST['openstreetmap'];
	$flickrImage = $_POST['flickr-image'];
	$commonsImage = $_POST['commons-image'];
	$wikipedia = $_POST['wikipedia'];
	$website = $_POST['website'];
	$source = $_POST['source'];
	$name = $_POST['sender-name'];
	$email = $_POST['email'];

	if(!$name) {
		$name = 'Anonymous';
	}

	if(!$email) {
		$email = 'noreply@fldb.cc';
	}

	$message = 'Location Name: ' . $locName . "\r\n" .
		'Native name: ' . $nativeName . "\r\n" .
		'Description: ' . $description . "\r\n" .
		'Media: ' . $media . "\r\n" .
		'Lat/lon: ' . $lat . '/' . $lon . "\r\n" .
		'OpenStreetMap: ' . $openstreetmap . "\r\n" .
		'flickr Image: ' . $flickrImage . "\r\n" .
		'Wikimedia Commons Image: ' . $commonsImage . "\r\n" .
		'Wikipedia: ' . $wikipedia . "\r\n" .
		'Website: ' . $website . "\r\n" .
		'Source: ' . $source . "\r\n";
	$headers = array(
		'From' => $name . '<' . $email . '>',
		'Reply-To' => $email,
		'X-Mailer' => 'PHP/' . phpversion()
	);

	// We just check one value before sending the mail so the script
	// Doesn't run by visiting the mail script only to prevent spam
	if($locName) {
		mail($to, $subject, $message, $headers);
	}
?>
Script to submit locations. Does nothing on its own.
