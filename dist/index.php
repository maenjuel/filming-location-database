<!doctype html>
<html class="no-js" lang="en-GB">

<head>
	<meta charset="utf-8">
	<title>filming location database</title>
	<meta name="description" content="The filming location database is an open database for movie and TV series filming locations. It is a project maintained by its community.">
	<meta name="keywords" content="fldb, filming location database, location, filming location, movie location, TV series location, TV show location">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#fff">

	<link rel="shortcut icon" type='image/x-icon' href="/favicon.ico" />

	<link rel="stylesheet" href="/vendor/Leaflet/Leaflet/leaflet.css" />
	<link rel="stylesheet" href="/vendor/Leaflet/Leaflet.markercluster/MarkerCluster.css" />
	<link rel="stylesheet" href="/vendor/Leaflet/Leaflet.markercluster/MarkerCluster.Default.css" />
	<link rel="stylesheet" href="/vendor/google/@material-icons/css/all.css" />
	<link rel="stylesheet" href="/vendor/KingSora/OverlayScrollbars/styles/overlayscrollbars.min.css" />
	<link rel="stylesheet" href="/vendor/bedlaj/@openfonts/lato_latin-ext/index.css" />
	<link rel="stylesheet" href="/css/fldb.css" />
</head>

<body>
	<!--[if IE]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

	<header id="page-header">
		<div id="logo">
			<a href="/" title="Home">
				<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" enable-background="new 0 0 41 32" viewBox="0 0 40.998329 45.313717" height="45.313717" width="40.998329" y="0px" x="0px" id="Layer_1" version="1.1">
					<metadata id="metadata15">
						<rdf:RDF>
							<cc:Work rdf:about="">
								<dc:format>image/svg+xml</dc:format>
								<dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
								<dc:title></dc:title>
							</cc:Work>
						</rdf:RDF>
					</metadata>
					<defs id="defs13"></defs>
					<path d="m 9.2384142,45.240719 c 0.009,0.006 0.021,0.003 0.03,0.009 0.073,0.037 0.149,0.064 0.231,0.064 0.044,0 0.088,-0.006 0.132,-0.018 l 10.8679998,-2.966 10.868,2.966 c 0.044,0.012 0.088,0.018 0.132,0.018 0.082,0 0.158,-0.027 0.23,-0.065 0.01,-0.005 0.021,-0.003 0.03,-0.009 l 9,-5.5 c 0.191,-0.117 0.281,-0.348 0.22,-0.563 l -4.984,-17.5 c -0.041,-0.147 -0.148,-0.267 -0.29,-0.326 -0.142,-0.057 -0.301,-0.048 -0.436,0.026 l -4.962,2.784 c -0.24,0.135 -0.326,0.44 -0.191,0.681 0.135,0.242 0.439,0.327 0.682,0.191 l 4.409,-2.475 4.707,16.526 -8.015,4.899 -1.904,-15.231 c -0.034,-0.275 -0.293,-0.466 -0.559,-0.434 -0.273,0.034 -0.468,0.284 -0.434,0.558 l 1.907,15.259 -9.91,-2.705 v -2.73 c 0,-0.276 -0.224,-0.5 -0.5,-0.5 -0.276,0 -0.5,0.224 -0.5,0.5 v 2.73 l -9.911,2.705 1.907,-15.259 c 0.034,-0.274 -0.16,-0.524 -0.434,-0.558 -0.272,-0.032 -0.524,0.159 -0.559,0.434 l -1.9029998,15.231 -8.015,-4.898 4.707,-16.525 4.4089998,2.475 c 0.242,0.134 0.546,0.049 0.682,-0.191 0.135,-0.241 0.049,-0.545 -0.191,-0.681 l -4.9629998,-2.785 c -0.133,-0.075 -0.292,-0.085 -0.435,-0.026 -0.142,0.059 -0.249,0.178 -0.29,0.326 l -4.98400004,17.5 c -0.062,0.216 0.028,0.446 0.22,0.563 z" id="path2" style="fill:#01a611;fill-opacity:1" />
					<g style="fill:#111111;fill-opacity:1;stroke:none" id="g10" transform="matrix(0.00286641,0,0,-0.00286641,10.211105,36.681719)">
						<path style="fill:#111111;fill-opacity:1" id="path4-3" d="m 3976,12785 c -459,-75 -837,-357 -1030,-766 -49,-103 -91,-227 -101,-293 -4,-23 -10,-48 -14,-54 -5,-8 -140,-42 -167,-42 -1,0 -21,30 -46,68 -161,239 -426,411 -722,467 -94,18 -282,21 -377,6 -389,-63 -728,-330 -870,-686 -179,-449 -68,-945 283,-1263 121,-110 275,-199 421,-243 l 67,-21 v -64 -64 h -113 c -62,0 -117,4 -123,9 -5,5 -65,30 -133,55 -121,45 -128,47 -221,44 l -96,-3 143,-300 c 80,-165 147,-303 151,-307 4,-4 30,-2 57,5 44,11 54,19 85,65 l 34,52 h 108 108 v -705 -705 h 180 c 142,0 180,-3 180,-13 0,-19 -39,-300 -42,-303 -2,-1 -195,-38 -430,-83 l -428,-81 -90,45 c -49,25 -97,45 -107,45 -29,0 -667,-122 -675,-129 -4,-4 1,-79 12,-166 29,-230 27,-224 50,-218 10,3 165,33 345,67 l 326,63 82,81 82,81 210,45 c 524,113 718,156 723,161 4,4 56,295 72,403 0,1 774,2 1720,2 h 1720 v 1170 1170 h -150 c -82,0 -150,2 -150,4 0,2 34,37 76,77 192,186 328,434 386,702 31,145 31,398 0,544 -58,271 -193,514 -391,706 -186,180 -395,293 -651,353 -136,32 -362,41 -494,19 z m -949,-2082 c 54,-85 132,-180 207,-252 37,-35 65,-65 63,-67 -2,-3 -160,-3 -350,-2 l -347,3 31,45 c 42,62 104,186 120,239 7,23 19,45 27,48 14,6 177,40 200,42 7,0 29,-25 49,-56 z M 5009,8496 c 41,-22 61,-59 61,-112 0,-39 -5,-51 -34,-80 -29,-29 -41,-34 -80,-34 -53,0 -90,20 -112,61 -55,107 58,220 165,165 z" />
						<path style="fill:#111111;fill-opacity:1" id="path6-6" d="m 6275,10318 -340,-207 h -272 l -273,-1 v -525 -525 h 271 272 l 339,-206 c 187,-113 341,-204 344,-202 2,3 3,425 2,939 l -3,935 z" />
						<path style="fill:#111111;fill-opacity:1" id="path8" d="m 2460,7715 v -235 h 61 c 53,0 60,-2 55,-17 -5,-16 -73,-355 -435,-2153 l -107,-535 -57,-3 -57,-3 v -144 -145 h 25 c 14,0 25,-4 25,-8 0,-5 -65,-332 -144,-728 C 1564,2443 1180,535 1175,514 c -4,-18 5,-22 81,-41 60,-15 87,-17 89,-9 2,6 200,736 440,1621 626,2307 645,2377 651,2386 3,5 32,9 65,9 h 59 v 145 c 0,138 -1,145 -20,145 -26,0 -25,7 9,131 16,57 120,439 231,849 111,410 217,799 235,865 l 33,120 6,-75 c 4,-41 24,-365 46,-720 22,-355 62,-1009 90,-1455 28,-445 66,-1062 85,-1370 60,-972 164,-2653 175,-2835 6,-96 13,-199 16,-227 l 5,-53 h 105 104 v 33 c 0,19 16,337 35,708 32,613 53,1032 145,2819 17,322 35,655 41,740 6,85 13,211 15,280 4,123 25,537 84,1680 16,316 30,603 30,638 0,34 4,62 10,62 6,0 10,-6 10,-14 0,-8 34,-140 76,-293 41,-153 174,-642 295,-1086 l 219,-809 v -139 -139 h 35 c 28,0 37,-5 44,-23 5,-13 214,-779 465,-1702 655,-2416 645,-2377 650,-2383 8,-7 166,29 166,38 0,4 -108,545 -241,1201 -132,657 -303,1505 -379,1884 -76,380 -152,754 -168,832 -16,78 -27,145 -24,148 3,3 25,5 49,5 h 43 v 145 145 h -79 -78 l -71,353 c -39,193 -161,800 -271,1347 -111,547 -201,999 -201,1005 0,6 20,9 50,7 l 50,-4 v 236 236 H 3570 2460 Z m 551,-352 c 2,-83 -4,-108 -94,-438 -52,-192 -164,-602 -247,-910 -84,-308 -194,-713 -244,-900 l -93,-340 -62,-3 -62,-3 7,28 c 4,15 127,624 274,1353 l 266,1325 120,5 c 140,6 132,13 135,-117 z m 839,85 c 0,-18 -13,-289 -30,-603 -16,-313 -45,-889 -65,-1280 -19,-390 -38,-734 -41,-762 l -5,-53 h -163 -162 l -12,188 c -7,103 -35,552 -62,997 -27,446 -59,951 -70,1124 -18,288 -19,317 -4,367 l 15,54 h 300 299 z m 550,29 c 0,-2 122,-608 270,-1346 149,-739 270,-1347 270,-1352 0,-6 -27,-9 -62,-7 l -61,3 -172,635 c -95,349 -257,946 -359,1325 -103,380 -191,702 -196,718 l -9,27 h 160 c 87,0 159,-2 159,-3 z M 2250,4474 c 0,-3 -66,-247 -146,-542 -80,-295 -160,-590 -176,-655 -17,-64 -33,-116 -35,-114 -4,5 254,1295 262,1308 7,10 95,13 95,3 z m 2896,-709 c 128,-635 161,-815 125,-668 -7,26 -93,346 -192,709 -98,364 -179,665 -179,669 0,3 23,5 52,3 l 52,-3 z m -1452,623 c -2,-35 -14,-238 -24,-453 -11,-214 -38,-740 -60,-1168 -22,-427 -40,-798 -40,-824 0,-91 -10,-34 -20,113 -10,152 -57,916 -110,1784 -16,267 -32,513 -35,548 l -6,62 h 150 150 z" />
					</g>
				</svg>
				<span>filming location database</span>
			</a>
		</div>
		<div id="search-container">
			<div id="search">
				<i class="material-icons-outline md-36 md-search"></i>
				<input type="text" id="search-box" placeholder="Type movie or TV show ..." />
				<div id="search-suggestions">
					<div class="show" id="newest">
						<strong>Newest movies and TV shows</strong>
						<ul id="newest-entries">
						</ul> <!-- /#newest-entries -->

						<div class="more">
							<a class="button" href="/movies"><i class="icon left material-icons-outline md-18 md-live_tv"></i> View all movies</a>
							<a class="button" href="/tv-shows"><i class="icon left material-icons-outline md-18 md-theaters"></i> View all TV shows</a>
						</div> <!-- /#more -->
					</div>

					<ul id="suggestions">
					</ul> <!-- /#suggestions -->
				</div> <!-- /#search-suggestions -->
			</div> <!-- /#search -->
		</div> <!-- /#filter-container -->
		<nav id="navigation">
			<ul id="header-menu">
			</ul> <!-- /#main-menu -->
		</nav> <!-- /#navigation -->
	</header> <!-- /#header -->

	<main id="page-content">
		<div class="show" id="content">
			<span class="close" id="close-content">
				<i class="icon material-icons md-close"></i>
			</span> <!-- /#toggle-content -->

			<div class="messages empty"></div>
			<?php
				// Depending on the URL, we require a different layout. The content itself
				// Will be populated by JavaScript
				$path = $_SERVER['REQUEST_URI'];

    		if($path == '/' || strpos($path, 'm/') !== false || strpos($path, 'tv/') !== false) {
    			require('assets/layout/location.php');
    		} else {
    			require('assets/layout/page.php');
    		}
			?>
		</div> <!-- /#content -->

		<footer id="page-footer">
			<nav id="legal">
				<ul id="footer-menu">
					<li><a href="/javascript-licenses.php" title="JavaScript licenses" rel="jslicense">JavaScript licenses</a></li>
					<li><a href="https://gitlab.com/maenjuel/filming-location-database" title="GitLab" target="_blank">GitLab</a></li>
				</ul> <!-- /#footer-menu -->
			</nav>
		</footer> <!-- /#page-footer -->

		<div id="map">
		</div> <!-- /#map -->
		
		<aside id="about">
			<h2 id="about-title"></h2>
			<div id="about-content">
			</div> <!-- /#aside-content -->
		</aside> <!-- /#site-info -->
		<div id="overlay"></div>
	</main> <!-- /#page-content -->

	<script src="/vendor/Leaflet/Leaflet/leaflet.js"></script>
	<script src="/vendor/Leaflet/Leaflet.markercluster/leaflet.markercluster.js"></script>
	<script src="/vendor/hosuaby/leaflet.smooth_marker_bouncing/bundle.min.js"></script>
	<script src="/vendor/ekalinin/typogr.js/typogr.min.js"></script>
	<script src="/js/fldb.js"></script>

	<!-- Matomo -->
	<script type="text/javascript">
		// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
		var _paq = window._paq || [];
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
		  var u="//matomo.fldb.cc/";
		  _paq.push(['setTrackerUrl', u+'matomo.php']);
		  _paq.push(['setSiteId', '1']);
		  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		})();
		// @license-end
	</script>
	<noscript><p><img src="//matomo.fldb.cc/matomo.php?idsite=1&amp;rec=1" style="border:0;" alt="" /></p></noscript>
	<!-- End Matomo Code -->
</body>

</html>
