# About the filming location database

The [filming location database](https://fldb.cc) is an open database of movie and TV series [filming locations](https://en.wikipedia.org/wiki/Filming_location). It combines the love for great entertainment - and some education too, hopefully - and the need for a free and open internet. The data is published under the [Open Database License](https://www.opendatacommons.org/licenses/odbl/) (ODbL).

# About this repository

The filming location database is a decoupled Drupal app using the [JSON:API core module](https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module). In this repository, the front-end is maintained. What you see on https://fldb.cc is the very code of this repository. The only difference is that the receiving email address of the submission location form is not published here to prevent spam.

The front-end is licensed under the [GPL v3 or later](LICENSE), while libraries and fonts might use a different license (as specified below).

# Cronjob for XML sitemap

To update the XML sitemap regularly, it is necessary to set up a cronjob. First you need to specify the (absolute) path of the sitemap-media.xml file on your server in the mediaSitemap.php script. Now you can add the cronjob. If you want to generate the sitemap every day on midnight, add the following cronjob:

``0 0 * * * php [fldb-folder]/assets/mediaSitemap.php``

If you have troubles running the cronjob, you might need to specify the folder where php is installed, i.e. `/usr/bin/php`. Make sure the user running the cronjob has permission to run the script.

# Licenses

## fldb front-end

[GPL v3 or later](LICENSE)

## Fonts

* Libre Bodoni: [SIL Open Font License v1.1](https://github.com/impallari/Libre-Bodoni/blob/master/OFL.txt)
* Lato:
	- CSS to self-host: [MIT](https://github.com/bedlaj/openfonts/blob/master/LICENSE.md)
	- Font: [SIL Open Font License v1.1](https://github.com/impallari/Libre-Bodoni/blob/master/OFL.txt)

## Libraries

* Leaflet: [BSD 2-Clause "Simplified" License](https://github.com/Leaflet/Leaflet/blob/master/LICENSE)
* Leaflet.markercluster: [MIT](https://github.com/Leaflet/Leaflet.markercluster/blob/master/MIT-LICENCE.txt)
* Leaflet.SmoothMarkerBouncing: [BSD 2-Clause "Simplified" License](https://github.com/hosuaby/Leaflet.SmoothMarkerBouncing/blob/master/LICENSE)
* Material icons: [Apache License 2.0](https://github.com/material-icons/material-icons/blob/master/LICENSE)
* typogr.js: [MIT](https://github.com/ekalinin/typogr.js/blob/master/LICENSE)
